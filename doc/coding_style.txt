Gtk+ test suite development C style guide.
******************************************

This document is based on Java Style Guide by Sun Microsystems
(http://java.sun.com/docs/codeconv/html/CodeConvTOC.doc.html)


File Organization.
==================
A file consists of sections that should be separated by blank lines
and an optional comment identifying each section.

Files longer than lines are cumbersome and should be avoided.


C Source Files.
--------------
C source files have the following ordering:
  - beginning comments;
  - #include directives;
  - #define directives;
  - functions prototype declarations;
  - global variables definitions;
  - functions definitions.

Every C file should have a newline character at the end. Some version
control systems require that.


Beginning comments.
------------------

All source files should begin with a comment that lists the copyright notice: 

/*
 * Copyright notice
 */


#include directives
-------------------
#include directives should be divided into 2 groups and separated by a
single blank line:

         #include <..>

         #include ".."

Directives in both groups should be placed in alphabetical order.


#define directives
-------------------
Use upper case letters in #define directives with words separated by "_":

         #define FULL_LENGTH 100
         #define DEBUG



Functions prototypes declarations
---------------------------------
Functions prototypes declarations should follow in alphabetical order.


Functions
---------
Functions in a file should follow in alphabetical order.

When declaring functions, the return type, function name and
opening and closing braces should all start on new lines, in column zero.

        int
        my_function (char *name, int count)
        {
         ...
        }

Use the Standard C syntax for function declarations, where the type and name 
of the parameters are included in parenthesis after the function name.
For example, use:

int
my_function (char *name, int count)
{
 ...
}

and NOT:

int
my_function (name, count)
        char *name;
        int count;
{
 ...
}

Insert a space between a function name and an open bracket:
my_function (name, count)


Indentation.
============

Using tabs.
-----------
Four spaces should be used as the unit of indentation. Using of tabs
should be avoided because different text editors may have different
tab size.

Line length.
-----------
Avoid lines longer than 80 characters, since they're not handled well
by many terminals and tools.


Wrapping Lines.
---------------
When an expression will not fit on a single line, break it according
to these general principles: 

     - break after a comma; 
     - break before an operator;
     - prefer higher-level breaks to lower-level breaks;
     - align the new line with the beginning of the expression at the
       same level on the previous line;
     - if the above rules lead to confusing code just indent 8 spaces instead.

Here are some examples of breaking method calls: 

     some_method (long_expression1, long_expression2, long_expression3, 
             long_expression4, long_expression5);
      
     var = some_method1 (long_expression1,
                   some_method2(long_expression2,
                           long_expression3)); 

Following are two examples of breaking an arithmetic expression. 
The first is preferred, since the break occurs outside the parenthesized 
expression, which is at a higher level. 

     long_name1 = long_name2 * (long_name3 + long_name4 - long_name5)
                + 4 * long_name6; /* PREFER */

     long_name1 = long_name2 * (long_name3 + long_name4
                            - long_name5) + 4 * long_name6; /* AVOID */ 


Following are two examples of indenting method declarations. The first
is the conventional case. The second would shift the second and third
lines to the far right if it used conventional indentation, so instead 
it indents only 8 spaces.

     /* CONVENTIONAL INDENTATION */
     some_method (int an_arg, int another_arg, int yet_another_arg,
                  int and_still_another) 
     {
         ...
     }

     /* INDENT 8 SPACES TO AVOID VERY DEEP INDENTS */
     static unsigned very_very_very_very_long_method_name (int an_arg,
             int another_arg, int yet_another_arg, int yet_yet_another_arg,
             int and_still_another) 
     {
         ...
     }


Here are three acceptable ways to format ternary expressions: 

     alpha = (a_long_boolean_expression) ? beta : gamma;  

     alpha = (a_long_boolean_expression) ? beta
                                      : gamma;  

     alpha = (a_long_boolean_expression)
             ? beta 
             : gamma;  



Comments
=========
There are three types of comments: block, single-line and trailing.


Block Comments
--------------
Block comments are used to provide descriptions of files, methods, data 
structures and algorithms. Block comments may be used at the beginning
of each file and before each method. They can also be used in other places, 
such as within methods. Block comments inside a function or method should be 
indented to the same level as the code they describe. 

A block comment should be preceded by a blank line to set it apart from the 
rest of the code.

     /*
      * Here is a block comment.
      */



Single-Line Comments
--------------------
Short comments can appear on a single line indented to the level of the code 
that follows. If a comment can't be written in a single line, it should follow 
the block comment format. A single-line comment should be  preceded by
a blank line. Here's an example of a single-line comment:

     if (condition) 
     {

         /* Handle the condition. */
         ...
     }


Trailing Comments
-----------------
Very short comments can appear on the same line as the code they describe, 
but should be shifted far enough to separate them from the statements. 
If more than one short comment appears in a chunk of code, they should all be
indented to the same tab setting. 

Here's an example of a trailing comment in Java code: 

     if (a == 2) 
     {
         return TRUE;              /* special case */
     } else {
         return is_prime (a);      /* works only for odd a */
     }


Declarations
=============

Number Per Line
---------------
Avoid declaring multiple variables in the same statement if this makes 
the code cluttered or difficult to read. For example, avoid:

        int count, *retval, max = 300;

instead, use

        int count;
        int *retval;
        int max = 300;


Initialization
--------------
Try to initialize local variables where they're declared. The only reason 
not to initialize a variable where it's declared is if the initial value 
depends on some computation occurring first. 

Placement
---------
Avoid local declarations that hide declarations at higher levels. For example, 
do not declare the same variable name in an inner block: 

     int count;
     ...
     my_method () 
     {
         if (condition) 
         {
             int count = 0;     /* AVOID! */
             ...
         }
         ...
     }


Statements
==========

Simple Statements
------------------

Each line should contain at most one statement. Example: 

     argv++;       /* Correct */
     argc--;       /* Correct */
     argv++; argc--;       /* AVOID! */


Compound Statements
-------------------
Compound statements are statements that contain lists of statements enclosed 
in braces "{ statements }". See the following sections for examples. 

The enclosed statements should be indented one more level than the compound 
statement. 

The opening brace should be on a separate line, at the same indentation
level as the previous lines; the closing brace should begin a line and
be indented to the beginning of the compound statement.

Braces are used around all statements, even single statements, when they are 
part of a control structure, such as a if-else or for statement. This makes 
it easier to add statements without accidentally introducing bugs due to 
forgetting to add braces.


return Statements
-----------------
A return statement with a value should not use parentheses unless they
make the return value more obvious in some way. Example: 

     return;

     return size ();

     return (size ? size : default_size);



if, if-else, if else-if else Statements
---------------------------------------

The if-else class of statements should have the following form: 

     if (condition) 
     {
         statements;
     }

     if (condition) 
     {
         statements;
     } else {
         statements;
     }

     if (condition) 
     {
         statements;
     } else if (condition) {
         statements;
     } else {
         statements;
     }
      

Note: if statements always use braces {}. Avoid the following error-prone form:

     if (condition) /* AVOID! THIS OMITS THE BRACES {}! */
         statement;


for Statements
--------------
A for statement should have the following form: 

     for (initialization; condition; update) 
     {
         statements;
     }

An empty for statement (one in which all the work is done in the 
initialization, condition, and update clauses) should have the following form: 

     for (initialization; condition; update);

When using the comma operator in the initialization or update clause
of a for statement, avoid the complexity of using more than three variables. 
If needed, use separate statements before the for loop (for the initialization
clause) or at the end of the loop (for the update clause).


while Statements
----------------

A while statement should have the following form: 

     while (condition) 
     {
         statements;
     }

An empty while statement should have the following form: 

     while (condition);


do-while Statements
-------------------

A do-while statement should have the following form: 

     do {
         statements;
     } while (condition);


switch Statements
-----------------
A switch statement should have the following form: 

     switch (condition) {
     case ABC:
         statements;
         /* falls through */

     case DEF:
         statements;
         break;

     case XYZ:
         statements;
         break;

     default:
         statements;
         break;
     }

Every time a case falls through (doesn't include a break statement), add a 
comment where the break statement would normally be. This is shown in the 
preceding code example with the /* falls through */ comment. 

Every switch statement should include a default case. The break in the
default case is redundant, but it prevents a fall-through error if later 
another case is added. 


White Space
===========

Blank Lines
-----------

Blank lines improve readability by setting off sections of code that are 
logically related. 

One blank line should always be used in the following circumstances: 
     - between methods;
     - between the local variables in a method and its first statement; 
     - before a block or single-line comment;
     - between logical sections inside a method to improve readability.


Blank Spaces
------------

Blank spaces should be used in the following circumstances: 

      - a keyword followed by a parenthesis should be separated by a space; 
      example: 

            while (true) 
            {
                ...
            }

      - a blank space should appear after commas in argument lists; 
      - all binary operators except . should be separated from their operands
        by spaces; Blank spaces should never separate unary operators such as 
        unary minus, increment ("++"), and decrement ("--") from their 
        operands.
        example: 

         a += c + d;
         a = (a + b) / (c * d);
         
         while (d++ = s++) 
         {
             n++;
         }
         
      - the expressions in a for statement should be separated by
        blank spaces; example: 

         for (expr1; expr2; expr3)


      - casts should be followed by a blank space; examples: 

         method ((char) ch, (int) x);
         method ((int) (cp + 5), ((int) (i + 3)) + 1);



Naming Conventions
==================

Methods And Variables
---------------------

Generally, use all lower case names, with words separated by "_":
           int test_result = test_purpose_1 ();

Macros
---------
Use all upper case names, with words separated by "_":
           #define FALSE 0


Standards
=========
All code should compile with no compiler warning, when compiled
by gcc with -Wall option.

All code must be 64-bit safe - for example, do not make any
assumptions about the size of long integers and pointers.

All code must be MT-safe.  This will involve avoiding Unix
system calls which are know to be NOT MT-safe on certain
implementations, eg vfork (), sigsuspend (), etc.

*** INSERT LIST OF MT-unsafe system calls ***


Makefiles
=========
We will not use Imakefiles.

Build options such as debug level, standard include paths, etc
should be placed in a master makefile, which will be included
by each individual make file.

Any platform-specific build instructions should be moved to
seperate platform-specific override files, e.g. makefile.win32,
makefile.sparc, etc, which will be included by the master
makefile.
