<%comment%>
static void test_purpose_<%purpose_number%>()
{
    <%define%>

    bug_inf = NULL;
    test_passed_flag = 1;          
    all_test_purp_num++;
    fprintf (stderr, mark_symbol (all_test_purp_num));
    tet_printf("test case: %s, TP number: %d ", tet_pname, tet_thistest);

    <%code%>
	
    test_passed ();

    double_bug = 0;
}
