/*
 * <%object_name%> methods tests
 * Testing <%object_name%> methods
 *
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tet_api.h>
#include <gtk/gtk.h>
#include <glib.h>
#include <gmodule.h>
#include <gdk/gdkkeysyms.h>
#include <pango/pango-tabs.h>
#include <errno.h>

#include "libtestgen.h"
#include "libstr.h"
#include "libfile.h"
#include "libthr.h"

#define TEST_FAIL(str, ...) {test_failed ();tet_printf (str, __VA_ARGS__);}

/*
 * global variables
 */

int test_purp_passed_num = 0;
int all_test_purp_num = 0;
int double_bug = 0;
unsigned test_passed_flag = 1;

/*
 * function prototypes
 */
static char* get_not_created_msg (void);
static char* get_wrong_type_msg (void);
static void test_passed (void);
static void test_failed (void);
static void startup_func();
static void cleanup_func();


<%test_purposes%>

static 
char* get_not_created_msg (void)
{
        return "Widget is not created\n";
}

static 
char* get_wrong_type_msg (void)
{
        return "Created widget is of wrong type\n";
}


/*
 *  test_passed
 */
static void 
test_passed (void)
{
    if (bug_inf && !double_bug)
    {
	double_bug = 1;
    }

    if (test_passed_flag)
    {
        test_purp_passed_num++;
    }
    tet_result (TET_PASS);
}

/*
 *  test_failed
 */
static void
test_failed (void)
{
    if (bug_inf && !double_bug)
    {
	double_bug = 1;
    }

    test_passed_flag = 0;
    tet_result (TET_FAIL);
}

/*
 * startup_func
 * open display and initialize other per test case things
 */
static void 
startup_func()
{
    int argc = 1;
    char** argv = NULL;

    argv = calloc(argc,  sizeof(char*));
    argv[0] = (char*)strdup(""); 
    gtk_init(&argc, &argv);
    fprintf (stderr, "Function tests for <%object_name%> ");
}
 
/*
 * cleanup_func
 */
static void 
cleanup_func()
{

    fprintf (stderr, " %d passed", test_purp_passed_num);
    if (all_test_purp_num != test_purp_passed_num)
    {
        fprintf (stderr, " %d FAILED", all_test_purp_num - test_purp_passed_num);        
    }
    fprintf (stderr, "\n"); 
}


/* 
 *Program hooks into the tet harness 
 */
void (*tet_startup)() = startup_func;
void (*tet_cleanup)() = cleanup_func;

/*
 * TET Tests included in this file. 
 */
struct tet_testlist tet_testlist[] = {
    <%tet_hooks%>
    {NULL,0}
};
