#**********  gtk_hscale_new () *********
<FUNCTION>
Name: gtk_hscale_new
Params: GtkAdjustment *adjustment
Returns: GtkHScale*
Assertion: Creates a new GtkHScale.
Params Values: <%0%>
Expected: GtkHScale* with adjustment = <%0%>
</FUNCTION>

<DEFINE>
#define ADJUSTMENT <%0%>

GtkWidget *widget;
GtkObject *adjustment;
GtkObject *real_adjustment;
</DEFINE>       


<CODE>
adjustment = ADJUSTMENT;

if (adjustment)
{
    widget = gtk_hscale_new (GTK_ADJUSTMENT (adjustment));
} else {
    widget = gtk_hscale_new (NULL);
}


if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

if (!GTK_IS_HSCALE (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}


gtk_object_get (GTK_OBJECT (widget), "adjustment", &real_adjustment, NULL);

if (adjustment)
{
    if (((GtkAdjustment*)adjustment)->lower 
        != ((GtkAdjustment*)real_adjustment)->lower)
    {
        TEST_FAIL ("adjustment->lower = %d, real_adjustment->lower = %d",
                   ((GtkAdjustment*)adjustment)->lower, 
                   ((GtkAdjustment*)real_adjustment)->lower);
    }
    if (((GtkAdjustment*)adjustment)->upper 
        != ((GtkAdjustment*)real_adjustment)->upper)
    {
        TEST_FAIL ("adjustment->upper = %d, real_adjustment->upper = %d",
                   ((GtkAdjustment*)adjustment)->upper, 
                   ((GtkAdjustment*)real_adjustment)->upper);
    }
    if (((GtkAdjustment*)adjustment)->value
        != ((GtkAdjustment*)real_adjustment)->value)
    {
        TEST_FAIL ("adjustment->value = %d, real_adjustment->value = %d",
                   ((GtkAdjustment*)adjustment)->value, 
                   ((GtkAdjustment*)real_adjustment)->value);
    }
    if (((GtkAdjustment*)adjustment)->step_increment
        != ((GtkAdjustment*)real_adjustment)->step_increment)
    {
        TEST_FAIL ("adjustment->step_increment = %d, real_adjustment->step_increment = %d",
                   ((GtkAdjustment*)adjustment)->step_increment, 
                   ((GtkAdjustment*)real_adjustment)->step_increment);
    }
    if (((GtkAdjustment*)adjustment)->page_increment
        != ((GtkAdjustment*)real_adjustment)->page_increment)
    {
        TEST_FAIL ("adjustment->page_increment = %d, real_adjustment->page_increment = %d",
                   ((GtkAdjustment*)adjustment)->page_increment, 
                   ((GtkAdjustment*)real_adjustment)->page_increment);
    }
    if (((GtkAdjustment*)adjustment)->page_size
        != ((GtkAdjustment*)real_adjustment)->page_size)
    {
        TEST_FAIL ("adjustment->page_size = %d, real_adjustment->page_size = %d",
                   ((GtkAdjustment*)adjustment)->page_size, 
                   ((GtkAdjustment*)real_adjustment)->page_size);
    }
} else {
    if (((GtkAdjustment*)real_adjustment)->lower)
    {
        TEST_FAIL ("real_adjustment->lower = %d, should be 0", ((GtkAdjustment*)real_adjustment)->lower);
    }
    if (((GtkAdjustment*)real_adjustment)->upper)
    {
        TEST_FAIL ("real_adjustment->upper = %d, should be 0", ((GtkAdjustment*)real_adjustment)->upper);
    }
    if (((GtkAdjustment*)real_adjustment)->value)
    {
        TEST_FAIL ("real_adjustment->value = %d, should be 0", ((GtkAdjustment*)real_adjustment)->value);
    }
    if (((GtkAdjustment*)real_adjustment)->step_increment)
    {
        TEST_FAIL ("real_adjustment->step_increment = %d, should be 0", ((GtkAdjustment*)real_adjustment)->step_increment);
    }
    if (((GtkAdjustment*)real_adjustment)->page_increment)
    {
        TEST_FAIL ("real_adjustment->page_increment = %d, should be 0", ((GtkAdjustment*)real_adjustment)->page_increment);
    }
    if (((GtkAdjustment*)real_adjustment)->page_size)
    {
        TEST_FAIL ("real_adjustment->page_size = %d, should be 0", ((GtkAdjustment*)real_adjustment)->page_size);
    }
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  
gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef ADJUSTMENT
</CODE>

&gen_purpose (
              NULL
              );
&gen_purpose (
              gtk_adjustment_new (1.0, 0.0, 2.0, 0.0, 0.0, 0.0)
              );



#**********  gtk_hscale_new_with_range () *********
<FUNCTION>
Name: gtk_hscale_new_with_range
Params: gdouble min, gdouble max, gdouble step
Returns:GtkHScale*
Assertion: Creates a new horizontal scale widget that lets the user input a number between min and max (including min and max) with the increment step.
Params Values: <%0%>, <%1%>, <%2%>
Expected: GtkHScale*
</FUNCTION>

<DEFINE>
#define MIN <%0%>
#define MAX <%1%>
#define STEP <%2%>

GtkWidget *widget;
GtkObject *real_adjustment;
</DEFINE>       


<CODE>
widget = gtk_hscale_new_with_range (MIN, MAX, STEP);


if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

if (!GTK_IS_HSCALE (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}


gtk_object_get (GTK_OBJECT (widget), "adjustment", &real_adjustment, NULL);

if (real_adjustment)
{
    if (((GtkAdjustment*)real_adjustment)->lower != MIN)
    {
        TEST_FAIL ("real_adjustment->lower = %d, MIN = %d",
                   ((GtkAdjustment*)real_adjustment)->lower, MIN);
    }
    if (((GtkAdjustment*)real_adjustment)-> upper!= MAX)
    {
        TEST_FAIL ("real_adjustment->upper = %d, MAX = %d",
                   ((GtkAdjustment*)real_adjustment)->upper, MAX);
    }
    if (((GtkAdjustment*)real_adjustment)->step_increment != STEP)
    {
        TEST_FAIL ("real_adjustment->step_increment = %d, STEP = %d",
                   ((GtkAdjustment*)real_adjustment)->step_increment, STEP);
    } 
} else {
    TEST_FAIL ("GtkHScale was created without adjustment", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  
gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef MIN
#undef MAX
#undef STEP
</CODE>

&gen_purpose (
              0.0
              1.0
              1.0
              );

&gen_purpose (
              0.0
              100.0
              100.0
              );

&gen_purpose (
              0
              100
              50.49
              );

&gen_purpose (
              0
              G_MAXDOUBLE
              G_MAXDOUBLE / 2
              );

