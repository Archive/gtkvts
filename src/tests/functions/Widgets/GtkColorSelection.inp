#not tested:
#  gtk_color_selection_set_update_policy () - deprecated (see implementation)
#

<GLOBAL>
void global_change_palette_hook_1 (const GdkColor *colors, gint n_colors)
{
}

void global_change_palette_hook_2 (const GdkColor *colors, gint n_colors)
{
}

void global_change_palette_with_screen_hook_1 (GdkScreen *screen, const GdkColor *colors, gint n_colors)
{
}

void global_change_palette_with_screen_hook_2 (GdkScreen *screen, const GdkColor *colors, gint n_colors)
{
}
</GLOBAL>

#**********  gtk_color_selection_new () **********

<FUNCTION>
Name: gtk_color_selection_new
Params: void
Returns: GtkWidget*
Assertion: Creates a new GtkColorSelection.
Params Values: void
Expected: GtkColorSelection*
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

</CODE>

&gen_purpose (
             );



#**********  gtk_color_selection_set_has_opacity_control () **********
#**********  gtk_color_selection_get_has_opacity_control () **********

<FUNCTION>
Name: gtk_color_selection_set_has_opacity_control
Params: GtkColorSelection *colorsel, gboolean has_opacity
Returns: void
Assertion: Sets the colorsel to use or not use opacity.
Params Values: a newly created GtkColorSelection*, <%0%>
Expected: void

Name: gtk_color_selection_get_has_opacity_control
Params: GtkColorSelection *colorsel
Returns: gboolean
Assertion: Determines whether the colorsel has an opacity control.
Params Values: a newly created GtkColorSelection*
Expected: gboolean
</FUNCTION>
 
<DEFINE>
#define HAS_OPACITY <%0%>

GtkWidget *widget;
gboolean has_opacity;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_color_selection_set_has_opacity_control (GTK_COLOR_SELECTION (widget), HAS_OPACITY);
has_opacity = gtk_color_selection_get_has_opacity_control(GTK_COLOR_SELECTION (widget));
if (has_opacity != HAS_OPACITY)
{
    TEST_FAIL ("has_opacity = %d, should be %d", has_opacity, HAS_OPACITY);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef HAS_OPACITY 
</CODE>

&gen_purpose(
             TRUE
            );

&gen_purpose(
             FALSE
            );



#**********  gtk_color_selection_set_has_palette () **********
#**********  gtk_color_selection_get_has_palette () **********

<FUNCTION>
Name: gtk_color_selection_set_has_palette
Params: GtkColorSelection *colorsel, gboolean has_palette
Returns: void
Assertion: Shows and hides the palette based upon the value of has_palette.
Params Values: a newly created GtkColorSelection*, <%0%>
Expected: void

Name: gtk_color_selection_get_has_palette
Params: GtkColorSelection *colorsel
Returns: gboolean
Assertion: Determines whether the color selector has a color palette.
Params Values: a newly created GtkColorSelection*
Expected: gboolean
</FUNCTION>
 
<DEFINE>
#define HAS_PALETTE <%0%>

GtkWidget *widget;
gboolean has_palette;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_color_selection_set_has_palette (GTK_COLOR_SELECTION (widget), HAS_PALETTE);
has_palette = gtk_color_selection_get_has_palette (GTK_COLOR_SELECTION (widget));
if (has_palette != HAS_PALETTE)
{
    TEST_FAIL ("has_palette = %d, should be %d", has_palette, HAS_PALETTE);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef HAS_PALETTE
</CODE>

&gen_purpose(
             TRUE
            );

&gen_purpose(
             FALSE
            );



#**********  gtk_color_selection_set_current_alpha () **********
#**********  gtk_color_selection_get_current_alpha () **********
<FUNCTION>
Name: gtk_color_selection_set_current_alpha
Params: GtkColorSelection *colorsel, guint16 alpha
Returns: void
Assertion: Sets the current opacity to be alpha.
Params Values: a newly created GtkColorSelection*, <%0%>
Expected: void

Name: gtk_color_selection_get_current_alpha
Params: GtkColorSelection *colorsel
Returns: guint16
Assertion:  Returns the current alpha value.
Params Values: a newly created GtkColorSelection*
Expected: guint16 
</FUNCTION>
 
<DEFINE>
#define ALPHA <%0%>

GtkWidget *widget;
guint16 alpha;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_color_selection_set_current_alpha (GTK_COLOR_SELECTION (widget), ALPHA);
alpha = gtk_color_selection_get_current_alpha (GTK_COLOR_SELECTION (widget));
if (alpha != ALPHA)
{
    TEST_FAIL ("alpha = %d, should be %d", alpha, ALPHA);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef ALPHA
</CODE>

&gen_purpose(
             0
            );

&gen_purpose(
             5
            );

&gen_purpose(
             20001
            );

&gen_purpose(
             30001
            );

&gen_purpose(
             65535
            );



#**********  gtk_color_selection_set_current_alpha () **********
<FUNCTION>
Name: gtk_color_selection_set_current_alpha
Params: GtkColorSelection *colorsel, guint16 alpha
Returns: void
Assertion: The first time this is called, it will also set the original opacity to be alpha too.
Params Values: a newly created GtkColorSelection*, <%0%>
Expected: void
</FUNCTION>
 
<DEFINE>
#define ALPHA <%0%>

GtkWidget *widget;
guint16 alpha;
guint16 prev_alpha;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_color_selection_set_current_alpha (GTK_COLOR_SELECTION (widget), ALPHA);
alpha = gtk_color_selection_get_current_alpha (GTK_COLOR_SELECTION (widget));
if (alpha != ALPHA)
{
    TEST_FAIL ("alpha = %d, should be %d", alpha, ALPHA);
}

prev_alpha = gtk_color_selection_get_previous_alpha (GTK_COLOR_SELECTION (widget));
if (prev_alpha != ALPHA)
{
    TEST_FAIL ("prev_alpha = %d, should be %d", prev_alpha, ALPHA);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef ALPHA
</CODE>

&gen_purpose(
             0
            );

&gen_purpose(
             5
            );

&gen_purpose(
             20001
            );

&gen_purpose(
             30001
            );

&gen_purpose(
             65535
            );



#**********  gtk_color_selection_set_previous_alpha () **********
#**********  gtk_color_selection_get_previous_alpha () **********

<FUNCTION>
Name: gtk_color_selection_set_previous_alpha
Params: GtkColorSelection *colorsel, guint16 alpha
Returns: void
Assertion: Sets the 'previous' alpha to be alpha.
Params Values: a newly created GtkColorSelection*, <%0%>
Expected: void

Name: gtk_color_selection_get_previous_alpha
Params: GtkColorSelection *colorsel
Returns: guint16 
Assertion: Returns the previous alpha value
Params Values: a newly created GtkColorSelection*
Expected: guint16 
</FUNCTION>
 
<DEFINE>
#define ALPHA <%0%>

GtkWidget *widget;
guint16 alpha;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_color_selection_set_previous_alpha (GTK_COLOR_SELECTION (widget), ALPHA);
alpha = gtk_color_selection_get_previous_alpha (GTK_COLOR_SELECTION (widget));
if (alpha != ALPHA)
{
    TEST_FAIL ("alpha = %d, should be %d", alpha, ALPHA);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef ALPHA
</CODE>

&gen_purpose(
             0
            );

&gen_purpose(
             1
            );

&gen_purpose(
             20001
            );

&gen_purpose(
             30001
            );

&gen_purpose(
             65535
            );


#**********  gtk_color_selection_set_current_color () *********
#**********  gtk_color_selection_get_current_color () **********

<FUNCTION>
Name: gtk_color_selection_set_current_color
Params: GtkColorSelection *colorsel, GdkColor *color 
Returns: void
Assertion: Sets the current color to be color.
Params Values: a newly created GtkColorSelection*, GdkColor*
Expected: void

Name: gtk_color_selection_get_current_color
Params: GtkColorSelection *colorsel, GdkColor *color
Returns: void
Assertion: Sets color to be the current color in the GtkColorSelection widget.
Params Values: a newly created GtkColorSelection*, GdkColor*
Expected: void
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
GdkColor color;
GdkColor real_color;

#define RED <%0%>
#define GREEN <%1%>
#define BLUE <%2%>
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

color.pixel = 0;
color.red = RED;
color.green = GREEN;
color.blue = BLUE;

gtk_color_selection_set_current_color (GTK_COLOR_SELECTION (widget), &color);

gtk_color_selection_get_current_color (GTK_COLOR_SELECTION (widget), &real_color);
if (real_color.red != color.red)
{
    TEST_FAIL ("real_color.red = %d, should be %d", real_color.red, color.red);
}
if (real_color.green != color.green)
{
    TEST_FAIL ("real_color.green = %d, should be %d", real_color.green, color.green);
}
if (real_color.blue != color.blue)
{
    TEST_FAIL ("real_color.blue = %d, should be %d", real_color.blue, color.blue);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef RED
#undef GREEN
#undef BLUE
</CODE>

&gen_purpose(
             0
             0
             0
             );

&gen_purpose(
             1
             1
             1
            );

&gen_purpose(
             20001
             20001
             20001
            );

&gen_purpose(
             30001
             30001
             30001
            );

&gen_purpose(
             65535
             65535
             65535
            );



#**********  gtk_color_selection_set_current_color () *********
#**********  gtk_color_selection_set_previous_color () *********
<FUNCTION>
Name: gtk_color_selection_set_current_color
Params: GtkColorSelection *colorsel, GdkColor *color 
Returns: void
Assertion: The first time this is called, it will also set the original color to be color too.
Params Values: a newly created GtkColorSelection*, GdkColor*
Expected: void

Name: gtk_color_selection_set_previous_color
Params: GtkColorSelection *colorsel, GdkColor *color 
Returns: void
Assertion: Calling gtk_color_selection_set_current_color() will also set this color the first time it is called.
Params Values: a newly created GtkColorSelection*, GdkColor*
Expected: void
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
GdkColor color;
GdkColor real_color;
GdkColor real_prev_color;

#define RED <%0%>
#define GREEN <%1%>
#define BLUE <%2%>
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

color.pixel = 0;
color.red = RED;
color.green = GREEN;
color.blue = BLUE;

gtk_color_selection_set_current_color (GTK_COLOR_SELECTION (widget), &color);

gtk_color_selection_get_current_color (GTK_COLOR_SELECTION (widget), &real_color);
if (real_color.red != color.red)
{
    TEST_FAIL ("real_color.red = %d, should be %d", real_color.red, color.red);
}
if (real_color.green != color.green)
{
    TEST_FAIL ("real_color.green = %d, should be %d", real_color.green, color.green);
}
if (real_color.blue != color.blue)
{
    TEST_FAIL ("real_color.blue = %d, should be %d", real_color.blue, color.blue);
}

gtk_color_selection_get_previous_color (GTK_COLOR_SELECTION (widget), 
                                        &real_prev_color);
if (real_prev_color.red != color.red)
{
    TEST_FAIL ("real_prev_color.red = %d, should be %d", 
               real_prev_color.red, color.red);
}
if (real_prev_color.green != color.green)
{
    TEST_FAIL ("real_prev_color.green = %d, should be %d", 
               real_prev_color.green, color.green);
}
if (real_prev_color.blue != color.blue)
{
    TEST_FAIL ("real_prev_color.blue = %d, should be %d", 
               real_prev_color.blue, color.blue);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef RED
#undef GREEN
#undef BLUE
</CODE>

&gen_purpose(
             0
             0
             0
            );

&gen_purpose(
             1
             1
             1
            );

&gen_purpose(
             20001
             20001
             20001
            );

&gen_purpose(
             30001
             30001
             30001
            );

&gen_purpose(
             65535
             65535
             65535
            );



#**********  gtk_color_selection_set_previous_color () *********
#**********  gtk_color_selection_get_previous_color () **********
<FUNCTION>
Name: gtk_color_selection_set_previous_color
Params: GtkColorSelection *colorsel, GdkColor *color 
Returns: void
Assertion: Sets the 'previous' color to be color.
Params Values: a newly created GtkColorSelection*, GdkColor*
Expected: void

Name: gtk_color_selection_get_previous_color
Params: GtkColorSelection *colorsel, GdkColor *color
Returns: void
Assertion: Fills color in with the original color value.
Params Values: a newly created GtkColorSelection*, GdkColor*
Expected: void
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
GdkColor color;
GdkColor real_color;

#define RED <%0%>
#define GREEN <%1%>
#define BLUE <%2%>
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

color.pixel = 0;
color.red = RED;
color.green = GREEN;
color.blue = BLUE;

gtk_color_selection_set_previous_color (GTK_COLOR_SELECTION (widget), &color);

gtk_color_selection_get_previous_color (GTK_COLOR_SELECTION (widget), &real_color);
if (real_color.red != color.red)
{
    TEST_FAIL ("real_color.red = %d, should be %d", real_color.red, color.red);
}
if (real_color.green != color.green)
{
    TEST_FAIL ("real_color.green = %d, should be %d", real_color.green, color.green);
}
if (real_color.blue != color.blue)
{
    TEST_FAIL ("real_color.blue = %d, should be %d", real_color.blue, color.blue);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef RED
#undef GREEN
#undef BLUE
</CODE>

&gen_purpose(
             0
             0
             0
            );

&gen_purpose(
             1
             1
             1
            );

&gen_purpose(
             20001
             20001
             20001
            );

&gen_purpose(
             30001
             30001
             30001
            );

&gen_purpose(
             65535
             65535
             65535
            );



#**********  gtk_color_selection_palette_from_string () **********
#**********  gtk_color_selection_palette_to_string () **********
<FUNCTION>
Name: gtk_color_selection_palette_from_string 
Params: const gchar *str,GdkColor **colors,gint *n_colors
Returns: gboolean
Assertion: Parses a color palette string; the string is a colon-separated list of color names readable by gdk_color_parse().
Params Values: gchar*,GdkColor**,gint*
Expected: gboolean

Name: gtk_color_selection_palette_to_string 
Params: const GdkColor *colors,gint n_colors
Returns: gchar*
Assertion: Encodes a palette as a string, useful for persistent storage.
Params Values: GdkColor*,<%3%>
Expected: gchar*
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
GdkColor color;
GdkColor* col;
gchar* str;
gint num;
gboolean res = FALSE;

#define RED <%0%>
#define GREEN <%1%>
#define BLUE <%2%>
#define NUM <%3%>
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();
color.pixel = 0;
color.red = RED;
color.green = GREEN;
color.blue = BLUE;
num = NUM;

str = gtk_color_selection_palette_to_string (&color,num);
col = &color;
res = gtk_color_selection_palette_from_string (str,&col,&num);
if (!res) 
{
    TEST_FAIL ("Palette wasn't successfully parsed",NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef RED
#undef GREEN
#undef BLUE
#undef NUM
</CODE>

&gen_purpose(
             0
             0
             0
             1
            );

&gen_purpose(
             0
             0
             0
             100
            );

&gen_purpose(
             1
             1
             1
             1
            );

&gen_purpose(
             1
             1
             1
             100
            );

&gen_purpose(
             20001
             20001
             20001
             1
            );

&gen_purpose(
             20001
             20001
             20001
             100
            );

&gen_purpose(
             30001
             30001
             30001
             1
            );

&gen_purpose(
             30001
             30001
             30001
             100
            );

&gen_purpose(
             65535
             65535
             65535
             1
            );

&gen_purpose(
             65535
             65535
             65535
             100
            );



#**********  gtk_color_selection_is_adjusting () **********
<FUNCTION>
Name: gtk_color_selection_is_adjusting
Params: void
Returns: GtkWidget*
Assertion: Gets the current state of the colorsel.
Params Values: void
Expected: FALSE
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
gboolean is_adjusting;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

is_adjusting = gtk_color_selection_is_adjusting (GTK_COLOR_SELECTION (widget));
if (is_adjusting)
{
    TEST_FAIL ("is_adjusting is TRUE, should be FALSE", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

</CODE>

&gen_purpose (
             );



#**********  gtk_color_selection_change_palette_hook () *********
<FUNCTION>
Name: gtk_color_selection_set_change_palette_hook
Params: GtkColorSelectionChangePaletteFunc func
Returns: GtkColorSelectionChangePaletteFunc
Assertion: Installs a global function to be called whenever the user tries to modify the palette in a color selection. 
Params Values: &global_change_palette_hook_1 in the first call, global_change_palette_hook_2 in the second call
Expected: global_change_palette_hook_1 in the second call
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
GtkColorSelectionChangePaletteFunc old_func;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_color_selection_set_change_palette_hook (global_change_palette_hook_1);
old_func = gtk_color_selection_set_change_palette_hook (global_change_palette_hook_2);

if (old_func != global_change_palette_hook_1)
{
    TEST_FAIL ("old_func != global_change_palette_hook_1", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));
</CODE>

&gen_purpose(
            );


#**********  gtk_color_selection_change_palette_with_screen_hook () *********
<FUNCTION>
Name: gtk_color_selection_set_change_palette_with_screen_hook
Params: GtkColorSelectionChangePaletteWithScreenFunc func
Returns: GtkColorSelectionChangePaletteFunc func
Assertion: Installs a global function to be called whenever the user tries to modify the palette in a color selection.
Params Values: &global_change_palette_with_screen_hook_1 in the first call, global_change_palette_with_screen_hook_2 in the second call
Expected: global_change_palette_with_screen_hook_1 in the second call
</FUNCTION>
 
<DEFINE>
GtkWidget *widget;
GtkColorSelectionChangePaletteWithScreenFunc old_func;
</DEFINE>       

<CODE>
widget = gtk_color_selection_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_COLOR_SELECTION (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_color_selection_set_change_palette_with_screen_hook 
    (global_change_palette_with_screen_hook_1);
old_func = gtk_color_selection_set_change_palette_with_screen_hook 
    (global_change_palette_with_screen_hook_2);

if (old_func != global_change_palette_with_screen_hook_1)
{
    TEST_FAIL ("old_func != global_change_palette_with_screen_hook_1", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));
</CODE>

&gen_purpose(
            );
