#not tested:
#    - gtk_statusbar_get_context_id - just call this method tested now


#**********  gtk_statusbar_new  **********

<FUNCTION>
Name: gtk_statusbar_new
Params: void
Returns: GtkWidget*
Assertion: Creates a new GtkStatusbar ready for messages.
Params Values: void 
Expected: GtkStatusBar*
</FUNCTION>

 
<DEFINE>
GtkWidget *widget;
</DEFINE>	


<CODE>
widget = gtk_statusbar_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

if (!GTK_IS_STATUSBAR (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

</CODE>

&gen_purpose(
            );


#**********  gtk_statusbar_set_has_resize_grip  **********
#**********  gtk_statusbar_get_has_resize_grip  **********

<FUNCTION>
Name: gtk_statusbar_set_has_resize_grip
Params: GtkStatusBar *statusbar, gboolean setting
Returns: void
Assertion: Sets whether the statusbar has a resize grip.
Params Values: newly created GtkStatusBar*, <%0%>
Expected: void

Name: gtk_statusbar_get_has_resize_grip
Params: GtkStatusBar *statusbar
Returns: gboolean
Assertion: Returns whether the statusbar has a resize grip.
Params Values: newly created GtkStatusBar*
Expected: <%0%>
</FUNCTION>

<DEFINE>
#define SETTING <%0%>

GtkWidget *widget;
</DEFINE>	


<CODE>
widget = gtk_statusbar_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_STATUSBAR (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_statusbar_set_has_resize_grip (GTK_STATUSBAR (widget), SETTING);

if (gtk_statusbar_get_has_resize_grip(GTK_STATUSBAR (widget)) != SETTING)
{
    TEST_FAIL("Setting =  %d, should be %d\n", gtk_statusbar_get_has_resize_grip(GTK_STATUSBAR (widget)), SETTING);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef SETTING
</CODE>

&gen_purpose(
             TRUE
            );

&gen_purpose(
             FALSE
            );


#**********  gtk_statusbar_set_has_resize_grip  **********
<FUNCTION>
Name: gtk_statusbar_set_has_resize_grip
Params: GtkStatusBar *statusbar, gboolean setting
Returns: void
Assertion: TRUE by default.
Params Values: newly created GtkStatusBar*, <%0%>
Expected: void
</FUNCTION>

<DEFINE>
GtkWidget *widget;
</DEFINE>	


<CODE>
widget = gtk_statusbar_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_STATUSBAR (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

if (gtk_statusbar_get_has_resize_grip(GTK_STATUSBAR (widget)) != TRUE)
{
    TEST_FAIL("default setting =  %d, should be %d\n", gtk_statusbar_get_has_resize_grip(GTK_STATUSBAR (widget)), TRUE);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

</CODE>

&gen_purpose(
            );


#**********  gtk_statusbar_get_context_id  **********

<FUNCTION>
Name: gtk_statusbar_get_context_id
Params: GtkStatusbar *statusbar, const gchar *context_description
Returns:guint
Assertion: Returns a new context identifier, given a description of the actual context.
Params Values: newly created GtkStatusbar*, <%0%> 
Expected: new context identifier
</FUNCTION>

<DEFINE>
#define CONTEXT_DESCRIPTION <%0%>

GtkWidget *widget;
</DEFINE>	


<CODE>
widget = gtk_statusbar_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_STATUSBAR (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

gtk_statusbar_get_context_id (GTK_STATUSBAR (widget), CONTEXT_DESCRIPTION);

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef CONTEXT_DESCRIPTION
</CODE>

&gen_purpose(
            ""
            );

&gen_purpose(
            " "
            );

&gen_purpose(
            "   "
            );

&gen_purpose(
            "Label String"
            );



#**********  gtk_statusbar_push  **********

<FUNCTION>
Name: gtk_statusbar_push
Params: GtkStatusbar *statusbar, guint context_id, const gchar *text
Returns:guint
Assertion: Pushes a new message onto a statusbar`s stack.
Params Values: newly created GtkStatusbar*, the message`s context id, as returned by gkt_statusbar_get_context_id(),<%1%> 
Expected: new message identifier
</FUNCTION>

<DEFINE>
#define CONTEXT_DESCRIPTION <%0%>
#define TEXT <%1%>
#define N_MESSAGES <%2%>

GtkWidget *widget;
guint context_id;
guint message_id = 0;
guint save_message_id = 0;
guint i;
</DEFINE>	

<CODE>
widget = gtk_statusbar_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_STATUSBAR (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

context_id = gtk_statusbar_get_context_id (GTK_STATUSBAR (widget), CONTEXT_DESCRIPTION);

save_message_id = gtk_statusbar_push (GTK_STATUSBAR (widget), context_id, TEXT);
for (i=0; i<N_MESSAGES; i++)
{
    message_id = gtk_statusbar_push (GTK_STATUSBAR (widget), context_id, TEXT);
    if (save_message_id == message_id)
    {
        TEST_FAIL("i = %i, message_id = save_message_id = %i\n", i, message_id);
    }
    save_message_id = message_id; 
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef CONTEXT_DESCRIPTION
#undef TEXT
#undef N_MESSAGES
</CODE>

&gen_purpose(
            ""
            ""
            1
            );

&gen_purpose(
            ""
            ""
            2
            );

&gen_purpose(
            " "
            " "
            10 
            );

&gen_purpose(
            "   "
            "   "
            1000
            );
<DISABLED>
&gen_purpose(
            "Label String"
            "Label String"
            G_MAXINT
            );
</DISABLED>

#**********  gtk_statusbar_pop  **********

<FUNCTION>
Name: gtk_statusbar_pop
Params: GtkStatusbar *statusbar, guint context_id
Returns: void
Assertion: Removes the message at the top of a GtkStatusBar`s stack.
Params Values: newly created GtkStatusbar*, the message`s context id, as returned by gkt_statusbar_get_context_id() 
Expected: void
</FUNCTION>

<DEFINE>
#define CONTEXT_DESCRIPTION <%0%>
#define TEXT <%1%>
#define N_MESSAGES <%2%>
#define N_POPS <%3%>

GtkWidget *widget;
guint context_id, message_id, save_message_id;
guint i;
</DEFINE>	

<CODE>
widget = gtk_statusbar_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_STATUSBAR (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

context_id = gtk_statusbar_get_context_id (GTK_STATUSBAR (widget), CONTEXT_DESCRIPTION);

save_message_id = gtk_statusbar_push (GTK_STATUSBAR (widget), context_id, TEXT);
for (i=0; i<N_MESSAGES; i++)
{
    message_id = gtk_statusbar_push (GTK_STATUSBAR (widget), context_id, TEXT);
    if (save_message_id == message_id)
    {
        TEST_FAIL("i = %i, message_id = save_message_id = %i\n", i, message_id);
    }
    save_message_id = message_id;
}

for (i=0; i<N_POPS; i++)
{
    gtk_statusbar_pop (GTK_STATUSBAR (widget), context_id);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef CONTEXT_DESCRIPTION
#undef TEXT
#undef N_MESSAGES
#undef N_POPS
</CODE>

&gen_purpose(
            ""
            ""
            1
            1
            );

&gen_purpose(
            ""
            ""
            2
            3
            );

&gen_purpose(
            " "
            " "
            10 
            7
            );

&gen_purpose(
            "   "
            "   "
            1000
            1010
            );
<DISABLED>
&gen_purpose(
            "Label String"
            "Label String"
            G_MAXINT
            G_MAXINT
            );
</DISABLED>

#**********  gtk_statusbar_remove  **********

<FUNCTION>
Name: gtk_statusbar_remove
Params: GtkStatusbar *statusbar, guint context_id, guint message_id
Returns: void
Assertion: Forces the removal of a message from a statusbar`s stack. 
Params Values: newly created GtkStatusbar*, the message`s context id, as returned by gkt_statusbar_get_context_id() 
Expected: void
</FUNCTION>

<DEFINE>
#define CONTEXT_DESCRIPTION <%0%>
#define TEXT <%1%>
#define N_MESSAGES <%2%>
#define REMOVE_MESSAGE_INDEX <%3%>

GtkWidget *widget;
guint context_id, message_id, save_message_id;
guint i;
</DEFINE>	

<CODE>
widget = gtk_statusbar_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
}

if (!GTK_IS_STATUSBAR (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

context_id = gtk_statusbar_get_context_id (GTK_STATUSBAR (widget), CONTEXT_DESCRIPTION);

for (i=0; i<N_MESSAGES; i++)
{
    message_id = gtk_statusbar_push (GTK_STATUSBAR (widget), context_id, TEXT);
    if (i == REMOVE_MESSAGE_INDEX)
    {
        save_message_id = message_id;
    }
}

gtk_statusbar_remove (GTK_STATUSBAR (widget), context_id, save_message_id);

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);   
/******* -------------  *******/ 

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef CONTEXT_DESCRIPTION
#undef TEXT
#undef N_MESSAGES
#undef REMOVE_MESSAGE_INDEX
</CODE>

&gen_purpose(
            ""
            ""
            1
            0
            );

&gen_purpose(
            ""
            ""
            2
            1
            );

&gen_purpose(
            " "
            " "
            10 
            7
            );

&gen_purpose(
            "   "
            "   "
            1000
            999
            );
<DISABLED>
&gen_purpose(
            "Label String"
            "Label String"
            G_MAXINT
            (G_MAXINT - 1)
            );
</DISABLED>
