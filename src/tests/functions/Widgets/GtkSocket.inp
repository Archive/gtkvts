<GLOBAL>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
</GLOBAL>


#**********  gtk_socket_new ()  **********

<FUNCTION>
Name: gtk_socket_new 
Params: void
Returns: GtkWidget*
Assertion: Create a new empty GtkSocket.
Params Values: void
Expected: GtkSocket*
</FUNCTION>

 
<DEFINE>
GtkWidget *widget;
</DEFINE>	


<CODE>
widget = gtk_socket_new ();

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

if (!GTK_IS_SOCKET (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

</CODE>


&gen_purpose(
            );


            

#**********  gtk_socket_add_id  **********

<FUNCTION>
Name: gtk_socket_add_id
Params: GtkSocket *socket, GdkNativeWindow id
Returns: void
Assertion: Adds an XEMBED client, such as a GtkPlug, to the GtkSocket.
Params Values: GtkSocket *socket, GdkNativeWindow id
Expected: void
</FUNCTION>

 
<DEFINE>
GtkWidget* window;
GtkWidget* widget;
GtkWidget* plug;
gint  pfds[2];
gchar buff[30];
GdkNativeWindow id;
</DEFINE>	


<CODE>


window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
if (!window)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

widget = gtk_socket_new ();
if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

if (!GTK_IS_SOCKET (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_container_add (GTK_CONTAINER (window), widget);

gtk_widget_realize (widget);

plug = gtk_plug_new ((GdkNativeWindow) 0);
if (!plug)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}
gtk_socket_add_id (GTK_SOCKET (widget), gtk_plug_get_id (GTK_PLUG (plug)));

</CODE>

&gen_purpose(
            );



#**********  gtk_socket_get_id  **********

<FUNCTION>
Name: gtk_socket_get_id
Params: GtkSocket *socket, GdkNativeWindow id
Returns: GdkNativeWindow
Assertion: Gets the window ID of a GtkSocket widget, which can then be used to create a client embedded inside the socket, for instance with gtk_plug_new().
Params Values: GtkSocket *socket, GdkNativeWindow id
Expected: GdkNativeWindow
</FUNCTION>

 
<DEFINE>
GtkWidget* window;
GtkWidget* widget;
GtkWidget* plug;
gint  pfds[2];
gchar buff[30];
GdkNativeWindow id;
</DEFINE>	


<CODE>

pipe (pfds);
window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
if (!window)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

widget = gtk_socket_new ();
if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

if (!GTK_IS_SOCKET (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_container_add (GTK_CONTAINER (window), widget);
gtk_widget_realize (widget);

if (fork())
{
    id = gtk_socket_get_id (GTK_SOCKET (widget));
    write (pfds[1], &id, sizeof (GdkNativeWindow));

    wait(NULL);
    close (pfds[1]);

} else {

    read (pfds[0], &id, sizeof (GdkNativeWindow));
    plug = gtk_plug_new (id);

    if (!plug)
    {
        TEST_FAIL (get_not_created_msg(), NULL); 
        return;
    }
    if (!GTK_PLUG (plug)->socket_window)
    {
        TEST_FAIL ("plug->socket_window is NULL", NULL); 
    }
    close (pfds[0]);
    exit (0);
}

</CODE>

&gen_purpose(
            );

