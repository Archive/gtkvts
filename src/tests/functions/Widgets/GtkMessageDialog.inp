<GLOBAL>
#include "librobot.h"

static gint global_count;

void  
global_response (GtkWidget *widget, gint arg1, gpointer data)
{
    global_count++;
}  

void  
global_click (GtkWidget *widget, gpointer data)
{
    GList* list = NULL;

    if (!GTK_WIDGET_REALIZED (widget)) return;
    
    list = gtk_container_get_children (GTK_CONTAINER (GTK_DIALOG (widget)->action_area));
    if (!list)
    {
        gtk_dialog_response (GTK_DIALOG (widget), 0);
        return;
    }
    while (list)
    {
        if (GTK_IS_BUTTON (list->data)) 
        {
            gtk_button_clicked (GTK_BUTTON (list->data));
        }
        list = list->next;
    }
    g_list_free (list);
}  



</GLOBAL>


#*********  gtk_message_dialog_new  **********

<FUNCTION>
Name: gtk_message_dialog_new
Params: GtkWindow *parent, GtkDialogFlags flags, GtkMessageType type, GtkButtonsType buttons,  const gchar *message_format
Returns: GtkWidget*
Assertion: Creates a new message dialog, which is a simple dialog with an icon indicating the dialog type (error, warning, etc.) and some text the user may want to see.
Params Values: <%0%>, <%1%>, <%2%>, <%3%>, <%4%>
Expected: GtkMessageDialog*

Name: gtk_message_dialog_new
Params: GtkWindow *parent, GtkDialogFlags flags, GtkMessageType type, GtkButtonsType buttons,  const gchar *message_format
Returns: GtkWidget*
Assertion:  When the user clicks a button a "response" signal is emitted with response IDs from GtkResponseType. 
Params Values: <%0%>, <%1%>, <%2%>, <%3%>, <%4%>
Expected: GtkMessageDialog*
</FUNCTION>
 
<DEFINE>
#define  PARENT   <%0%>
#define  FLAGS    <%1%>
#define  TYPE     <%2%>
#define  BUTTONS  <%3%>
#define  FORMAT   <%4%>

GtkWidget* widget;
GtkWidget* button;
GtkWindow* real_parent = NULL;
GtkWindow* parent_window = NULL;
</DEFINE>


<CODE>
global_count = 0;

if (PARENT)
{
    parent_window = GTK_WINDOW(PARENT);
} else {
    parent_window = NULL;
}

widget = gtk_message_dialog_new (
    parent_window, 
    FLAGS, 
    TYPE, 
    BUTTONS, 
    FORMAT);

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
}

if (!GTK_IS_MESSAGE_DIALOG (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

g_signal_connect (G_OBJECT (widget), "response", GTK_SIGNAL_FUNC (global_response), NULL);  

/******* VISUALISATION *******/
TEST_VISUALIZE_AND_DO (widget, global_click);   
/******* -------------  *******/ 

if (!global_count)
{
    TEST_FAIL ("global_count = %i", global_count);
}

gtk_widget_destroy (widget);

#undef  PARENT  
#undef  FLAGS   
#undef  BUTTONS 
#undef  TYPE
#undef  FORMAT 
</CODE>


&gen_purpose(
             NULL 
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_OK 
             "Info"
            );

&gen_purpose(
             NULL 
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_OK 
             NULL
            );

&gen_purpose(
             NULL 
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_ERROR 
             GTK_BUTTONS_OK  
             "Error loading file  xxx.err" 
             );

&gen_purpose(
             NULL 
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_WARNING 
             GTK_BUTTONS_OK  
             "Warning"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_QUESTION 
             GTK_BUTTONS_OK  
             "Is it question ?"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_INFO
             GTK_BUTTONS_NONE
             "None"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_INFO  
             GTK_BUTTONS_CLOSE 
             "Close"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_CANCEL 
             "Cancel"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_YES_NO 
             "YesNo"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_DESTROY_WITH_PARENT 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_OK_CANCEL 
             "OkCancel"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_OK 
             "Info"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_OK 
             NULL
            );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_ERROR 
             GTK_BUTTONS_OK  
             "Error loading file" 
            );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_WARNING 
             GTK_BUTTONS_OK  
             "Warning"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_QUESTION 
             GTK_BUTTONS_OK  
             "Is it question ?"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_NONE  
             "None"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL
             GTK_MESSAGE_INFO
             GTK_BUTTONS_CLOSE 
             "Close"
            );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_CANCEL  
             "Cancel"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_YES_NO 
             "YesNo"
             );

&gen_purpose(
             gtk_window_new(GTK_WINDOW_TOPLEVEL)
             GTK_DIALOG_MODAL 
             GTK_MESSAGE_INFO 
             GTK_BUTTONS_OK_CANCEL 
             "OkCancel"
            );
