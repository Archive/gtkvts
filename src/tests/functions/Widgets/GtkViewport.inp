#********* gtk_viewport_new  *********

<FUNCTION>
Name:  gtk_viewport_new
Params: GtkAdjustment *hadjustment, GtkAdjustment *vadjustment,
Returns: GtkWidget*
Assertion: Creates a new GtkViewport with the given adjustments.
Params Values: <%0%>, <%1%>
Expected: GtkViewport*
</FUNCTION>


<DEFINE>
#define    HADJ <%0%>
#define    VADJ <%1%>

GtkWidget* widget;
GtkAdjustment* hadj;
GtkAdjustment* vadj;
GtkAdjustment* ha;
GtkAdjustment* va;
</DEFINE>


<CODE>
hadj = GTK_ADJUSTMENT (HADJ);
vadj = GTK_ADJUSTMENT (VADJ);
widget = gtk_viewport_new (hadj, vadj);

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
} 

if (!GTK_IS_VIEWPORT (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_object_get(GTK_OBJECT(widget), "hadjustment", &ha, NULL);
if (hadj != ha)
{
    TEST_FAIL("hadjustment fails", NULL);
}

gtk_object_get(GTK_OBJECT(widget), "vadjustment", &va, NULL);
if (vadj != va)
{
    TEST_FAIL("vadjustment fails", NULL);
}

/*  gtk_object_destroy (GTK_OBJECT (ha)); */
/*  gtk_object_destroy (GTK_OBJECT (va)); */

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef  HADJ
#undef  VADJ
</CODE>

<DISABLED>
&gen_purpose(
             NULL
             NULL
             );
</DISABLED>
&gen_purpose(
             gtk_adjustment_new (10.0, 1.0, 100.0, 5.0, 20.0, 50.0) 
             gtk_adjustment_new (10.0, 1.0, 100.0, 5.0, 20.0, 50.0) 
            );

&gen_purpose(
             gtk_adjustment_new (0.0, 0.0, 10.0, 1.0, 5.0, 10.0) 
             gtk_adjustment_new (10.0, 1.0, 100.0, 5.0, 20.0, 50.0) 
            );


#********* gtk_viewport_set_hadjustment  *********

<FUNCTION>
Name:  gtk_viewport_set_hadjustment
Params: GtkViewport *viewport, GtkAdjustment *adjustment
Returns: void
Assertion: Sets the horizontal adjustment of the viewport.
Params Values: GtkViewport *viewport, GtkAdjustment *adjustment
Expected: void
</FUNCTION>


<DEFINE>
#define    ADJ <%0%>

GtkWidget* widget;
GtkAdjustment* ad;
GtkAdjustment* adj;

</DEFINE>


<CODE>

adj = GTK_ADJUSTMENT (ADJ);
widget = gtk_viewport_new (GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)),
                           GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)));

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
} 

if (!GTK_IS_VIEWPORT (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_viewport_set_hadjustment (GTK_VIEWPORT (widget), adj);
gtk_object_get(GTK_OBJECT(widget), "hadjustment", &ad, NULL);

if (adj != ad)
{
    TEST_FAIL("set hadjustment fails", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef  ADJ
</CODE>


&gen_purpose(
             gtk_adjustment_new (10.0, 1.0, 100.0, 5.0, 20.0, 50.0) 
            );

&gen_purpose(
             gtk_adjustment_new (0.0, 0.0, 10.0, 1.0, 5.0, 10.0) 
            );


#********* gtk_viewport_set_vadjustment  *********

<FUNCTION>
Name:  gtk_viewport_set_vadjustment
Params: GtkViewport *viewport, GtkAdjustment *adjustment
Returns: void
Assertion: Sets the vertical adjustment of the viewport.
Params Values: GtkViewport *viewport, GtkAdjustment *adjustment
Expected: void
</FUNCTION>


<DEFINE>
#define    ADJ <%0%>

GtkWidget* widget;
GtkAdjustment* ad;
GtkAdjustment* adj;
</DEFINE>


<CODE>
adj = GTK_ADJUSTMENT (ADJ);
widget = gtk_viewport_new (GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)),
                           GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)));

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
} 

if (!GTK_IS_VIEWPORT (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_viewport_set_vadjustment (GTK_VIEWPORT (widget), adj);
gtk_object_get(GTK_OBJECT(widget), "vadjustment", &ad, NULL);
if (adj != ad)
{
    TEST_FAIL("set vadjustment fails", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef  ADJ
</CODE>


&gen_purpose(
             gtk_adjustment_new (10.0, 1.0, 100.0, 5.0, 20.0, 50.0) 
            );

&gen_purpose(
             gtk_adjustment_new (0.0, 0.0, 10.0, 1.0, 5.0, 10.0) 
            );


#********* gtk_viewport_get_hadjustment  *********

<FUNCTION>
Name:  gtk_viewport_get_hadjustment
Params: GtkViewport *viewport
Returns: GtkAdjustment*
Assertion: Returns the horizontal adjustment of the viewport.
Params Values: GtkViewport *viewport
Expected: GtkAdjustment*
</FUNCTION>


<DEFINE>
#define    ADJ <%0%>

GtkWidget* widget;
GtkAdjustment* adj;
</DEFINE>


<CODE>
adj = GTK_ADJUSTMENT (ADJ);
widget = gtk_viewport_new (GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)),
                           GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)));

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
} 

if (!GTK_IS_VIEWPORT (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_object_set(GTK_OBJECT(widget), "hadjustment", adj, NULL);
if (adj != gtk_viewport_get_hadjustment (GTK_VIEWPORT (widget)))
{
    TEST_FAIL("get hadjustment fails", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef  ADJ
</CODE>


&gen_purpose(
             gtk_adjustment_new (10.0, 1.0, 100.0, 5.0, 20.0, 50.0) 
            );

&gen_purpose(
             gtk_adjustment_new (0.0, 0.0, 10.0, 1.0, 5.0, 10.0) 
            );


#********* gtk_viewport_get_vadjustment  *********

<FUNCTION>
Name:  gtk_viewport_get_vadjustment
Params: GtkViewport *viewport
Returns: GtkAdjustment*
Assertion: Returns the vertical adjustment of the viewport.
Params Values: GtkViewport *viewport
Expected: GtkAdjustment*
</FUNCTION>


<DEFINE>
#define    ADJ <%0%>

GtkWidget* widget;
GtkAdjustment* adj;
</DEFINE>


<CODE>
adj = GTK_ADJUSTMENT (ADJ);
widget = gtk_viewport_new (GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)),
                           GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)));

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
} 

if (!GTK_IS_VIEWPORT (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_object_set(GTK_OBJECT(widget), "vadjustment", adj, NULL);
if (adj != gtk_viewport_get_vadjustment (GTK_VIEWPORT (widget)))
{
    TEST_FAIL("get vadjustment fails", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef  ADJ
</CODE>


&gen_purpose(
             gtk_adjustment_new (10.0, 1.0, 100.0, 5.0, 20.0, 50.0) 
            );

&gen_purpose(
             gtk_adjustment_new (0.0, 0.0, 10.0, 1.0, 5.0, 10.0) 
            );


#********* gtk_viewport_set_shadow_type  *********

<FUNCTION>
Name:  gtk_viewport_set_shadow_type
Params: GtkViewport *viewport, GtkShadowType type
Returns: void
Assertion: Sets the shadow type of the viewport.
Params Values: GtkViewport *viewport, <%0%>
Expected: void
</FUNCTION>


<DEFINE>
#define    SHADOW <%0%>

GtkWidget* widget;
GtkShadowType shadow_type;
</DEFINE>


<CODE>
widget = gtk_viewport_new (GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)),
                           GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)));

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
} 

if (!GTK_IS_VIEWPORT (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_viewport_set_shadow_type (GTK_VIEWPORT (widget), SHADOW);
gtk_object_get(GTK_OBJECT(widget), "shadow-type", &shadow_type, NULL);
if (shadow_type != SHADOW)
{
    TEST_FAIL("shadow type fails", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef  SHADOW
</CODE>


&gen_purpose(
            GTK_SHADOW_NONE
            );

&gen_purpose(
            GTK_SHADOW_IN
            );

&gen_purpose(
            GTK_SHADOW_OUT
            );

&gen_purpose(
            GTK_SHADOW_ETCHED_IN
            );

&gen_purpose(
            GTK_SHADOW_ETCHED_OUT
            );



#********* gtk_viewport_get_shadow_type  *********
<FUNCTION>
Name:  gtk_viewport_get_shadow_type
Params: GtkViewport *viewport
Returns: GtkShadowType type
Assertion: Gets the shadow type of the GtkViewport.
Params Values: GtkViewport *viewport
Expected: <%0%>
</FUNCTION>


<DEFINE>
#define    SHADOW <%0%>

GtkWidget* widget;
GtkShadowType shadow_type;
</DEFINE>


<CODE>
widget = gtk_viewport_new (GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)),
                           GTK_ADJUSTMENT (gtk_adjustment_new (1.0, 2.0, 5.0, 5.0, 20.0, 50.0)));

if (!widget)
{
    TEST_FAIL (get_not_created_msg(), NULL); 
    return;
} 

if (!GTK_IS_VIEWPORT (widget))
{
    TEST_FAIL (get_wrong_type_msg(), NULL); 
    return;
}

gtk_object_set(GTK_OBJECT(widget), "shadow-type", SHADOW, NULL);

shadow_type = gtk_viewport_get_shadow_type (GTK_VIEWPORT (widget));
if (shadow_type != SHADOW)
{
    TEST_FAIL("shadow type fails", NULL);
}

/******* VISUALISATION *******/
TEST_VISUALIZE (widget);
/******* -------------  *******/  

gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef  SHADOW
</CODE>


&gen_purpose(
            GTK_SHADOW_NONE
            );

&gen_purpose(
            GTK_SHADOW_IN
            );

&gen_purpose(
            GTK_SHADOW_OUT
            );

&gen_purpose(
            GTK_SHADOW_ETCHED_IN
            );

&gen_purpose(
            GTK_SHADOW_ETCHED_OUT
            );

