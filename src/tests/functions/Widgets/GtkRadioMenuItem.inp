# TODO:
# add test purpose which checks if its possible to create a group with >=2 
# active radio buttons

#**********  gtk_radio_menu_item_new () **********

<FUNCTION>
Name: gtk_radio_menu_item_new
Params: GSList *group
Returns: GtkWidget*
Assertion: Creates a new GtkRadioMenuItem.
Params Values: CSList*
Expected: GtkRadioMenuItem*
</FUNCTION>
 
<DEFINE>
#define    GROUP <%0%>

GSList *group;
GtkWidget *widget;
</DEFINE>       

<CODE>
group = GROUP;
widget = gtk_radio_menu_item_new (group);

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
} 

if (!GTK_IS_RADIO_MENU_ITEM (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

/******* VISUALISATION *******/
{
    GtkWidget* menu_shell = gtk_menu_bar_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu_shell), widget);
    TEST_VISUALIZE (menu_shell);
}
/******* -------------  *******/  
gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef GROUP
</CODE>


&gen_purpose (
             NULL
             );

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             );



#**********  gtk_radio_menu_item_new_with_label () **********
<FUNCTION>
Name: gtk_radio_menu_item_new_with_label
Params: GSList *group, const gchar *label
Returns: GtkRadioMenuItem*
Assertion: Creates a new GtkRadioMenuItem whose child is a simple GtkLabel. 
Params Values: <%0%>, <%1%>
Expected: GtkRadioMenuItem*
</FUNCTION>

<DEFINE>
#define GROUP <%0%>
#define LABEL <%1%>

GSList *group;
GtkWidget *widget;
GtkWidget* child;
G_CONST_RETURN gchar* real_label = NULL;
</DEFINE>       

<CODE>
group = GROUP;
widget = gtk_radio_menu_item_new_with_label (group, LABEL);

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
} 

if (!GTK_IS_RADIO_MENU_ITEM (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

child = gtk_bin_get_child (GTK_BIN (widget));
if (LABEL)
{
    if (!GTK_IS_LABEL (child))
    {
        TEST_FAIL ("radio menu item child should be of type GtkLabel", NULL);
        return;
    }
} else {
    if (child)
    {
        TEST_FAIL ("radio menu item child should be NULL", NULL);
    }
}

if (LABEL)
{
    real_label = gtk_label_get_text (GTK_LABEL (child));
    if (strcmp (real_label, LABEL)) {
        TEST_FAIL ("real_label is '%s', should be '%s'", real_label, LABEL);
    }
}

/******* VISUALISATION *******/
{
    GtkWidget* menu_shell = gtk_menu_bar_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu_shell), widget);
    TEST_VISUALIZE (menu_shell);
}
/******* -------------  *******/  
gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef LABEL
#undef GROUP
</CODE>

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             "Text"
             );

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
             );

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             "!@#$%^&*()_+|~=\";:'?<>`/"
             );

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             "\b\n\t"
             );

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             " "
             );

&gen_purpose (
             NULL
             "Text"
             );

&gen_purpose (
             NULL
             "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
             );

&gen_purpose (
             NULL
             "!@#$%^&*()_+|~=\";:'?<>`/"
             );

&gen_purpose (
             NULL
             "\b\n\t"
             );


#**********  gtk_radio_menu_item_new_with_mnemonic () **********
<FUNCTION>
Name: gtk_radio_menu_item_new_with_mnemonic
Params: GSList *group, const gchar *label
Returns: GtkRadioMenuItem*
Assertion: Creates a new GtkRadioMenuItem containing a label.
Params Values: <%0%>, <%1%>
Expected: GtkRadioMenuItem*
</FUNCTION>

<DEFINE>
#define GROUP <%0%>
#define LABEL <%1%>
#define CORRECT_LABEL <%2%>

GSList *group;
GtkWidget *widget;
GtkWidget* child;
G_CONST_RETURN gchar* real_label = NULL;
</DEFINE>       

<CODE>
group = GROUP;
widget = gtk_radio_menu_item_new_with_mnemonic (group, LABEL);

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
} 

if (!GTK_IS_RADIO_MENU_ITEM (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

child = gtk_bin_get_child (GTK_BIN (widget));
if (LABEL)
{
    if (!GTK_IS_LABEL (child))
    {
        TEST_FAIL ("radio menu item child should be of type GtkLabel", NULL);
        return;
    }
} else {
    if (child)
    {
        TEST_FAIL ("radio menu item child should be NULL", NULL);
    }
}

if (LABEL)
{
    real_label = gtk_label_get_text (GTK_LABEL (child));
    if (strcmp (real_label, CORRECT_LABEL)) {
        TEST_FAIL ("real_label is '%s', should be '%s'", 
                   real_label, CORRECT_LABEL);
    }
}

/******* VISUALISATION *******/
{
    GtkWidget* menu_shell = gtk_menu_bar_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu_shell), widget);
    TEST_VISUALIZE (menu_shell);
}
/******* -------------  *******/  
gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef LABEL
#undef CORRECT_LABEL
#undef GROUP
</CODE>

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
       "Text"
       "Text"
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
       "Tex_t"
       "Text"
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
      "ab_cdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
       "!@#$%^&*()__+|~=\";:'?<>`/"
       "!@#$%^&*()_+|~=\";:'?<>`/"
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
       "\b\n\t"
       "\b\n\t"
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
       "\b_\n\t"
       "\b\n\t"
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
       " "
       " "
              );

&gen_purpose (
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
       "_ "
       " "
              );



#**********  gtk_radio_menu_item_new_with_mnemonic () **********
<FUNCTION>
Name: gtk_radio_menu_item_new_with_mnemonic
Params: GSList *group, const gchar *label
Returns: GtkRadioMenuItem*
Assertion: The label will be created using gtk_label_new_with_mnemonic(), so underscores in label indicate the mnemonic for the menu item.
Params Values: <%0%>, <%1%>
Expected: GtkRadioMenuItem*
</FUNCTION>

<DEFINE>
#define GROUP <%0%>
#define LABEL <%1%>
#define KEY <%2%>

GSList *group;
GtkWidget *widget;
GtkWidget* child;
guint keyval;
</DEFINE>       

<CODE>
group = GROUP;
widget = gtk_radio_menu_item_new_with_mnemonic (group, LABEL);

if (!widget)
{
    TEST_FAIL (get_not_created_msg (), NULL); 
    return;
} 

if (!GTK_IS_RADIO_MENU_ITEM (widget))
{
    TEST_FAIL (get_wrong_type_msg (), NULL); 
    return;
}

child = gtk_bin_get_child (GTK_BIN (widget));
keyval = gtk_label_get_mnemonic_keyval (GTK_LABEL (child));
if (keyval != KEY)
{
    TEST_FAIL ("keyval = %i, should be %i", keyval, KEY);
}

/******* VISUALISATION *******/
{
    GtkWidget* menu_shell = gtk_menu_bar_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu_shell), widget);
    TEST_VISUALIZE (menu_shell);
}
/******* -------------  *******/  
gtk_widget_destroy (gtk_widget_get_toplevel (widget));

#undef LABEL
#undef CORRECT_LABEL
#undef KEY
</CODE>

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             "Te_xt"
             GDK_x
             );

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             "_Text"
             GDK_t
             );

&gen_purpose (
             gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             "T_ext"
             GDK_e
             );






#*********  gtk_radio_menu_item_group *************
#*********  gtk_radio_menu_item_set_group *************
<FUNCTION>
Name:   gtk_radio_menu_item_get_group
Params: GtkRadioMenuItem *radio_menu_item
Returns: GSList*
Assertion: Returns the group to which the radio menu item belongs, as a GList of GtkRadioMenuItem.
Params Values: a newly created GtkRadioMenuItem
Expected: GSList*

Name:   gtk_radio_menu_item_set_group
Params: GtkRadioMenuItem *radio_menu_item, GSList *group
Returns: void
Assertion: Sets the group of a radio menu item, or changes it.
Params Values: a newly created GtkRadioMenuItem, GSList*
Expected: void

</FUNCTION>

<DEFINE>
#define GROUP <%0%>
#define ELEM_NUM <%1%>
#define NEW_GRP_ELEM_NUM <%2%>

GtkWidget* widget[ELEM_NUM];
GtkWidget* wdg1;
GtkWidget* wdg2;
GSList* grp1;
GSList* grp2;
GSList* group_value;
gint i;
gint group_length;
</DEFINE>	

<CODE>
wdg1 = gtk_radio_menu_item_new (NULL);
wdg2 = gtk_radio_menu_item_new (NULL);

grp1 = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (wdg1));
grp2 = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (wdg2));

for (i = 0; i < ELEM_NUM; i++)
{
    widget[i] = gtk_radio_menu_item_new (grp1);

    if (!widget[i])
    {
        TEST_FAIL (get_not_created_msg (), NULL); 
        return;
    } 

    if (!GTK_IS_RADIO_MENU_ITEM (widget[i]))
    {
        TEST_FAIL (get_wrong_type_msg (), NULL); 
        return;
    }

    grp1 = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (widget[i]));
}

/* change group for a few elements */
for (i = 0; i < NEW_GRP_ELEM_NUM; i++)
{
    gtk_radio_menu_item_set_group (GTK_RADIO_MENU_ITEM (widget[i]), grp2);
    grp2 = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (widget[i]));
}

for (i = 1; i < NEW_GRP_ELEM_NUM; i++)
{
    group_value = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (widget[i]));
    if (group_value != grp2)
    {
        TEST_FAIL ("changed group: group of widget[%i] is wrong", i);
    }
}

group_length = g_slist_length (gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (wdg1)));
if (group_length != ELEM_NUM - NEW_GRP_ELEM_NUM + 1)
{
    TEST_FAIL ("changed group: old group_length = %d, should be %d", group_length, ELEM_NUM - NEW_GRP_ELEM_NUM + 1);
} 

/******* VISUALISATION *******/
{
    gint i;
    GtkWidget* menu_shell = gtk_menu_bar_new ();
    for (i = 0; i < ELEM_NUM; i++)
    {
        gtk_menu_shell_append (GTK_MENU_SHELL (menu_shell), widget[i]);
    }
    TEST_VISUALIZE (menu_shell);
}
/******* -------------  *******/  
gtk_widget_destroy (gtk_widget_get_toplevel (widget[0]));

#undef ELEM_NUM
#undef NEW_GRP_ELEM_NUM
</CODE>

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             1
             1
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             10
             1
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             10
             2
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             10
             10
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             100
             1
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             100
             26
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             100
             49
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             100
             99
             );

&gen_purpose(
       gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (gtk_radio_menu_item_new_with_label (NULL, "Item")))
             100
             100
             );
