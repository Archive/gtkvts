#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <common.h>

#include "libfile.h"
#include "libtestcov.h"
#include "libmem.h"
#include "libstr.h"

#define NAME " * Name:"
#define PARAMS " * Params:"
#define RETURNS " * Returns:"
#define ASSER " * Assertion:"

static gint cl[5];
static gint fu[5];
static gint as[5];

static gint list_type = -1;
static gint list_state = -1;

const char *list_class_mark = "@";
const char *list_func_mark = "&";
const char *list_asr_mark = "$";

const char *ignore_list_mark = "{Ignore_List:";

static TestListItem* real_test_list_find_node_by_name_and_type (TestListItem* tree,  gchar* name,  TestListType type, gint flag);


char*
asr_get_ignore_list_record (char* asr)
{
    char* rbr_pos = NULL;

    if (strncmp ((const char*) asr + 1, ignore_list_mark, 
                  strlen (ignore_list_mark)))
    {
        return NULL;
    } else {
        rbr_pos = strchr (asr + 1 + strlen (ignore_list_mark) + 1, '}');
        if (!rbr_pos)
        {
            printf ("Libtestcov warning:\nassertion\n'%s'\n has unclosed {Ignore_List:} record.\nSee coverage/asr_exclude_list.txt.\n\n", asr);
            return NULL;
        } else {
            char* res = NULL;
            res = alloc_mem_for_string (res, rbr_pos + 1 - (asr + 1));
            res = strncpy (res, asr + 1, rbr_pos + 1 - (asr + 1));
            return res;
        }
    }
}
    
gboolean
asr_has_ignore_list_record (char* asr)
{
    if (strncmp ((const char*) asr + 1, ignore_list_mark, 
                 strlen (ignore_list_mark)))
    {
        return FALSE;
    } else {
        if (!strchr (asr + 1 + strlen (ignore_list_mark) + 1, '}'))
        {
            printf ("Libtestcov warning:\nassertion\n'%s'\n has unclosed {Ignore_List:} record.\nSee coverage/asr_exclude_list.txt.\n\n",
                    asr);
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
            
gboolean
ign_list_rec_fits_ign_list (char* asr_ignl_rec, char* ignore_list)
{
    char* pch;
    gboolean fits = FALSE;
    char* asr_ignl_rec_cpy = strdup (asr_ignl_rec);
    
    
    pch = strtok (asr_ignl_rec_cpy 
                  + strlen (ignore_list_mark), 
                  "#}");
    while (pch)
    {
        gchar* found = strstr (ignore_list, pch);
        if (found)
        {
            if ((found == ignore_list)
                || (*(found - 1) == '#'))
            {
                if (strlen (pch) 
                    == strlen (found))
                {
                    fits = TRUE;
                } else if (strlen (pch) 
                           < strlen (ignore_list)) {
                    if (*(found + strlen (pch)) == '#')
                    {
                        fits = TRUE;
                    }
                }
            } 
        }
        pch = strtok (NULL, "#");
        
    }
    free (asr_ignl_rec_cpy);

    return fits;
}


char* 
cut_tags (char *str)
{
    int i;
    char *pchar;
    int j = 0;
    char* ret_val = NULL;
    int may_add = 1;

    if (!str)
    {
        return NULL;
    }

    ret_val = alloc_mem_for_string (ret_val, strlen (str));

    pchar = strchr (str, '\n');
    while (pchar)
    {
        *(pchar) = ' ';
        pchar = strchr (pchar + 1, '\n');
    }

    for (i = 0; i < strlen (str); i++)
    {
        if (str [i] == '<')
        {
            while (str[i] != '>' && i < strlen (str))
            {
                i++;
            }
        } else if (may_add == 1) {
            ret_val[j++] = str[i];
        }
    }
    
    return ret_val;
}


void 
gen_asr_list_from_tree (TestListItem *tree, char* asr_list_file_name)
{

    GSList* treelist = tree->list;
    FILE* p_output_file = open_file (asr_list_file_name, 
                                     "at", NULL);
    static gboolean class_excluded = FALSE;
    static gboolean func_excluded = FALSE;

    if (tree->type != ROOT)
    {
        if (tree->type == CLASS)
        {
            if (tree->state == EXCLUDED || tree->is_deprecated == TRUE)
            {
                class_excluded = TRUE;
            } else {
                class_excluded = FALSE;
            }

            if (!class_excluded)
            {
                if (treelist)
                {

                    fprintf (p_output_file, "\n\n%s%s", list_class_mark, tree->name);
                } else {
                    fclose (p_output_file);
                    return;
                }
            }
        } else if (tree->type == FUNCTION) {
            if (tree->state == EXCLUDED || tree->is_deprecated == TRUE)
            {
                func_excluded = TRUE;
            } else {
                func_excluded = FALSE;
            }
            
            if (!class_excluded && !func_excluded)
            {
                fprintf (p_output_file, "\n\n%s%s", list_func_mark, tree->name);
            }
        } else if (tree->type == ASSERTION) {
            if (!class_excluded && !func_excluded 
                && tree->state != EXCLUDED
                && tree->is_deprecated != TRUE)
            {
                fprintf (p_output_file, "\n%s%s", list_asr_mark, tree->name);
            }
        } 
    }

    fclose (p_output_file);

    while (treelist)
    {
        gen_asr_list_from_tree ((TestListItem*)treelist->data, 
                                asr_list_file_name);
        treelist = treelist->next;

    }
}


void 
gen_hdr_list_from_tree (TestListItem *tree, char* hdr_list_file_name)
{
    GSList* treelist = tree->list;
    FILE* p_output_file = open_file (hdr_list_file_name, 
                                     "at", NULL);

    if (tree->type != ROOT)
    {
        if (tree->type == CLASS)
        {
            if (treelist)
            {
                fprintf (p_output_file, "\n%s%s\n", list_class_mark, tree->name);
            } else {
                fclose (p_output_file);
                return;
            }
        } else if (tree->type == FUNCTION) {
            fprintf (p_output_file, "%s%s\n", list_func_mark, tree->name);
        } 
    }

    fclose (p_output_file);

    while (treelist)
    {
        gen_hdr_list_from_tree ((TestListItem*)treelist->data, 
                                hdr_list_file_name);
        treelist = treelist->next;

    }
}



char*
get_tag_contents (char* source, const char* tag, const char* must_be_tag, const char* start, const char *end)
{
    char *tag_contents = NULL;
    char *tag_open = NULL;
    char *tag_close = NULL;
    char *tag_open_pos = NULL;
    char *tag_close_pos = NULL;
    char *tag_contents_start_pos = NULL;
    int brackets_number = 0;
    int tag_open_len = 0;
    int tag_close_len = 0;
    char* p;

    tag_open = alloc_mem_for_string (tag_open, strlen (tag) + 1);
    tag_close = alloc_mem_for_string (tag_close, strlen (tag) + 2);

    strcat (tag_open, "<");
    strcat (tag_open, tag);
    strcat (tag_close, "</");
    strcat (tag_close, tag);

    tag_open_pos = strstr (source, tag_open);
    tag_close_pos = strstr (source, tag_close);
    tag_open_len = strlen (tag_open);
    tag_close_len = strlen (tag_close);

    if (!tag_open_pos || !tag_close_pos)
    {
        free (tag_open);
        free (tag_close);
        return NULL;
    }
    
    if (must_be_tag != NULL)
    {
        char* must_be_tag_open_pos = tag_open_pos + strlen (tag_open);
        while (must_be_tag_open_pos)
        {
            while (*must_be_tag_open_pos == ' ')
            {
                must_be_tag_open_pos = must_be_tag_open_pos + 1;
            }
            
            if (strncmp (must_be_tag_open_pos, must_be_tag, strlen (must_be_tag)))
            {
                must_be_tag_open_pos = strstr (must_be_tag_open_pos + 1, tag_open);
            } else {
                tag_open_pos = must_be_tag_open_pos;
                break;
            }
        }
        if (!must_be_tag_open_pos)
        {
            return NULL;
        }
    }

    p = tag_open_pos + tag_open_len;
    brackets_number = 1;
    while (p)
    {
        if (!(strncmp (p, tag_open, tag_open_len)))
        {
            brackets_number ++;
        }
            
        if (!(strncmp (p, tag_close, tag_close_len)))
        {
            brackets_number --;
        }

        if (brackets_number > 0)
        {
            p = p + 1;
        } else {
            break;
        }
    }

    if (!p && brackets_number > 0)
    {
        return NULL;
    }

    if (brackets_number == 0)
    {
        tag_close_pos = p;
    }

    if (end)
    {
        char* end_pos = strstr (source, end);

        if (end_pos && end_pos < tag_close_pos)
        {
            free (tag_open);
            free (tag_close);
            return NULL;
        }
    }
            
    tag_contents_start_pos = strchr (tag_open_pos, '>') + 1;
    if (!tag_contents_start_pos)
    {
        printf ("\nlibtestcov warning: unclosed tag '%s'\n\n");
        free (tag_open);
        free (tag_close);
        return NULL;
    }

    tag_contents = alloc_mem_for_string (tag_contents, 
                                         tag_close_pos - tag_contents_start_pos);
    strncpy (tag_contents, tag_contents_start_pos, 
             tag_close_pos - tag_contents_start_pos);

/*     tag_contents = replace_substr_in_string (tag_close_pos, tag_close, ""); */

    free (tag_open);
    free (tag_close);

    return tag_contents;
}

/* char* */
/* get_tag_contents (char* source, const char* tag, const char* must_be_tag, const char* start, const char *end) */
/* { */
/*     char *tag_contents = NULL; */
/*     char *tag_open = NULL; */
/*     char *tag_close = NULL; */
/*     char *tag_open_pos = NULL; */
/*     char *tag_close_pos = NULL; */
/*     char *tag_contents_start_pos = NULL; */

/*     tag_open = alloc_mem_for_string (tag_open, strlen (tag) + 1); */
/*     tag_close = alloc_mem_for_string (tag_close, strlen (tag) + 2); */

/*     strcat (tag_open, "<"); */
/*     strcat (tag_open, tag); */
/*     strcat (tag_close, "</"); */
/*     strcat (tag_close, tag); */

/*     tag_open_pos = strstr (source, tag_open); */
/*     tag_close_pos = strstr (source, tag_close); */

/*     if (!tag_open_pos || !tag_close_pos) */
/*     { */
/*         free (tag_open); */
/*         free (tag_close); */
/*         return NULL; */
/*     } */
    
/*     if (end) */
/*     { */
/*         char* end_pos = strstr (source, end); */

/*         if (end_pos && end_pos < tag_close_pos) */
/*         { */
/*             free (tag_open); */
/*             free (tag_close); */
/*             return NULL; */
/*         } */
/*     } */
            
/*     if (must_be_tag != NULL) */
/*     { */
/*         char* p = tag_open_pos + strlen (tag_open); */

/*         while (*p == ' ') */
/*         { */
/*             p = p + 1; */
/*         } */

/*         if (strncmp (p, must_be_tag, strlen (must_be_tag))) */
/*         { */
/*             free (tag_open); */
/*             free (tag_close); */
/*             return NULL; */
/*         } */
/*     } */
    
/*     tag_contents_start_pos = strchr (tag_open_pos, '>') + 1; */
/*     if (!tag_contents_start_pos) */
/*     { */
/*         printf ("\nlibtestcov warning: unclosed tag '%s'\n\n"); */
/*         free (tag_open); */
/*         free (tag_close); */
/*         return NULL; */
/*     } */

/*     tag_contents = alloc_mem_for_string (tag_contents,  */
/*                                          tag_close_pos - tag_contents_start_pos); */
/*     strncpy (tag_contents, tag_contents_start_pos,  */
/*              tag_close_pos - tag_contents_start_pos); */

/*     tag_contents = replace_all_substr_in_string (tag_contents, tag_close, ""); */

/*     free (tag_open); */
/*     free (tag_close); */
/*     return tag_contents; */
/* } */


char*
split_asr_set (char* asr_set)
{
    int asr_set_pos;
    int ret_val_pos;
    int ret_val_len;
    int i;
    const char* asr_start = "$";
    char* ret_val = NULL;


    if (!asr_set)
    {
        ret_val = alloc_mem_for_string (ret_val, 1);
        *ret_val = '\0';

        return ret_val;
    }

    asr_set = trim (asr_set);
    if (!strlen (asr_set))
    {
        ret_val = alloc_mem_for_string (ret_val, 1);
        *ret_val = '\0';

        return ret_val;
    }

    ret_val_len = strlen (asr_start) + strlen (asr_set) + 1;
    ret_val = alloc_mem_for_string (ret_val, ret_val_len);
    strcpy (ret_val, asr_start);

    for (asr_set_pos = 0, ret_val_pos = strlen (asr_start); 
         asr_set_pos < strlen (asr_set);
         ret_val_pos++, asr_set_pos++)
    {
        ret_val[ret_val_pos] = asr_set[asr_set_pos];
        if (asr_set[asr_set_pos] == '(' 
            || asr_set[asr_set_pos] == '[' )
        {
            gchar open_bracket = asr_set[asr_set_pos];
            int counter = 0;
            gchar close_bracket[] = {')', ']'};

            if (open_bracket == '[')
            {
                close_bracket [1] = '\0';
            }

            do {
                if (asr_set[asr_set_pos] == open_bracket)
                {
                    counter++;
                } else if (asr_set[asr_set_pos] == close_bracket [0]
                           || asr_set[asr_set_pos] == close_bracket [1])
                {
                    counter--;
                }
                
                ret_val [ret_val_pos++] = asr_set[asr_set_pos++];
            } while (counter != 0 && asr_set[asr_set_pos]);

            ret_val [ret_val_pos] = asr_set[asr_set_pos];

            if (asr_set[asr_set_pos - 2] == '.')
            {
                i = 1;
                while (asr_set[asr_set_pos + i] != '\0'
                       && asr_set[asr_set_pos + i] == ' ')
                {
                      /* do nothing; just skip spaces */ 
                    i++;
                }
                if (asr_set[asr_set_pos + i] >= 'A'
                    && asr_set[asr_set_pos + i] <= 'Z')

                {
                    ret_val = realloc (ret_val, 
                                       ret_val_len + strlen (asr_start));
                    memset (ret_val + ret_val_len, '\0', strlen (asr_start));
                    ret_val_len += strlen (asr_start);
                    strcat (ret_val + ret_val_pos + 1, asr_start);
                    ret_val_pos += strlen (asr_start);
                }
            }

            if (asr_set[asr_set_pos] == '.')
            {
                asr_set_pos--;
                ret_val_pos--;
            }
        } else if (asr_set[asr_set_pos] == '.') {
            if (asr_set[asr_set_pos + 1] 
                && asr_set[asr_set_pos + 1] >= '0'
                && asr_set[asr_set_pos + 1] <= '9')
            {
                continue;
            } else if (asr_set[asr_set_pos + 1] 
                && asr_set[asr_set_pos + 1] >= 'a'
                && asr_set[asr_set_pos + 1] <= 'z')
            {
                continue;
            } else if ((strstr (asr_set + asr_set_pos - 1, "i.e.") 
                        == asr_set + asr_set_pos - 1)
                       || (strstr (asr_set + asr_set_pos - 1, "e.g.") 
                           == asr_set + asr_set_pos - 1)
                       || (strstr (asr_set + asr_set_pos - 3, "i.e.") 
                           == asr_set + asr_set_pos - 3)
                       || (strstr (asr_set + asr_set_pos - 3, "e.g.") 
                           == asr_set + asr_set_pos - 3)) {
                continue;
            } else if (asr_set[asr_set_pos + 1] == '.') {
                continue;
            } else if (asr_set[asr_set_pos + 1] == '_') {
                continue;
            } else if (strstr (asr_set + asr_set_pos - 2, "Ie.") 
                       == asr_set + asr_set_pos - 2) {
                continue;
            } else if (strstr (asr_set + asr_set_pos - 2, "ie.") 
                       == asr_set + asr_set_pos - 2) {
                continue;
            } else if (strstr (asr_set + asr_set_pos - 2, "vs.") 
                       == asr_set + asr_set_pos - 2) {
                continue;
            /* this is a hack, but how can I handle this other way? :) */
            } else if (strstr (asr_set + asr_set_pos, ". 65535") 
                       == asr_set + asr_set_pos) {
                continue;
            /* this is a hack too */
            } else if (asr_set[asr_set_pos + 1] == '/') {
                continue;
            } else if (asr_set[asr_set_pos + 1] == '\"') {
                continue;
            } else {
                if (asr_set [asr_set_pos + 1]) {
                    if (strstr (asr_set + asr_set_pos - 3, "etc.") 
                        == asr_set + asr_set_pos - 3) {
                        if (asr_set[asr_set_pos + 1] == ' ')
                        {
                            int j = 2;
                            while (asr_set[asr_set_pos + j] 
                                   && asr_set[asr_set_pos + j] == ' ')
                            {
                                j++;
                            }
                            
                            if (!(asr_set[asr_set_pos + j] > 'A' 
                                  && asr_set[asr_set_pos + j] < 'Z'))
                            {
                                continue;
                            } else {
                            }
                        } else {
                            continue;
                        }
                    }
                    ret_val = realloc (ret_val, 
                                       ret_val_len + strlen (asr_start));
                    memset (ret_val + ret_val_len, '\0', strlen (asr_start));
                    ret_val_len += strlen (asr_start);
                    strcat (ret_val + ret_val_pos + 1, asr_start);
                    ret_val_pos += strlen (asr_start);
                    while (asr_set[asr_set_pos + 1] == ' ')
                    {
                        asr_set_pos++;
                    }
                } else {
                    break;
                }
            }
        }
    }

    return ret_val;
}



TestListItem* test_list_new ()
{
    TestListItem* tree = (TestListItem*) calloc (1, sizeof (TestListItem));
    if (tree)
    {
        tree->name = malloc (sizeof (char) * (strlen ("root") + 1));
        tree->name = strcpy (tree->name, "root");
        tree->list = NULL;
        tree->type = ROOT;
        tree->state = NOT_TESTED_DOCUMENTED;
        tree->is_deprecated = 0;
    }
    return tree;

}


gint  test_list_compare (TestListItem* item1, TestListItem* item2)
{
    if (item1->type == FUNCTION)
    {
        gchar* str1 = (gchar*)strstr (item1->name, " ");
        gchar* str2 = (gchar*)strstr (item2->name, " ");
        if (!str1) str1 = item1->name;
        if (!str2) str2 = item2->name;
        return  strcmp (str1, str2);
    } else {
        return  strcmp (item1->name, item2->name);
    }
}

gchar*  test_list_parse_string (gchar* str)
{
    gchar prev;
    gchar next;
    gint i = 0;
    
    while (str[0] == ' ')
    {
        strcpy (&str[0], &str[1]);
    }

    prev = str[i++];
    while (prev != '\0')
    {
        next = str[i];

        if (prev == '`')
        {
            str[i-1] = '\'';
            next = str[--i];
        }

        if ((prev == ' ' && next == ' ') || (prev == ' ' && next == ')') ||
            (prev == '(' && next == ' ') || (prev == '\n'))
        {
            strcpy (&str[i-1], &str[i]);
            next = str[--i];
        }

        if (prev == ' ' && next == '*')
        {
            str[i-1] = '*';
            str[i]   = ' ';
            next = str[--i];
        }
        if (prev == ' ' && next == ',')
        {
            str[i-1] = ',';
            str[i]   = ' ';
            next = str[--i];
        }
 
        prev = next;
        i++;
    }

    while ((str [strlen (str)-1] == ';') || (str [strlen (str)-1] == '\'') ||
           (str [strlen (str)-1] == ' ') || (str [strlen (str)-1] == '.'))
        str [strlen (str)-1] = '\0';

    return str;
}


TestListItem* test_list_add (TestListItem* node, gchar* nme, TestListType tpe, TestListState stat, gboolean is_depr)
{
    TestListItem* tree = (TestListItem*) calloc (1, sizeof (TestListItem));
    if (tree)
    {
        tree->name = calloc (strlen (nme)+1, sizeof (gchar));
        strcpy (tree->name, nme);
        tree->list = NULL;
        tree->type = tpe;
        tree->state = stat;
        tree->is_deprecated = is_depr;
        node->list = g_slist_insert_sorted (node->list, tree, 
                                            (GCompareFunc)test_list_compare);
    }

    return tree;
}


void test_list_free (TestListItem* tree)
{
    GSList* list = tree->list;

    while (list)
    {
        TestListItem* node = list->data;

        if (node)
        {
            test_list_free (node);
        }
        list = list->next;
    }

    free (tree->name);
    g_slist_free (tree->list);
    free (tree);
}

TestListItem*  test_list_find  (TestListItem* tree, gchar* name)
{
    GSList* list = tree->list;
    TestListItem* item = NULL;
    while (list)
    {
        TestListItem* item = list->data;
        gchar* find = (gchar*)strstr (item->name, name);
        if (find)
        {
            if ((!strcmp (item->name, name)) || (find [strlen (name)] == ' '))
            {
                if (item->state == NOT_TESTED_DOCUMENTED)
                    item->state = TESTED_DOCUMENTED;
                return item;
            }
        }
        list = list->next;
    }
    item = test_list_add (tree, name, tree->type+1, 
                          TESTED_NOT_DOCUMENTED, FALSE);
    return item;
}

void test_list_add_source_file (TestListItem* list, gchar* filename)
{
    FILE* file;
    TestListItem* class = NULL;
    TestListItem* assr = NULL;
    TestListItem* func = NULL;
    gchar* str = NULL;
    gchar* buff = NULL;
    gint size = 0;
    gint i = 0;
    
    file = fopen (filename, "r");
    if (!file)
    {
        return;
    }
    size = fsize (file);

    str = calloc (size, sizeof (gchar));
    if (!str)
    {
        fclose (file);
        return;
    }

    buff  = (gchar*)strrchr (filename, '/');
    if (buff)
        strcpy (str, &buff[1]);
    else
        strcpy (str, filename);

    str[strlen (str)-2] = '\0';

    i = 0;
    while (str[i] != '\0')
    {
        if (str[i] == '_')
            str[i] = ' ';
        i++;
    }
    
    class = test_list_find (list, str);

    while (!feof (file))
    {
        gint len = 0;

        fgets (str, size, file);
        len = strlen (str);
        
        if (len > 1) 
        {
            if (strstr (str, NAME)) 
            {
                func = test_list_find (class, test_list_parse_string (&str[strlen (NAME)]));
            }

            if (strstr (str, ASSER)) 
            {
                assr = test_list_find (func,  test_list_parse_string (&str[strlen (ASSER)]));
            }
        }
    }
    
    free (str);
    fclose (file);
}


void test_list_add_assertion_file (TestListItem* list, gchar* filename)
{
    FILE* file;
    TestListItem* class = NULL;
    TestListItem* assr = NULL;
    TestListItem* func = NULL;
    gchar* str = NULL;
    gint size = 0;
    
    file = fopen (filename, "r");
    if (!file)
    {
        return;
    }

    size = fsize (file);
    str = calloc (sizeof (gchar), size + 1);
    if (!str)
    {
        fclose (file);
        return ;
    }

    while (!feof (file))
    {
        fgets (str, size, file);
        if (strlen (str) > 1) 
        {
            if (str[0] == '@') 
            {
                class = test_list_add (list,   test_list_parse_string (&str[1]), CLASS, NOT_TESTED_NOT_DOCUMENTED, FALSE);
            } else if (str[0] == '&')  {
                func = test_list_add (class,  test_list_parse_string (&str[1]), FUNCTION, NOT_TESTED_NOT_DOCUMENTED, FALSE);
            } else  if (str[0] == '$') {
                assr = test_list_add (func,  test_list_parse_string (&str[1]), ASSERTION, NOT_TESTED_DOCUMENTED, FALSE);
                class->state = NOT_TESTED_DOCUMENTED;
                func->state = NOT_TESTED_DOCUMENTED;
            }
        }
    }

    free (str);

    fclose (file);
}


void test_list_append_exclude_list_file (TestListItem* list, gchar* filename, gchar* ignore_list)
{
    FILE* file;
    TestListItem* class = NULL;
    TestListItem* asr = NULL;
    TestListItem* func = NULL;
    gchar* str = NULL;
    gint size = 0;
    gint line_no = 0;
    TestListType type = ROOT;

    file = fopen (filename, "r");
    if (!file)
    {
        return;
    }

    size = fsize (file);
    str = calloc (sizeof (gchar), size + 1);

    if (!str)
    {
        fclose (file);
        return ;
    }

    while (!feof (file))
    {
        fgets (str, size, file);
        line_no++;

        if (strlen (str) > 1) 
        {
            if (!strncmp (str, "all", 3))
            {
                switch (type)
                {
                  case CLASS:
                      class->state = EXCLUDED;
                      break;
                  case FUNCTION:
                      func->state = EXCLUDED;
                      break;
                  default:
                      printf ("wrong 'all' at line %d\n", line_no);
                      break;
                }
            } else if (str[0] == '@') {
                class = test_list_find_node_by_name_and_type 
                    (list, test_list_parse_string (&str[1]), CLASS);
                if (class)
                {
                    type = CLASS;
                } else {
                    printf ("Libtestcov warning:\nclass\n'%s'\n doesn't exist.\n\n",
                            test_list_parse_string (&str[1]));
                    type = UNDEFINED;
                }
            } else if (str[0] == '&') {
                gboolean do_ignore = FALSE;
                char* func_ignl_rec = NULL;

                if (ignore_list && strlen (ignore_list) 
                    && strlen (str) > strlen (ignore_list_mark))
                {
                    func_ignl_rec = asr_get_ignore_list_record (str);
                    if (func_ignl_rec)
                    {
                        do_ignore = ign_list_rec_fits_ign_list 
                            (func_ignl_rec, ignore_list);
                        
                    }
                }

                if (class && do_ignore == FALSE)
                {
                    char* pre_func = NULL;
                    pre_func = test_list_parse_string (&str[1]);
                    if (!pre_func)
                    {
                        printf ("Libtestcov warning:\ntest_list_parse_string ('%s') returned NULL\n\n", &str[1]);
                        continue;
                    }

                    if (func_ignl_rec)
                    {
                        pre_func = replace_substr_in_string 
                            (pre_func, func_ignl_rec, "");
                    }

                    func = test_list_find_node_by_name_and_type 
                        (class, pre_func, FUNCTION);

                    if (func)
                    {
                        type = FUNCTION;
                    } else {
                        printf ("Libtestcov warning:\nclass\n'%s'\n doesn't have function\n'%s'.\nPlease check the name and the signature.\n\n",
                                class->name, test_list_parse_string (&str[1]));
                        type = UNDEFINED;
                    }
                }
            } else if (str[0] == '$') {
                gboolean do_ignore = FALSE;
                char* asr_ignl_rec = NULL;

                if (ignore_list && strlen (ignore_list) 
                    && strlen (str) > strlen (ignore_list_mark))
                {
                    asr_ignl_rec = asr_get_ignore_list_record (str);
                    if (asr_ignl_rec)
                    {
                        do_ignore = ign_list_rec_fits_ign_list 
                            (asr_ignl_rec, ignore_list);
                        
                    }
                }

                if (func && do_ignore == FALSE)
                {
                    char* pre_asr = NULL;
                    pre_asr = test_list_parse_string (&str[1]);
                    if (!pre_asr)
                    {
                        printf ("Libtestcov warning:\ntest_list_parse_string ('%s') returned NULL\n\n", &str[1]);
                        continue;
                    }
                        
                    if (asr_ignl_rec)
                    {
                        pre_asr = replace_substr_in_string (pre_asr, 
                                                            asr_ignl_rec, "");
                    }

                    asr = test_list_find_node_by_name_and_type (func, pre_asr, 
                                                                ASSERTION);

                    if (asr)
                    {
                        asr->state = EXCLUDED;
                        type = ASSERTION;
                    } else {
                        printf ("Libtestcov warning:\nfunction\n'%s'\n doesn't have assertion\n'%s'\n\n",
                                func->name, pre_asr);
                        type = UNDEFINED;
                    }
                }
            }
        }
    }

    free (str);

    fclose (file);
}

TestListItem* test_list_find_node_by_name_and_type 
    (TestListItem* tree,  gchar* name,  TestListType type)
{
    return real_test_list_find_node_by_name_and_type (tree, name, type, 1);
}
 

static TestListItem* real_test_list_find_node_by_name_and_type 
    (TestListItem* tree,  gchar* name,  TestListType type, gint flag)
{
    static TestListItem* item = NULL;
    GSList* treelist = tree->list;

    if (!name)
    {
        return NULL;
    }

    if (flag)
    {
        item = NULL;
    }
    
    if (!item)
    {

        if (tree->type == type)
        {
            if (!strcmp ((char*)test_list_parse_string (name), 
                         (char*)test_list_parse_string (tree->name)))
            {
                item = tree;
            } else {
            }
        }
        
        while (treelist)
        {
            TestListItem* item; 
            item = real_test_list_find_node_by_name_and_type 
                ((TestListItem*)treelist->data, name, type, 0);
            if (item)
            {
                break;
            }
            treelist = treelist->next;
        }
    }
    return item;
}    

gchar* test_list_get_label (TestListItem* item)
{
    if (item->state == TESTED_DOCUMENTED)
    {
        return "+";
    } else if (item->state == NOT_TESTED_DOCUMENTED) {
        return "-";
    } else if (item->state == TESTED_NOT_DOCUMENTED) {
        return "*";
    } else if (item->state == NOT_TESTED_NOT_DOCUMENTED) {
        return "#";
    } else {
        printf ("warning: wrong item->state\n");
        return "";
    }
}

gboolean test_list_has_child (TestListItem* tree)
{
    GSList* list = tree->list;

    if (((list_type == -1) || (tree->type == list_type)) && ((tree->state == list_state) || (list_state == -1)))
        return TRUE;

    while (list)
    {
        TestListItem* node = list->data;
        if (((list_type == -1) || (node->type == list_type)) && ((node->state == list_state) || (list_state == -1)))
            return TRUE;

        if ((node->list) && (node->type < list_type))
            if (test_list_has_child (node))
                return TRUE;
        list = list->next;
    }
    return FALSE;
}


void test_list_set_params (gchar* params)
{
    if (!params || strlen (params) < 2) return;
    
    if (!strcmp (params, "-class") || !strcmp (params, "-c"))
        list_type = CLASS;
    if (!strcmp (params, "-function") || !strcmp (params, "-f"))
        list_type = FUNCTION;
    if (!strcmp (params, "-assertion") || !strcmp (params, "-a"))
        list_type = ASSERTION;

    if (!strcmp (params, "-tested_documented") || !strcmp (params, "-td"))
        list_state = TESTED_DOCUMENTED;
    if (!strcmp (params, "-not_tested_documented") || !strcmp (params, "-ntd"))
        list_state = NOT_TESTED_DOCUMENTED;
    if (!strcmp (params, "-tested_not_documented")|| !strcmp (params, "-tnd"))
        list_state = TESTED_NOT_DOCUMENTED;
    if (!strcmp (params, "-not_tested_not_documented") || !strcmp (params, "-ntnd"))
        list_state = NOT_TESTED_NOT_DOCUMENTED;
}


void test_list_print_1 (TestListItem* tree, FILE* file)
{
    GSList* list = tree->list;
    while (list)
    {
        TestListItem* node = list->data;

        if (node->type == CLASS)
        {
            cl[node->state]++;
            if (test_list_has_child (node))
                fprintf (file, "\n\n\n%s %s ", test_list_get_label (node), node->name);
        }

        if (node->type == FUNCTION)
        {
            fu[node->state]++;
            if (test_list_has_child (node))
                fprintf (file, "\n\n\t%s %s", test_list_get_label (node), node->name);
        }

        if (node->type == ASSERTION)
        {
            as[node->state]++;
            if (test_list_has_child (node))
                fprintf (file, "\n\t\t%s '%s'", test_list_get_label (node), node->name);
        }

        if (node->list)
        {
            test_list_print_1 (node, file);
        }
        list = list->next;
    }
}


void test_list_print (TestListItem* tree, gchar* filename)
{
    gint i;
    FILE* file;

    for (i = 0; i < 4; i++)
    {
        cl[i] = 0;
        fu[i] = 0;
        as[i] = 0;
    }

    file = fopen (filename, "w");
    test_list_print_1 (tree, file);


    fprintf (file, "\n\n\nTOTAL:");

    fprintf (file, "\n____________________________________________________________________\n");

    fprintf (file, "\ntested  documented |\tclasses\t\tfunctions\tassertions\n");
    fprintf (file, "____________________________________________________________________\n");
    fprintf (file, "\nyes     yes        |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[1], 100.0*cl[1]/(cl[0]+cl[1]+cl[2]+cl[3]), '%',
            fu[1], 100.0*fu[1]/(fu[0]+fu[1]+fu[2]+fu[3]), '%',
            as[1], 100.0*as[1]/(as[0]+as[1]+as[2]+as[3]), '%');
    fprintf (file, "\nno      yes        |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[0], 100.0*cl[0]/(cl[0]+cl[1]+cl[2]+cl[3]), '%', 
            fu[0], 100.0*fu[0]/(fu[0]+fu[1]+fu[2]+fu[3]), '%', 
            as[0], 100.0*as[0]/(as[0]+as[1]+as[2]+as[3]), '%');
    fprintf (file, "\nyes     no         |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[2], 100.0*cl[2]/(cl[0]+cl[1]+cl[2]+cl[3]), '%',
            fu[2], 100.0*fu[2]/(fu[0]+fu[1]+fu[2]+fu[3]), '%', 
            as[2], 100.0*as[2]/(as[0]+as[1]+as[2]+as[3]), '%');
    fprintf (file, "\nno      no         |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[3], 100.0*cl[3]/(cl[0]+cl[1]+cl[2]+cl[3]), '%',
            fu[3], 100.0*fu[3]/(fu[0]+fu[1]+fu[2]+fu[3]), '%', 
            as[3], 100.0*as[3]/(as[0]+as[1]+as[2]+as[3]), '%');
    fprintf (file, "____________________________________________________________________\n");
    fprintf (file, "\ntotal              |\t%4i\t\t%4i\t\t%4i\n", cl[0]+cl[1]+cl[2]+cl[3], fu[0]+fu[1]+fu[2]+fu[3], as[0]+as[1]+as[2]+as[3]);
    

    fprintf (file, "\n\n____________________________________________________________________\n\n");
    fprintf (file, "test_cov: ");

    if (list_type == -1)
        fprintf (file, "all elements, ");
    if (list_type == CLASS)
        fprintf (file, "classes, ");
    if (list_type == FUNCTION)
        fprintf (file, "functions, ");
    if (list_type == ASSERTION)
        fprintf (file, "assertions, ");

    if (list_state == -1)
        fprintf (file, "all assertions");
    if (list_state == TESTED_DOCUMENTED)
        fprintf (file, "tested and documented ");
    if (list_state == NOT_TESTED_DOCUMENTED)
        fprintf (file, "not tested but documented ");
    if (list_state == TESTED_NOT_DOCUMENTED)
        fprintf (file, "tested but not documented ");
    if (list_state == NOT_TESTED_NOT_DOCUMENTED)
        fprintf (file, "not documented and not tested ");

    fprintf (file, "\n____________________________________________________________________\n");
    fprintf (file, "\nLegend:\n");
    fprintf (file, "\t'+' - tested and documented\n");
    fprintf (file, "\t'-' - not tested but documented\n");
    fprintf (file, "\t'*' - tested but not documented\n");
    fprintf (file, "\t'#' - not tested and not documented\n");
    fprintf (file, "\n____________________________________________________________________\n");


    fclose (file);

    printf("\n");
    printf ("\ntested  documented |\tclasses\t\tfunctions\tassertions\n");
    printf ("____________________________________________________________________\n");
    printf ("\nyes     yes        |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[1], 100.0*cl[1]/(cl[0]+cl[1]+cl[2]+cl[3]), '%',
            fu[1], 100.0*fu[1]/(fu[0]+fu[1]+fu[2]+fu[3]), '%',
            as[1], 100.0*as[1]/(as[0]+as[1]+as[2]+as[3]), '%');
    printf ("\nno      yes        |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[0], 100.0*cl[0]/(cl[0]+cl[1]+cl[2]+cl[3]), '%', 
            fu[0], 100.0*fu[0]/(fu[0]+fu[1]+fu[2]+fu[3]), '%', 
            as[0], 100.0*as[0]/(as[0]+as[1]+as[2]+as[3]), '%');
    printf ("\nyes     no         |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[2], 100.0*cl[2]/(cl[0]+cl[1]+cl[2]+cl[3]), '%',
            fu[2], 100.0*fu[2]/(fu[0]+fu[1]+fu[2]+fu[3]), '%', 
            as[2], 100.0*as[2]/(as[0]+as[1]+as[2]+as[3]), '%');
    printf ("\nno      no         |\t%4i (%4.2f%c)\t%4i (%4.2f%c)\t%4i (%4.2f%c)\n", 
            cl[3], 100.0*cl[3]/(cl[0]+cl[1]+cl[2]+cl[3]), '%',
            fu[3], 100.0*fu[3]/(fu[0]+fu[1]+fu[2]+fu[3]), '%', 
            as[3], 100.0*as[3]/(as[0]+as[1]+as[2]+as[3]), '%');
    printf ("____________________________________________________________________\n");
    printf ("\ntotal              |\t%4i\t\t%4i\t\t%4i\n", cl[0]+cl[1]+cl[2]+cl[3], fu[0]+fu[1]+fu[2]+fu[3], as[0]+as[1]+as[2]+as[3]);
    printf("\n");
}


