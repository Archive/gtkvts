#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <common.h>

int 
exec_program (const char* path, char *const argv[])
{
    int pid;
    int status;
    
    if (path == 0)
    {
        return -1;
    }

    pid = fork();

    if (pid == -1)
    {
        return -1;
    }

    if (pid == 0) 
    {
        if (execv (path, argv) == -1)
        {
            exit (-1);
        }
    }
    
    do 
    {
        if (waitpid(pid, &status, 0) == -1) 
        {
            if (errno != EINTR) {               
                return -1;
            }
        } else {
            return WEXITSTATUS(status);
        }
    } while(1);
}
