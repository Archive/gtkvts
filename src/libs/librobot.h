#ifndef ROBOT_H
#define ROBOT_H


#include <gtk/gtk.h>



/*
 * function prototypes
 */


void   
robot_activate (GtkWidget* widget);

gboolean   
robot_button_press_event (GtkWidget* widget, guint button, gdouble x, gdouble y, guint state);

gboolean   
robot_button_release_event (GtkWidget* widget, guint button, gdouble x, gdouble y, guint state);

gboolean   
robot_button_dblclk_event (GtkWidget* widget, guint button, gdouble x, gdouble y, guint state);

gboolean   
robot_configure_event (GtkWidget* widget, gint16 x, gint16 y, gint16 width, gint16 height);

gboolean   
robot_delete_event (GtkWidget* widget);

gboolean   
robot_destroy_event (GtkWidget* widget);

gboolean   
robot_event (GtkWidget* widget, gchar* event_name, GdkEvent* e, gpointer user_data);

gboolean   
robot_focus_in_event (GtkWidget* widget);

gboolean   
robot_focus_out_event (GtkWidget* widget);

void   
robot_hide (GtkWidget* widget);

gboolean   
robot_key_press_event (GtkWidget* widget, guint keyval, guint state);

gboolean   
robot_key_release_event (GtkWidget* widget, guint keyval, guint state);

gboolean   
robot_motion_notify_event (GtkWidget* widget, gdouble x, gdouble y, guint state);

void   
robot_show (GtkWidget* widget);


#endif /* ROBOT_H */
