#ifndef LIBTESTCOV_H
#define LIBTESTCOV_H

typedef enum 
{
    ROOT,
    CLASS,
    FUNCTION,
    ASSERTION,
    UNDEFINED,
} TestListType;


typedef enum 
{
    NOT_TESTED_DOCUMENTED,
    TESTED_DOCUMENTED,
    TESTED_NOT_DOCUMENTED,
    NOT_TESTED_NOT_DOCUMENTED,
    EXCLUDED,
} TestListState;


typedef struct _TestListItem
{
    gchar* name;
    TestListType type;
    TestListState state;
    gboolean is_deprecated;
    GSList* list;
} TestListItem; 


char* cut_tags (char *text);
void gen_asr_list_from_tree (TestListItem *tree, 
                                    char* asr_list_file_name);
void gen_hdr_list_from_tree (TestListItem *tree, 
                                    char* hdr_list_file_name);
char* get_tag_contents (char* source, const char* tag, 
                        const char* must_be_tag, const char* start, 
                        const char *end);
char* split_asr_set (char* asr_set);
TestListItem* test_list_new ();
TestListItem* test_list_add (TestListItem* node, gchar* name, TestListType type, TestListState state, gboolean is_depr);
TestListItem* test_list_find  (TestListItem* tree, gchar* name);
void   test_list_free (TestListItem* tree);
void   test_list_add_source_file (TestListItem* list, gchar* filename);
void   test_list_add_assertion_file (TestListItem* list, gchar* filename);
void test_list_add_exclude_list_file (TestListItem* list, gchar* filename);
void test_list_print (TestListItem* tree, gchar* filename);
void test_list_set_params (gchar* params);
TestListItem* test_list_find_node_by_name_and_type 
    (TestListItem* tree, gchar* name, TestListType type);
void test_list_append_exclude_list_file (TestListItem* list, gchar* filename, 
                                         gchar* ignore_list);

#endif /* LIBTESTCOV_H */
