#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <common.h>

#include "libfile.h"
#include "libtestgen.h"


/*
 *   Global variables
 */
static gint test_synchronize_events_count = 0;
static GtkCallback test_func = NULL;
const static gint sleep_delay = 200000;

/*
 * Get copyright notice from file 
 */
char*
get_copyright_notice (char* path)
{
    FILE* fCopyright = NULL;
    char* copyright = NULL;
    fCopyright = open_file (path,"r",NULL);
    if (fCopyright != NULL) 
    {
        copyright = read_file_to_string (fCopyright);
        fclose (fCopyright);
    }
    return copyright;
}

/*
 *   Send "synchronize" event (GDK_KEY_PRESS, keyval = 0)
 */
static void
test_send_synchronize_event (GtkWidget* widget)
{
    const guint keyval = 0;
    GdkEventKey e;

    e.type = GDK_KEY_PRESS;
    e.string = gdk_keyval_name (keyval);
    e.length = 1;
    e.state = 0;
    e.group = 0;
    e.hardware_keycode = 0;
    e.keyval = keyval;
    e.window = widget->window;
    e.send_event = TRUE;
    e.time = GDK_CURRENT_TIME;

    g_object_ref (G_OBJECT (widget->window));
    gtk_main_do_event ((GdkEvent*)&e);
    g_object_unref (G_OBJECT (widget->window));
}


/*
 *   Handles GDK_EXPOSE, GDK_MAP and "synchronize" events.
 *   
 *   When GDK_EXPOSE and GDK_MAP events received (i.e. window shown), 
 *   send "synchronize" events.
 *   If there no events between two "synchronize" events, 
 *   the main events queue is empty and quit main loop.
 */

static gboolean    
test_catch_synchronize_event (GtkWidget *widget, GdkEvent *event, gpointer data)
{

    if (!GTK_WIDGET_REALIZED (widget)) return FALSE;

    if (event->type == GDK_EXPOSE)
    {
        test_send_synchronize_event (widget);
        test_synchronize_events_count = 0;
        return FALSE;
    }

    if (event->type == GDK_MAP)
    {
        GdkRectangle rect;
        GtkWidget* top = gtk_widget_get_toplevel (widget);
        gdk_window_get_frame_extents (top->window, &rect);
        gdk_window_invalidate_rect (top->window, &rect, TRUE);
        gdk_window_process_all_updates ();
        return FALSE;
    }

    if ((event->type == GDK_KEY_PRESS) &&
       (((GdkEventKey*)event)->state == 0) &&
       (((GdkEventKey*)event)->keyval == 0))
    {
        test_synchronize_events_count++;
        if (test_synchronize_events_count < 2)
        {
            gdk_window_process_all_updates ();
            test_send_synchronize_event (widget);
            return TRUE;

        } else {

            if (test_func)
            {
                (*test_func) (widget, NULL);
                test_func = NULL;
                test_send_synchronize_event (widget);
                test_synchronize_events_count = 0;
                return FALSE;
            }

            test_synchronize_events_count = 0;
            if (gtk_main_level () > 0)
                gtk_main_quit ();
        }

    } else {

            test_synchronize_events_count = 0;
    }

    return FALSE;
}


/*
 *   Includes widget in toplevel container, show and enter main loop
 */

void  
test_visualize (GtkWidget* widget, GtkCallback func) 
{
    GtkWidget* window = widget;

    if (!widget) return;

    if ((!GTK_WIDGET_TOPLEVEL (widget) && !GTK_IS_MENU_SHELL (widget)) || (GTK_IS_MENU_BAR (widget)))
    {
        window = gtk_window_new (GTK_WINDOW_TOPLEVEL); 
        if (!window) return;
        g_object_ref (G_OBJECT (widget));
        gtk_container_add (GTK_CONTAINER (window), 
                           gtk_widget_get_toplevel (widget)); 
        gtk_window_set_position (GTK_WINDOW (window), 
                                 GTK_WIN_POS_CENTER_ALWAYS);
        gtk_container_set_border_width (GTK_CONTAINER (window), 20); 
    }
    test_func = func;
    g_signal_connect (G_OBJECT (window), "event", GTK_SIGNAL_FUNC (test_catch_synchronize_event), NULL);  
    gtk_widget_show_all (window); 
    gtk_main (); 
    g_usleep (sleep_delay); 

}

