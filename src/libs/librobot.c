#include "librobot.h"


#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>
#include <X11/Shell.h>
#include <X11/StringDefs.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Intrinsic.h>
#include <X11/keysymdef.h>
#include <X11/Intrinsic.h>
#include <gdk/gdkkeysyms.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <poll.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <common.h>



void   
robot_activate (GtkWidget* widget)
{
    gtk_signal_emit_by_name (GTK_OBJECT (widget), "activate");
}


gboolean   
robot_button_event (GtkWidget* widget, GdkEventType type, guint button, gdouble x, gdouble y, guint state)
{
    GdkEventButton  e;
    gboolean result = FALSE;
    
    e.type = type;
    e.x = x;
    e.y = y;
    e.x_root = 0;
    e.y_root = 0;
    e.window = widget->window;
    e.state = state;
    e.time = CurrentTime;
    e.button = button;
    e.device = (GdkDevice*)0xFEDC;
    e.axes = NULL;
    if (type == GDK_BUTTON_RELEASE)
    {
        gtk_signal_emit_by_name  (GTK_OBJECT (widget), "button-release-event", &e, &result);

    } else {

        gtk_signal_emit_by_name  (GTK_OBJECT (widget), "button-press-event", &e, &result);
    }

    return result;
}


gboolean   
robot_button_press_event (GtkWidget* widget, guint button, gdouble x, gdouble y, guint state)
{
    return robot_button_event (widget, GDK_BUTTON_PRESS, button, x, y, state);
}


gboolean   
robot_button_release_event (GtkWidget* widget, guint button, gdouble x, gdouble y, guint state)
{
    return robot_button_event (widget, GDK_BUTTON_RELEASE, button, x, y, state);
}


gboolean   
robot_button_dblclk_event (GtkWidget* widget, guint button, gdouble x, gdouble y, guint state)
{
    gboolean result;
    result = robot_button_event (widget, GDK_BUTTON_PRESS, button, x, y, state);
    result &= robot_button_event (widget, GDK_BUTTON_RELEASE, button, x, y, state);
    result &= robot_button_event (widget, GDK_BUTTON_PRESS, button, x, y, state);
    result &= robot_button_event (widget, GDK_2BUTTON_PRESS, button, x, y, state);
    result &= robot_button_event (widget, GDK_BUTTON_RELEASE, button, x, y, state);
    return result;
}


gboolean   
robot_configure_event (GtkWidget* widget, gint16 x, gint16 y, gint16 width, gint16 height)
{
    GdkEventConfigure  e;
    gboolean result = FALSE;
    
    e.type = GDK_CONFIGURE;
    e.x = x;
    e.y = y;
    e.width = width;
    e.height = height;
    e.window = widget->window;
    gtk_signal_emit_by_name  (GTK_OBJECT (widget), "configure-event", &e, &result);

    return result;
}


gboolean   
robot_delete_event (GtkWidget* widget)
{
    GdkEventAny e;
    gboolean result = FALSE;

    e.type = GDK_DELETE;
    e.window = widget->window;
    e.send_event = TRUE;
    gtk_signal_emit_by_name  (GTK_OBJECT (widget), "delete-event", &e, &result);

    return result;
}


gboolean   
robot_destroy_event (GtkWidget* widget)
{
    GdkEventAny e;
    gboolean result = FALSE;

    e.type = GDK_DESTROY;
    e.window = widget->window;
    e.send_event = TRUE;
    gtk_signal_emit_by_name  (GTK_OBJECT (widget), "destroy-event", &e, &result);

    return result;
}


gboolean   
robot_event (GtkWidget* widget, gchar* event_name, GdkEvent* e, gpointer user_data)
{
    gboolean result = FALSE;

    gtk_signal_emit_by_name  (GTK_OBJECT (widget), event_name, e, user_data, &result);

    return result;
}


gboolean   
robot_focus_in_event (GtkWidget* widget)
{
    GdkEventFocus e;
    gboolean result = FALSE;

    e.type = GDK_FOCUS_CHANGE;
    e.in = TRUE;
    e.window = widget->window;
    e.send_event = TRUE;
    gtk_signal_emit_by_name  (GTK_OBJECT (widget), "focus-in-event", &e, &result);

    return result;
}


gboolean   
robot_focus_out_event (GtkWidget* widget)
{
    GdkEventFocus e;
    gboolean result = FALSE;

    e.type = GDK_FOCUS_CHANGE;
    e.in = FALSE;
    e.window = widget->window;
    e.send_event = TRUE;
    gtk_signal_emit_by_name  (GTK_OBJECT (widget), "focus-out-event", &e, &result);

    return result;
}


void   
robot_hide (GtkWidget* widget)
{
    gtk_signal_emit_by_name (GTK_OBJECT (widget), "hide");
}


static void
robot_set_keyfields (GdkEventKey* e, guint keyval, guint state)
{
    GdkKeymapKey* keys;
    gint count;
    
    e->string = gdk_keyval_name (keyval);
    e->length = 1;
    e->state = state;
    e->keyval = keyval;
    e->group = 0;
    e->hardware_keycode = 0;

    if (gdk_keymap_get_entries_for_keyval (NULL, keyval, &keys, &count))
    {
        e->hardware_keycode = keys[0].keycode;
        e->group = keys[0].group;
        if (keys[0].level == 1)
            e->keyval |= GDK_SHIFT_MASK;
        g_free (keys);
    }
}


gboolean   
robot_key_press_event (GtkWidget* widget, guint keyval, guint state)
{
    GdkEventKey e;
    gboolean result = FALSE;

    e.type = GDK_KEY_PRESS;
    e.window = widget->window;
    e.send_event = TRUE;
    e.time = CurrentTime;

    robot_set_keyfields (&e, keyval, state);

    g_signal_emit_by_name  (GTK_OBJECT (widget), "key-press-event", &e, &result);

    return result;
}


gboolean   
robot_key_release_event (GtkWidget* widget, guint keyval, guint state)
{
    GdkEventKey e;
    gboolean result = FALSE;

    e.type = GDK_KEY_RELEASE;
    e.window = widget->window;
    e.send_event = TRUE;
    e.time = CurrentTime;

    robot_set_keyfields (&e, keyval, state);

    g_signal_emit_by_name  (GTK_OBJECT (widget), "key-release-event", &e, &result);

    return result;
}


gboolean   
robot_motion_notify_event (GtkWidget* widget, gdouble x, gdouble y, guint state)
{
    GdkEventMotion  e;
    gboolean result = FALSE;
    
    e.type = GDK_MOTION_NOTIFY;
    e.x = x;
    e.y = y;
    e.x_root = 0;
    e.y_root = 0;
    e.window = widget->window;
    e.state = state;
    e.time = CurrentTime;
    e.device = (GdkDevice*)0xFEDC;
    e.axes = NULL;
    gtk_signal_emit_by_name  (GTK_OBJECT (widget), "motion-notify-event", &e, &result);

    return result;
}

void   
robot_show (GtkWidget* widget)
{
    gtk_signal_emit_by_name (GTK_OBJECT (widget), "show");
}





