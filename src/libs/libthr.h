#ifndef LIBTHR_H
#define LIBTHR_H

#define EMPTY_PATH -1
#define CANT_FORK -2

extern int exec_program (const char *path, char *const argv[]);

#endif /* LIBTHR_H */
