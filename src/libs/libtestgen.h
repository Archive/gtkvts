#ifndef LIBTESTGEN_H
#define LIBTESTGEN_H

#include <gtk/gtk.h>

#define TEST_VISUALIZE(widget) {test_visualize(widget, NULL);}
#define TEST_VISUALIZE_AND_DO(widget,func) {test_visualize(widget, func);}
#define TEST_VISUALIZE_TOPLEVEL(widget) {test_visualize(widget, NULL);}

extern char* get_copyright_notice (char* path);

extern void test_visualize (GtkWidget* widget, GtkCallback func);

#endif /* LIBTESTGEN_H */

