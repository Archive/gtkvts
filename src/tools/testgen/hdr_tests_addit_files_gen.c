/*
 * This program generates .c files that used in header tests
 *
 * The template is
 * 
 * <include files>
 * int
 * main (int argc, char** argv)
 * {
 *     <return holder and function arguments declarations>
 *     <function call>
 * }
 * 
 * The structure of argv argument is:
 * 0th string - not used
 * 1st string - what to generate (e.g. GEN_INC_BLK)
 * 2nd string - generated file name (must be *.c or "")
 * 3rd string - include files (e.g. "gtkarrow.h");
 * 4th string - function returning type (e.g. "void");
 * 5th string - function name (e.g. "gtk_arrow_set");
 * 6th string - function parameters (e.g. "int* q").
 */   

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <common.h>
#include "hdr_tests_addit_files_gen.h"
#include "libfile.h"
#include "libmem.h"
#include "libstr.h"

#define TRUE 1
#define FALSE 0

static void gen_declarations(FILE* pfile, 
			     int is_void_func, int func_has_params, 
			     char* ret_type, char* func_name, char* params);
static void gen_enable_broken (FILE* pfile);
static void gen_func_call(FILE* pfile, 
			  int is_void_func, int func_has_params, 
			  char* func_name, char* ret_type, char* params);
static void gen_include_files_block (FILE* pfile, char* include_files);
static void gen_last_bracket (FILE* pfile);
static void gen_main_func_start (FILE* pfile);

char default_gen_file_name[] = "gen_file.c";
char correct_extension[] = ".c";
char default_func_name[] = "f";
char default_param[] = "int iii";

int 
main (int argc, char** argv) 
{

    FILE* pfile;
    int is_void_func = FALSE;
    int func_has_params = FALSE;
    int gen_inc_blk = FALSE;
    int gen_main_func = FALSE;

    char *func_params = NULL;

    pfile = open_file (argv[GENERATED_FILE_NAME_POS], "wt", ".c");

    if (!strcmp (argv[FUNC_RET_TYPE_POS], "void"))
    {
	is_void_func = TRUE;
    }

    if (strlen (argv[FUNC_PARAMS_POS]) && 
        strcmp (argv[FUNC_PARAMS_POS], "void"))
    { 
	func_has_params = TRUE;
    }

    if (func_has_params)
    {
        char *str;

        func_params = alloc_mem_for_string 
            (func_params, strlen (argv[FUNC_PARAMS_POS]));
        strcpy (func_params, argv[FUNC_PARAMS_POS]);

        str = strstr (func_params, "argc");
        if (str)
        {
            *(str + 3) = 'C';
        }

        str = strstr (func_params, "argv");
        if (str)
        {
            *(str + 3) = 'V';
        }
    }

    if (atoi (argv[WHAT_TO_GEN_POS]) & GEN_INC_BLK) 
    {
        gen_inc_blk = TRUE;
    }

    if (atoi (argv[WHAT_TO_GEN_POS]) & GEN_MAIN_FUNC) 
    {
        gen_main_func = TRUE;
    }

    gen_enable_broken (pfile);

    if (gen_inc_blk)
    {
        gen_include_files_block(pfile, argv[INCLUDE_FILES_POS]);
    }

    if (gen_main_func)
    {        
        gen_main_func_start(pfile);
        gen_declarations(pfile, is_void_func, func_has_params, 
                         argv[FUNC_RET_TYPE_POS], argv[FUNC_NAME_POS], 
                         func_params);
        gen_func_call(pfile, is_void_func, func_has_params,
                      argv[FUNC_NAME_POS], argv[FUNC_RET_TYPE_POS], 
                      func_params);
        gen_last_bracket(pfile);
    }
    
    fclose (pfile);
    free (func_params);

    exit (EXIT_CODE_OK);
}

/* generate declarations of params and return value holders */
static void
gen_declarations(FILE* pfile, int is_void_func, int func_has_params, 
		 char* ret_type, char* func_name, char* params)
{
    char* pchar;
    char* pbracket;
    char* aster;
    char* pars = NULL;    

    if (func_has_params)
    {
        pars = (char*) calloc (1, strlen (params) + 1);
        strcpy (pars, params);
    }

    /* generate declaration of return value holder */
    if (!is_void_func)
    {
	fprintf (pfile, "    %s ret;\n", ret_type);
    }

    /* generate declarations of params holders */
    if (func_has_params)
    {
	pchar = strtok (pars, ",");
	while (pchar)
	{
            char* pchar1 = strdup (pchar);
            pchar1 = trim (pchar1);
            pbracket = strchr (pchar1, '[');
            if (pbracket && *(pbracket + 1) == ']' && *(pbracket + 2) == '\0')
            {
                pchar1 = alloc_mem_for_string (pchar1, strlen (pchar1) + 1);
                *(pbracket + 1) = '2';
                *(pbracket + 2) = ']';
            }

            if (!strcmp (pchar1, "..."))
            {
                char* new_param = NULL;
                new_param = alloc_mem_for_string 
                    (new_param, strlen (default_param));
                strcpy (new_param, default_param);
                pchar1 = new_param;
            }

	    fprintf (pfile, "    %s;\n", pchar1);
	    pchar = strtok (NULL, ",");
	}
    }

    aster = strchr (func_name, '*');
    if (aster)
    {
        func_name = strtok (aster + 1, ")");
        fprintf (pfile, "    %s *%s;\n", func_name, default_func_name);
    }

    free (pars);

}

static void
gen_enable_broken (FILE* pfile)
{
    fprintf (pfile, "#define GTK_ENABLE_BROKEN\n\n");
}

/* generate function call */
static void
gen_func_call (FILE* pfile, int is_void_func, int func_has_params, 
		 char* func_name, char* ret_type, char* params)
{
    char* pchar;
    char* pchar1;
    char* pbracket;
    char* pars = NULL;
    int i;

    if (func_has_params)
    {
        pars = (char*) calloc (1, strlen (params) + 1);
        strcpy (pars, params);
    }

    if (!is_void_func)
    {
	fprintf (pfile, "    ret = ");
        fprintf (pfile, "(%s) ", ret_type);
    } else {
	fprintf (pfile, "    ");
    }

    if (strchr (func_name, '*'))
    {
        fprintf (pfile, "(*%s) (", default_func_name);
    } else {
        fprintf (pfile, "%s (", func_name);
    }

    if (func_has_params) {
	pchar = strtok (pars,  ",");
	while (pchar)
	{
            pchar = trim (pchar);
            if (!strcmp (pchar, "..."))
            {
                char* new_pchar = NULL;
                new_pchar = alloc_mem_for_string 
                    (new_pchar, strlen (default_param));
                strcpy (new_pchar, default_param);
                pchar = new_pchar;
            }
                
	    pchar1 = strrchr (pchar, ' ');
	    if (pchar1 && *(pchar1 + 1))
	    {
                for (i = 1; i < strlen (pchar1 + 1); i++)
                {
                    if (*(pchar1 + i) != '*')
                    {
                        break;
                    }
                }

                if (!strlen (pchar1 + i))
                { 
                    exit (EXIT_CODE_INCORRECT_PARAMS_STR_FORMAT);
                }

                pbracket = strchr (pchar1 + i, '[');
                if (pbracket && *(pbracket + 1) == ']' && *(pbracket + 2) == '\0')
                {
                    *pbracket = '\0';
                }

		fprintf (pfile, "%s", pchar1 + i);
	    } else {
		exit (EXIT_CODE_INCORRECT_PARAMS_STR_FORMAT);
	    }
	    pchar = strtok (NULL, ",");
	    if (pchar)
	    {
		fprintf (pfile, ", ");
	    }
	}
        free (pars);
    }

    fprintf (pfile, ");\n");
    
}

/*  generate include files block */
static void 
gen_include_files_block (FILE* pfile, char* include_files)
{
    char* pchar;

    /* generate include files block */
    pchar = strtok (include_files, ",");
    while (pchar)
    {
	fprintf (pfile, "#include %s\n", pchar);
	pchar = strtok (NULL, ",");
    }
}

/* generate the last curved bracket */
static void
gen_last_bracket (FILE* pfile)
{
    fprintf (pfile, "}\n");
}

/*
 * generate this block below:
 * void
 * main (int argc, char** argv)
 * {
 */
static void 
gen_main_func_start (FILE* pfile)
{
    fprintf (pfile, "\n");
    fprintf (pfile, "int\n");
    fprintf (pfile, "main (int argc, char** argv)\n");
    fprintf (pfile, "{");
    fprintf (pfile, "\n");
}


