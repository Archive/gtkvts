/*
 * This program generates header tests.
 *
 * The structure of argv argument is:
 * 0th string - not used
 * 1st string - path gtkvts root
 * 2nd string - path where to put generated tests
 * 3rd string - path to scenario files dir
 * 4th string - path to the file with the list of functions to be tested
 * 5th string - path to the file containing the copyright notice
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <common.h>
#include "hdr_tests_generator.h"
#include "libfile.h"
#include "libmem.h"
#include "libstr.h"
#include "libtestgen.h"

static void add_item_to_scenario_file (char* scen_file, char* item);
static void add_item_to_tet_scen_file (char* tet_scen_file, char* item);
static void append_copyright_notice (char* file_name, char* notice);
static void append_tet_hook (char *file_name, char* tet_hook);
static void gen_finish_func (char *file_path);
static void gen_header_tests (char* gtkvts_root, char *store_path, 
                              char* scenario_file, char* func_list_file,
                              char* copyright_notice_file_name);
static void gen_header_test_makefile (char* gtkvts_root, char* dir, 
                                      char* current_test_file_name);
static void gen_includes_block (char *file_path);
static void gen_runtest_func (char *file_path);
static void gen_start_decls (char *file_path, char *group_name);
static void gen_start_func (char *file_path, char *group_name);
static void gen_test_failed_func (char *file_path);
static void gen_test_passed_func (char *file_path);
static char* gen_test_purpose (char *file_path, char* test_case_name, 
                           char* func_decl, char* tet_hook, 
                           int tet_hook_tail_length);
static char* gen_tet_hook_common_parts (char *tet_hook);
static char* get_app_name ();

const char comment_char = '#';
const char function_name_char = '&';
const char group_name_char = '@';

const char* test_case_name_prefix = "test_purpose_";
char* hdr_tests_scenario_file_name = "headers_scen";

int tet_hook_tail_length = 15;

int 
main (int argc, char** argv) 
{
    char* gtkvts_root;
    char* func_list_file;
    char* generated_tests_path;
    char* copyright_notice_file_name;
    char* tet_scen_file;
    FILE* p_func_list_file;
    char* gtkvts_root1;
    char* func_list_file1;
    char* generated_tests_path1;
    char* copyright_notice_file_name1;
    char* scenario_file;
    char* scenario_file2;
    char* scenario_file1 = NULL;
    char* cur_work_dir = (char*) calloc (1, 1024);
    
    cur_work_dir = getcwd (cur_work_dir, 1024);

    gtkvts_root1 = concat_paths (cur_work_dir, argv[GTKVTS_ROOT_POS]);
    gtkvts_root = shorten_path (gtkvts_root1);
    
    tet_scen_file = argv[TET_SCEN_FILE_POS];

    func_list_file1 = concat_paths (cur_work_dir, argv[FUNC_LIST_PATH_POS]);
    func_list_file = shorten_path (func_list_file1);

    p_func_list_file = open_file (func_list_file, "r", NULL);
    if (!is_file_exists (func_list_file) || fsize (p_func_list_file) == 0)
    {
        exit (0);
    }

    generated_tests_path1 = concat_paths (cur_work_dir, 
                                          argv[GEN_TESTS_PATH_POS]);
    generated_tests_path = shorten_path (generated_tests_path1);

    copyright_notice_file_name1 = concat_paths (cur_work_dir, argv[COPYRIGHT_PATH_POS]);
    copyright_notice_file_name = shorten_path (copyright_notice_file_name1);

    if (!is_directory_exists (argv[SCEN_DIR_PATH_POS]))
    {
        create_dir (argv[SCEN_DIR_PATH_POS]);
    }
    scenario_file1 = alloc_mem_for_string (scenario_file1, 
                                          strlen (argv[SCEN_DIR_PATH_POS]));
    scenario_file1 = str_append (scenario_file1, argv[SCEN_DIR_PATH_POS]);
    scenario_file1 = str_append (scenario_file1, "/");
    scenario_file1 = str_append (scenario_file1, hdr_tests_scenario_file_name);
    scenario_file2 = concat_paths (cur_work_dir, scenario_file1);
    scenario_file = shorten_path (scenario_file2);

    gen_header_tests (
        gtkvts_root,
        generated_tests_path, 
        scenario_file,
        func_list_file, 
        copyright_notice_file_name);

    if (!is_file_exists (tet_scen_file))
    {
        add_item_to_tet_scen_file (tet_scen_file, "all\n");
    }
    add_item_to_tet_scen_file (tet_scen_file, 
                               "\t:include:/scenarios/headers_scen\n");

    free (cur_work_dir);
    free (gtkvts_root);
    free (gtkvts_root1);
    free (func_list_file);
    free (func_list_file1);
    free (generated_tests_path);
    free (generated_tests_path1);
    free (copyright_notice_file_name);
    free (copyright_notice_file_name1);
    free (scenario_file);
    free (scenario_file1);
    free (scenario_file2);

    fclose (p_func_list_file);

    return 0;
}
    
/*
 * Adds an item to a scenario file
 */
static void
add_item_to_scenario_file (char* scen_file, char* item)
{

    FILE* p_scen_file = open_file (scen_file, "a", NULL);
    
    fprintf (p_scen_file, "\t");
    fprintf (p_scen_file, item);
    fprintf (p_scen_file, "\n");

    fclose (p_scen_file);
}

/*
 * Adds an item to a scenario file
 */
static void
add_item_to_tet_scen_file (char* tet_scen_file, char* item)
{
    FILE* p_scen_file = open_file (tet_scen_file, "a", NULL);
    
    fprintf (p_scen_file, item);

    fclose (p_scen_file);
}


/*
 * Append the copyright notice to a test file
 */
static void 
append_copyright_notice (char* file_name, char* notice)
{
    FILE* file = open_file (file_name, "a", NULL);
    
    if (notice)
    {
        fprintf (file, notice);
        fprintf (file, "\n");
    }

    fclose (file);
}
    
/*
 * Append TET hook to a test file
 */
void
append_tet_hook (char* file_name, char* tet_hook)
{
    FILE* file = open_file (file_name, "a", NULL);

    if (tet_hook)
    {
        fprintf (file, "\n");
        fprintf (file, tet_hook);
    }

    fclose (file);
}

/*
 * Generate finish_func() for a test file
 */
static void
gen_finish_func (char *file_path)
{
    FILE* file = open_file (file_path, "a", "c");

    fprintf (file, "\n");
    fprintf (file, "void finish_func (void)\n");
    fprintf (file, "{\n");
    fprintf (file, "    fprintf (stderr, \" %%d passed\", test_purp_num - test_purp_failed_num);\n");
    fprintf (file, "\n");
    fprintf (file, "    if (test_purp_failed_num)\n");
    fprintf (file, "    {\n");
    fprintf (file, "        fprintf (stderr, \", %%d FAILED\", test_purp_failed_num);\n");
    fprintf (file, "    }\n");
    fprintf (file, "    fprintf (stderr, \"\\n\");\n");
    fprintf (file, "\n");
    fprintf (file, "    remove (add_filename);\n");
    fprintf (file, "}\n");

    fclose (file);
} 

/*
 * Generate header tests
 * 
 * Parameters:
 *   char* store_path - root directory for generated tests
 *   char* func_list_file - path to the file with functions list
 * Returns:
 *   none
 */
static void
gen_header_tests (char* gtkvts_root, char *store_path,
                  char* scenario_file, char* func_list_file,
                  char* copyright_notice_file_name)
{
    char* copyright_notice;
    char* tet_hook = NULL;
    char* current_test_file_name = NULL;
    char* line = NULL;

    int func_count = 0;
    FILE* p_func_list_file = open_file (func_list_file, "r", NULL);

    line = alloc_mem_for_string (line, 1024);
    if (!is_directory_exists (store_path))
    {
        create_dir (store_path);
    }

    while (!feof (p_func_list_file))
    {
        int line_length;

        /* the next string is to avoid errors when reading an empty string */
        *line = '\0';
        fgets (line, 1024, p_func_list_file);
        
        line_length = strlen (line);


        if (line_length < 0)
        {
            if (tet_hook)
            {
                append_tet_hook (current_test_file_name, tet_hook);
                free (tet_hook);
            }
            break;
        }

        if (!line_length || *line == comment_char)
        {
            continue;
        }
        
        if (*line == group_name_char)
        {
            char* old_dir1;
            char* old_dir2;
            char* group_name = NULL;
            char* new_scen_entry = NULL;
            char* new_scen_entry1 = NULL;
            char* new_scen_entry2 = NULL;
            char* new_dir = NULL;
            char* new_dir1 = NULL;
            char* current_test_file_name1 = NULL;
            
            if (tet_hook)
            {
                append_tet_hook (current_test_file_name, tet_hook);
                free (tet_hook);
            }

            old_dir1 = change_dir (store_path);
            new_dir1 = alloc_mem_for_string (new_dir1, strlen (line));
            memmove (new_dir1, line + 1, strlen (line + 1) - 1);
            *(new_dir1 + strlen (line + 1) - 1) = '\0';
            new_dir = replace_char_in_string (new_dir1, ' ', '_');

            create_dir (new_dir);
            old_dir2 = change_dir (new_dir);

            /* copy a group name without '\n' character */
            group_name = alloc_mem_for_string 
                (group_name, strlen (line + 1) - 1);
            memmove (group_name, line + 1, strlen (line + 1) - 1);

            new_scen_entry1 = alloc_mem_for_string (new_scen_entry1, 
                                                    strlen (store_path));
            new_scen_entry1 = str_append (new_scen_entry1, store_path);
            new_scen_entry2 = replace_substr_in_string (new_scen_entry1, 
                                                       gtkvts_root, "");
            new_scen_entry2 = str_append (new_scen_entry2, "/");
            new_scen_entry2 = str_append (new_scen_entry2, group_name);
            new_scen_entry2 = str_append (new_scen_entry2, "/");
            new_scen_entry2 = str_append (new_scen_entry2, group_name);
            new_scen_entry = replace_char_in_string (new_scen_entry2, ' ', '_');

            current_test_file_name = 
                alloc_mem_for_string (current_test_file_name, 
                                      strlen (store_path));
            current_test_file_name = 
                strcpy (current_test_file_name, store_path);
            current_test_file_name = str_append (current_test_file_name, "/");
            current_test_file_name = str_append (current_test_file_name, 
                                                 group_name);
            current_test_file_name = str_append (current_test_file_name, "/");
            current_test_file_name = str_append (current_test_file_name, 
                                                 group_name);
            current_test_file_name = str_append (current_test_file_name, ".c");
            current_test_file_name1 = replace_char_in_string 
                (current_test_file_name, ' ', '_');
            current_test_file_name = alloc_mem_for_string 
                (current_test_file_name, strlen (current_test_file_name1));
            strcpy (current_test_file_name, current_test_file_name1);

            copyright_notice = get_copyright_notice 
                (copyright_notice_file_name);
            append_copyright_notice (current_test_file_name, copyright_notice);

            gen_includes_block (current_test_file_name);
            gen_start_decls (current_test_file_name, group_name);
            gen_runtest_func (current_test_file_name);
            gen_start_func (current_test_file_name, group_name);
            gen_finish_func (current_test_file_name);
            gen_test_passed_func (current_test_file_name);
            gen_test_failed_func (current_test_file_name);
            tet_hook = gen_tet_hook_common_parts (tet_hook);

            gen_header_test_makefile (gtkvts_root, new_dir, group_name);

            add_item_to_scenario_file (scenario_file, new_scen_entry);
            func_count = 0;
            
            free (old_dir1);
            free (old_dir2);
            free (new_dir);
            free (new_dir1);
            free (group_name);
            free (new_scen_entry);
            free (new_scen_entry1);
            free (new_scen_entry2);
            free (copyright_notice);
            free (current_test_file_name1);
        }

        if (*line == function_name_char)
        {
            char* func_count_str = NULL;
            char* test_case_name = NULL;
            char* func_decl = NULL;

            func_count_str = alloc_mem_for_string 
                (func_count_str, strlen (test_case_name_prefix) + 16);

            sprintf (func_count_str, "%d", ++func_count);

            test_case_name = alloc_mem_for_string
                (test_case_name, strlen (test_case_name_prefix));
            strcpy (test_case_name, test_case_name_prefix);
            test_case_name = str_append (test_case_name, func_count_str);

            /* allocate strlen (line + 1) - 1 because we'll not memmove '\n' */
            func_decl = alloc_mem_for_string (func_decl, strlen (line + 1) - 1);
            /* don't include \n*/
            memmove (func_decl, line + 1, strlen (line + 1) - 1);
            tet_hook = gen_test_purpose (current_test_file_name, test_case_name, 
                                      func_decl, tet_hook, tet_hook_tail_length);
            free (func_count_str);
            free (test_case_name);
            free (func_decl);
        }
    }

    if (tet_hook)
    {
        append_tet_hook (current_test_file_name, tet_hook);
        free (tet_hook);
    }

    fclose (p_func_list_file);
    free (current_test_file_name);
    free (line);

}

/*
 * Generate a makefile for a test group
 */
static void
gen_header_test_makefile (char* gtkvts_root, char* dir, char* current_test_file_name)
{
    FILE* mf;
    char* str = NULL;
    char* makefile_name = "makefile";
    char* test_file_name = NULL;

    test_file_name = replace_char_in_string (current_test_file_name, ' ', '_');

    str = alloc_mem_for_string (str, 1024);
    str = getcwd (str, 1024);

    mf = open_file (makefile_name, "w+", NULL);
    
    fprintf (mf, "TET_INC_DIR    = $(TET_ROOT)/inc/tet3\n");    
    fprintf (mf, "TET_LIB_DIR    = $(TET_ROOT)/lib/tet3\n");    
    fprintf (mf, "GTKVTS_ROOT    = %s\n", gtkvts_root);    
    fprintf (mf, "GTKVTS_LIB_DIR = $(GTKVTS_ROOT)/lib\n");    
    fprintf (mf, "\n");    
    fprintf (mf, "LIBS = $(TET_LIB_DIR)/tcm.o $(TET_LIB_DIR)/libapi.a $(GTKVTS_LIB_DIR)/libthr.a $(GTKVTS_LIB_DIR)/librobot.a $(GTKVTS_LIB_DIR)/libtestgen.a $(GTKVTS_LIB_DIR)/libfile.a $(GTKVTS_LIB_DIR)/libstr.a $(GTKVTS_LIB_DIR)/libmem.a\n");    
    fprintf (mf, "\n");    
    fprintf (mf, "%s:\t%s.c $(TET_INC_DIR)/tet_api.h\n", 
             test_file_name, test_file_name);    
        
    fprintf (mf, "\t$(CC) ");
    {
        char* cflags = getenv ("CFLAGS");
        if (cflags)
        {
            fprintf (mf, "%s", cflags);
        }
    }
    fprintf (mf, " `pkg-config --cflags gtk+-2.0` -I$(GTKVTS_ROOT)/src/tools/testgen -I$(GTKVTS_ROOT)/src/libs -I$(TET_INC_DIR) -o $@ $< `pkg-config --libs gtk+-2.0` $(LIBS)\n");    

    fprintf (mf, "\tchmod a+x %s\n", test_file_name);    
    fprintf (mf, "\n");    
    fprintf (mf, "additional:\t additional.c\n");    
    fprintf (mf, "\t$(CC) `pkg-config --cflags gtk+-2.0` $< `pkg-config --libs gtk+-2.0`\n");    
    fprintf (mf, "\n");    
    fprintf (mf, "clean:\n");    
    fprintf (mf, "\trm -rf additional.c errors_test_purpose*\n");    
    fprintf (mf, "\n");    

    free (test_file_name);
    free (str);
    fclose (mf);
}

/*
 * Generate #include's block
 */
static void
gen_includes_block (char *file_path) 
{
    FILE* file = open_file (file_path, "a", "c");
    
    fprintf (file, "#include <stdio.h>\n");
    fprintf (file, "#include <stdlib.h>\n");
    fprintf (file, "#include <string.h>\n");
    fprintf (file, "#include <glib.h>\n");
    fprintf (file, "#include <unistd.h>\n");
    fprintf (file, "#include <tet_api.h>\n");
    fprintf (file, "#include <libthr.h>\n");
    fprintf (file, "#include <gtk/gtk.h>\n");
    fprintf (file, "#include \"hdr_tests_addit_files_gen.h\"\n");

    fclose (file);
}

    
/*
 * Generate run_test() function which is used by all the headers test cases
 */
static void
gen_runtest_func (char *file_path)
{
    FILE* file = open_file (file_path, "a", "c");

    fprintf (file, "\n");
    fprintf (file, "unsigned\n");
    fprintf (file, "run_test (char* test_name, char* func_ret_type, \n");
    fprintf (file, "          char* func_name, char* func_params)\n");
    fprintf (file, "{\n");
    fprintf (file, "    char* s;\n");
    fprintf (file, "    char* log_path;\n");
    fprintf (file, "    char* errors_file_path;\n");
    fprintf (file, "\n");
    fprintf (file, "    test_purp_num++;\n");
    fprintf (file, "    fprintf (stderr, \".\");\n");
    fprintf (file, "    cwd = (char*) calloc (sizeof (char), 1024);\n");
    fprintf (file, "    cwd = getcwd (cwd, 1024 * sizeof (char));\n");
    fprintf (file, "    tet_root = (char*) getenv (\"TET_ROOT\");\n");
    fprintf (file, "    log_path = (char*) calloc (sizeof (char), strlen (tet_root) + strlen (log_subpath) + sizeof (char));\n");
    fprintf (file, "    strcpy (log_path, tet_root);\n");
    fprintf (file, "    strcat (log_path, log_subpath);\n");
    fprintf (file, "    errors_file_path = (char*) calloc (sizeof (char), strlen (make_output_file_prefix) + strlen (test_name) + sizeof (char));\n");
    fprintf (file, "    strcpy (errors_file_path, make_output_file_prefix);\n");
    fprintf (file, "    strcat (errors_file_path, test_name);\n");
    fprintf (file, "    s = (char*) calloc (sizeof (char), strlen (tet_root) + strlen (test_subdir) + sizeof (char));\n");
    fprintf (file, "    strcpy (s, tet_root);\n");
    fprintf (file, "    strcat (s, test_subdir);\n");
    fprintf (file, "    chdir (test_subdir);\n");
    fprintf (file, "\n");
    fprintf (file, "    if (!was_cleaned)\n");
    fprintf (file, "    {\n");
    fprintf (file, "        exec_data[0] = \"/bin/bash\";\n");
    fprintf (file, "        exec_data[1] = \"-c\";\n");
    fprintf (file, "        exec_data[2] = (char*) calloc (sizeof (char), 256);\n");
    fprintf (file, "        sprintf(exec_data[2], \"$MAKE clean >> %%s\", log_path);\n");
    fprintf (file, "        exec_data[3] = 0;\n");
    fprintf (file, "        exec_data[4] = 0;\n");
    fprintf (file, "        exec_data[5] = 0;\n");
    fprintf (file, "        exec_data[6] = 0;\n");
    fprintf (file, "        exec_data[7] = test_subdir;\n");
    fprintf (file, "        exec_data[8] = 0;\n");
    fprintf (file, "\n");
    fprintf (file, "        if (exec_program (\"/bin/bash\", exec_data))\n");
    fprintf (file, "        {\n");
    fprintf (file, "            test_failed();\n");
    fprintf (file, "            free (s);\n");
    fprintf (file, "            free (cwd);\n");
    fprintf (file, "            free (log_path);\n");
    fprintf (file, "            free (errors_file_path);\n");
    fprintf (file, "            free (exec_data[2]);\n");
    fprintf (file, "            return FALSE;\n");
    fprintf (file, "        }\n");
    fprintf (file, "\n");
    fprintf (file, "        free (exec_data[2]);\n");
    fprintf (file, "        was_cleaned = TRUE;\n");
    fprintf (file, "    }\n");
    fprintf (file, "\n");
    fprintf (file, "    s = (char*) realloc (s, sizeof (char) * (strlen (tet_root)) + \n");
    fprintf (file, "                         strlen(hdr_tests_add_files_gen_subpath) + 1);\n");
    fprintf (file, "    strcpy (s + strlen (tet_root), hdr_tests_add_files_gen_subpath);\n");
    fprintf (file, "\n");
    fprintf (file, "    exec_data[0] = s;\n");
    fprintf (file, "    exec_data[1] = (char*) calloc (sizeof (char), 256);\n");
    fprintf (file, "    sprintf(exec_data[1], \"%%d\", GEN_INC_BLK | GEN_MAIN_FUNC); \n");
    fprintf (file, "    exec_data[2] = add_filename;\n");
    fprintf (file, "    exec_data[3] = \"<glib.h>,<gtk/gtk.h>\";\n");
    fprintf (file, "    exec_data[4] = func_ret_type;\n");
    fprintf (file, "    exec_data[5] = func_name;\n");
    fprintf (file, "    exec_data[6] = func_params;\n");
    fprintf (file, "    exec_data[7] = test_subdir;\n");
    fprintf (file, "    exec_data[8] = 0;\n");
    fprintf (file, "\n");
    fprintf (file, "    if (exec_program (s, exec_data))\n");
    fprintf (file, "    {\n");
    fprintf (file, "        test_failed(); \n");
    fprintf (file, "        free (s);\n");
    fprintf (file, "        free (cwd);\n");
    fprintf (file, "        free (log_path);\n");
    fprintf (file, "        free (errors_file_path);\n");
    fprintf (file, "        free (exec_data[1]);\n");
    fprintf (file, "        return FALSE; \n");
    fprintf (file, "    }\n");
    fprintf (file, "\n");
    fprintf (file, "    free (exec_data[1]);\n");
    fprintf (file, "    s = (char*) realloc\n");
    fprintf (file, "        (s, sizeof (char) * (strlen (make_run_test_command) + strlen (errors_file_path)+ 4 + strlen (log_path) + 1));\n");
    fprintf (file, "    strcpy (s, make_run_test_command);\n");
    fprintf (file, "    strcat (s, errors_file_path);\n");
    fprintf (file, "    strcat (s, \" 1>>\");\n");
    fprintf (file, "    strcat (s, log_path);\n");
    fprintf (file, "\n");
    fprintf (file, "    exec_data[0] = \"/bin/bash\";\n");
    fprintf (file, "    exec_data[1] = \"-c\";\n");
    fprintf (file, "    exec_data[2] = s;\n");
    fprintf (file, "    exec_data[3] = 0;\n");
    fprintf (file, "    exec_data[4] = 0;\n");
    fprintf (file, "    exec_data[5] = 0;\n");
    fprintf (file, "    exec_data[6] = 0;\n");
    fprintf (file, "    exec_data[7] = test_subdir;\n");
    fprintf (file, "    exec_data[8] = 0;\n");
    fprintf (file, "\n");
    fprintf (file, "    if (exec_program (\"/bin/bash\", exec_data)) \n");
    fprintf (file, "    {\n");
    fprintf (file, "        test_failed ();\n");
    fprintf (file, "        free (s);\n");
    fprintf (file, "        free (cwd);\n");
    fprintf (file, "        free (log_path);\n");
    fprintf (file, "        free (errors_file_path);\n");
    fprintf (file, "        return FALSE;\n");
    fprintf (file, "    } else {\n");
    fprintf (file, "        test_passed (errors_file_path);\n");
    fprintf (file, "    }\n");
    fprintf (file, "    chdir (cwd);\n");
    fprintf (file, "    free (s);\n");
    fprintf (file, "    free (cwd);\n");
    fprintf (file, "    free (log_path);\n");
    fprintf (file, "    free (errors_file_path);\n");
    fprintf (file, "    return TRUE;\n");
    fprintf (file, "\n");
    fprintf (file, "}\n");

    fclose (file);
}
    


/*
 * Generate start declarations:
 *
 * char* exec_data[9];
 * char* add_filename = "additional.c";
 * char* test_subdir = <test_subdir>;
 * char* make_run_test_command = "$MAKE additional 2> errors_";
 * unsigned was_cleaned = FALSE;
 * char* tet_root;
 * char* cwd;
 */
static void
gen_start_decls (char *file_path, char *group_name) 
{
    FILE* file = open_file (file_path, "a", "c");
    char* dir = get_dir (file_path);

    fprintf (file, "\n");
    fprintf (file, "void test_passed (char *errors_file_path);\n");
    fprintf (file, "void test_failed (void);\n");
    fprintf (file, " unsigned run_test (char* test_name, char* func_ret_type, char* func_name, char* func_params);\n");
    fprintf (file, "void start_func (void);\n");
    fprintf (file, "void finish_func (void);\n");
    fprintf (file, "\n");
    fprintf (file, "char* exec_data[9];\n");
    fprintf (file, "char* add_filename = \"additional.c\";\n");
    fprintf (file, "char* group = \"%s\";\n", group_name);
    fprintf (file, "char* test_subdir = \"%s\";\n", dir);
    fprintf (file, "char* hdr_tests_add_files_gen_subpath = \"/gtkvts/bin/hdr_tests_addit_files_gen\";\n");
    fprintf 
        (file, 
         "char* make_run_test_command = \"$MAKE additional 2>> \";\n");
    fprintf (file, "char* make_output_file_prefix = \"errors_\";\n");
    fprintf (file, "unsigned was_cleaned = FALSE;\n");
    fprintf (file, "char* log_subpath = \"/gtkvts/tests/runtests.log\";\n");
    fprintf (file, "int test_purp_num = 0;\n");
    fprintf (file, "int test_purp_failed_num = 0;\n");
    fprintf (file, "char* tet_root;\n");
    fprintf (file, "char* cwd;\n");

    free (dir);
    fclose (file);
}

/*
 * Generate start_func() for a test file
 */
static void 
gen_start_func (char *file_path, char *group_name)
{
    FILE* file = open_file (file_path, "a", "c");

    fprintf (file, "\n");
    fprintf (file, "void start_func (void)\n");
    fprintf (file, "{\n");
    fprintf (file, 
             "    fprintf (stderr, \"Hdr tests for %s: \");\n", group_name);
    fprintf (file, "}\n");

    fclose (file);
}
   
static void
gen_test_failed_func (char *file_path)
{
    FILE* file = open_file (file_path, "a", "c");

    fprintf (file, "\n");
    fprintf (file, "void test_failed (void)\n");
    fprintf (file, "{\n");
    fprintf (file, "    char* new_add_filename = (char*) calloc (sizeof (char), strlen (add_filename) + 5);\n");
    fprintf (file, "    strcpy (new_add_filename, add_filename);\n");
    fprintf (file, "    sprintf (new_add_filename + strlen (new_add_filename), \"%%d\", test_purp_num);\n");
    fprintf (file, "\n");
    fprintf (file, "    test_purp_failed_num++;\n");
    fprintf (file, "\n");
    fprintf (file, "    rename (add_filename, new_add_filename);\n");
    fprintf (file, "    free (new_add_filename);\n");
    fprintf (file, "}\n");
 
    fclose (file);
}    

static void
gen_test_passed_func (char *file_path)
{
    FILE* file = open_file (file_path, "a", "c");

    fprintf (file, "\n");
    fprintf (file, "void test_passed (char *errors_file_path)\n");
    fprintf (file, "{\n");
    fprintf (file, "    remove (errors_file_path);\n");
    fprintf (file, "}\n");
 
    fclose (file);
}    

/*
 * Generate a test case 
 */
static char*
gen_test_purpose (char* file_path, char* test_case_name, char* func_decl, 
               char* tet_hook, int tet_hook_tail_length)
{
    FILE* file;
    char* func_ret_type_and_name;
    char* pchar;
    char* func_params;
    char* func_name_pointer;
    char* str = NULL;
    char* tet_hook_new_record = NULL;
    char* func_ret_type = NULL;
    char* func_name = NULL;
    char* func_decl_str = NULL;

    int old_tail_pos;
    int new_tail_pos;

    file = open_file (file_path, "a", "c");

    /* get the function return type and name */
    func_decl_str = alloc_mem_for_string (func_decl_str, strlen (func_decl));

    memmove (func_decl_str, func_decl, strlen (func_decl));

    /* check if pointer to a function but not a function name 
       is passed to test*/
    func_name_pointer = strstr (func_decl_str, "(*");
    func_ret_type_and_name = strtok (func_decl_str, "(");
    if (func_name_pointer) {
        char* second_part;
        int first_part_length = func_name_pointer - func_decl_str;
        second_part = strtok (NULL, "(");
        *(func_ret_type_and_name + first_part_length) = '(';
    }

    func_ret_type_and_name = trim (func_ret_type_and_name);
        
    pchar = strrchr (func_ret_type_and_name, ' ');

    if (!pchar)
    {
        error_with_exit_code (BAD_FUNC_DECL_FORMAT, get_app_name(), 
                              "%s: bad function declaration in functions list",
                              func_decl);
    }

    pchar = trim (pchar);
    func_name = alloc_mem_for_string (func_name, strlen (pchar));
    memcpy (func_name, pchar, strlen (pchar));
    
    *pchar = '\0';
    
    func_ret_type_and_name = trim (func_ret_type_and_name);
    func_ret_type = alloc_mem_for_string(func_ret_type, 
                                         strlen(func_ret_type_and_name));
    memcpy (func_ret_type, func_ret_type_and_name, 
            strlen (func_ret_type_and_name));

    if (!func_name || !func_ret_type)
    {
        error_with_exit_code (BAD_FUNC_DECL_FORMAT, get_app_name(),  
                              "%s: bad function declaration in functions list",
                              func_decl);
    }

    /* get the function parameters */
    memmove (func_decl_str, func_decl, strlen (func_decl));
    pchar = strchr (func_decl_str, '(');
    if (func_name_pointer)
    {
        pchar = strchr (pchar + 1, '(');
    }
    func_params = strtok (pchar + 1, ");"); /* should be params decls plus );*/

    /* the test case itself generation */
    fprintf (file, "\n");
    fprintf (file, "void %s (void)\n", test_case_name);
    fprintf (file, "{\n");
    fprintf (file, "    char* test_name = \"%s\";\n", test_case_name);
    fprintf (file, "    char* func_ret_type = \"%s\";\n", func_ret_type);
    fprintf (file, "    char* func_name = \"%s\";\n", func_name);

    if (!func_params || !strlen (func_params))
    {
        fprintf (file, "    char* func_params = \"void\";\n");
    } else {
        fprintf (file, "    char* func_params = \"%s\";\n", func_params);
    }
    fprintf (file, "\n");
    fprintf (file, "    if (run_test (test_name, func_ret_type, func_name, func_params) == TRUE)\n");
    fprintf (file, "    {\n");
    fprintf (file, "        tet_result (TET_PASS);\n");
    fprintf (file, "    } else {\n");
    fprintf (file, "        tet_result (TET_FAIL);\n");
    fprintf (file, "    }\n");
    fprintf (file, "}\n");

    /* adding the test case to the TET list */
    if (!str)
    {
        str = alloc_mem_for_string (str, 2);
    }
    sprintf (str, "%d", 1);

    tet_hook_new_record = alloc_mem_for_string (tet_hook_new_record, 
                                                strlen ("    {"));
    tet_hook_new_record = str_append (tet_hook_new_record, "    {");
    tet_hook_new_record = str_append (tet_hook_new_record, test_case_name);
    tet_hook_new_record = str_append (tet_hook_new_record, ", ");
    tet_hook_new_record = str_append (tet_hook_new_record, str);
    tet_hook_new_record = str_append (tet_hook_new_record, "},\n");

    old_tail_pos = strlen (tet_hook) - tet_hook_tail_length;
    new_tail_pos = old_tail_pos + strlen (tet_hook_new_record);

    tet_hook = alloc_mem_for_string        
        (tet_hook, strlen (tet_hook) + strlen (tet_hook_new_record) + tet_hook_tail_length);

    memmove (tet_hook + new_tail_pos, tet_hook + old_tail_pos, 
             tet_hook_tail_length);
    memmove (tet_hook + old_tail_pos, tet_hook_new_record, 
             strlen (tet_hook_new_record));
    *(tet_hook + new_tail_pos + tet_hook_tail_length) = '\0';

    free (func_decl_str);
    free (func_name);
    free (func_ret_type);
    free (tet_hook_new_record);
    free (str);

    fclose (file);

    return tet_hook;
}

/*
 * Generate TET hook common part:
 *
 * void (*tet_startup) (void) = NULL;
 * void (*tet_cleanup) (void) = NULL;
 * 
 * struct tet_tetlist tet_testlist[] = 
 * {
 *     {NULL, 0}
 * }
 *
 * The function stores generated data into *tet_hook.
 * Returns *tet_hook.
 */
static char*
gen_tet_hook_common_parts (char *tet_hook)
{
    char* hook_start = NULL;
    char* hook_tail = NULL;

    hook_tail = alloc_mem_for_string (hook_tail, 1024);

    hook_start = alloc_mem_for_string (hook_start, 1);
    hook_start = str_append (hook_start, "\n");
    hook_start = str_append (hook_start, "void (*tet_startup)(void) = start_func;\n");
    hook_start = str_append (hook_start, "void (*tet_cleanup)(void) = finish_func;\n");
    hook_start = str_append (hook_start, "\n");
    hook_start = str_append (hook_start, "struct tet_testlist tet_testlist[] = \n{\n");

    sprintf (hook_tail, "    {NULL, 0}\n};\n");

    hook_start = str_append (hook_start, hook_tail);

    tet_hook = hook_start;
    tet_hook_tail_length = strlen (hook_tail);

    free (hook_tail);

    return tet_hook;
}

/*
 * Get this application name
 */
static char* 
get_app_name ()
{
    return "hdr_tests_generator";
}
