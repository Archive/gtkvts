/*
 * This program generate tests.
 *
 * The structure of argv argument is:
 * 0th string - not used
 * 1st string - path to the directory where gtkvts suite resides
 * 2nd string - path where directories with input files resides
 * 3rd string - path where to put catalogs with generated tests
 * 4th string - path to scenario file
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <common.h>
#include "libmem.h"
#include "libstr.h"
#include "libfile.h"

#define INPUT_EXT          ".inp"         /* input file extantion */
#define COMMON_TEST_TPL    "_test.tpl"   /* common test template file name */
#define COMMON_PURPOSE_TPL "_purpose.tpl"/* common purpose template file name */

#define COMMON_TAGS_NUM 4
#define GROUP_NAME_POS    0
#define OBJECT_NAME_POS   1
#define TEST_PURPOSES_POS 2
#define TET_HOOKS_POS     3

static char* common_tags[COMMON_TAGS_NUM] = { "<%group_name%>",
                                              "<%object_name%>",
                                              "<%test_purposes%>",
                                              "<%tet_hooks%>"
                                             };
#define MAX_PARAMS_NUM 64
#define TAGS_NUM 5
#define COMMENT_TAG_POS  0
#define DEFINE_TAG_POS   1
#define CODE_TAG_POS     2
#define PURPNUM_TAG_POS  3
#define FUNCTION_TAG_POS 4
#define INDENT 4 /* indent for <%define%> and <%code%> */

#define print_warning(str, ...) {if(debug) {printf(str, __VA_ARGS__);}}

int debug;
char* bug_id;
char* ignore_list = NULL;

static char* tags[TAGS_NUM] = { "<%comment%>",
                                "<%define%>",
                                "<%code%>",
                                "<%purpose_number%>",
                                "<%function%>",
                              };

char* func_tests_scenario_file_name = "func_scen";

static void  
run_generator (const char* gtkvts_root, const char* input_dir, 
               const char* output_dir, const char* scen_dir);
static char*  
gen_tests_group (const char* gtkvts_root, const char* output_dir, 
                 const char* group_path, const char* group_nme, 
                 const char* scen_file);
static char*  
gen_test (const char* test_tpl, const char* purpose_tpl, 
          const char* input_path, const char* group_nme, const char* test_nme);
static char*  
gen_all_test_purposes (const char* fobject_tpl, const char* purpose_tpl, 
                       int* purposes_number, const char* test_nme);
static char**  
do_substitutions (int tags_num, char* tags[], int params_num, char* params[]);
static char*  
gen_tet_hooks (int number);
static void   
add_test_in_scen (const char* gtkvts_root, const char* scen_file, 
                  const char* output_dir, const char* ftest_nme);
static void 
gen_makefile (const char* gtkvts_root, const char* dir_path, 
              const char* ftest_nme);
static char*  
check_function_tag (const char* test_nme, int str_number, char* fTag);
static char* 
fgets_and_skip_comments (char* str, int num, FILE* stream, int* str_counter);

int 
main (int argc, char* argv[]) 
{
    if (!strcmp (argv[1], "-debug"))
    {
        if (!strcmp (argv[2], "-ignore"))
        {
            if (argc != 8)
            {
                printf ("\n Incorrect parameters number \n");
                return 0;
            }
            debug = 1;
            ignore_list = alloc_mem_for_string (ignore_list, 
                                              strlen(argv[3])+15);
            ignore_list = "&Ignore_List: ";
            ignore_list = str_sum (ignore_list,argv[3]);
            ignore_list = str_sum (ignore_list,";");
            run_generator (argv[4], argv[5], argv[6], argv[7]);
        } else {
            if (argc != 6)
            {
                printf ("\n Incorrect parameters number \n");
                return 0;
            }
            debug = 1;
            run_generator (argv[2], argv[3], argv[4], argv[5]);
        }
    } else {
        if (!strcmp (argv[1], "-ignore"))
        {
            if (argc != 7)
            {
                printf ("\n Incorrect parameters number \n");
                return 0;
            }
            debug = 0;
            ignore_list = alloc_mem_for_string (ignore_list, 
                                              strlen(argv[2])+15);
            ignore_list = "&Ignore_List: ";
            ignore_list = str_sum (ignore_list,argv[2]);
            ignore_list = str_sum (ignore_list,";");
            run_generator (argv[3], argv[4], argv[5], argv[6]);
        } else {
            if (argc != 5)
            {
                printf ("\n Incorrect parameters number \n");
                return 0;
            }
            debug = 0;
            run_generator (argv[1], argv[2], argv[3], argv[4]);
        }
    }
    return 0;
}

/*
 * Run gen_tests_group() for every tests group directory 
 * from inputdir_nme directory
 *
 */
static void 
run_generator (const char* gtkvts_root_in, const char* input_dir,
               const char* output_dir_in, const char* scen_dir_in)
{
    DIR*  pDir;
    FILE* ftet_scen = NULL;
    struct dirent* ep;
    char* output_dir;
    char* output_dir1;
    char* input_path = NULL;
    char* input_path1 = NULL;
    char* input_path2 = NULL;
    char* ftet_scen_path = NULL;
    char* ftet_scen_path1 = NULL;
    unsigned ftet_scen_exists = 0;
    char* pSave = NULL;
    char* scenario_file = NULL;
    char* path1 = NULL;
    char* gtkvts_root = NULL;
    char* gtkvts_root1 = NULL;
    char* scen_dir = NULL;
    char* cur_work_dir = (char*) calloc (1, 1024);
     
    cur_work_dir = getcwd (cur_work_dir, 1024);
    path1 = concat_paths (cur_work_dir, (char*) scen_dir_in);
    scen_dir = (char*) shorten_path (path1);

    if (!is_directory_exists ((char*) scen_dir))
    {
        create_dir ((char*) scen_dir);
    }

    scenario_file = alloc_mem_for_string (scenario_file, 
                                          strlen (scen_dir));
    scenario_file = str_append (scenario_file, scen_dir);
    scenario_file = str_append (scenario_file, "/");
    scenario_file = str_append (scenario_file, func_tests_scenario_file_name);

    output_dir1 = concat_paths (cur_work_dir, (char*) output_dir_in);
    output_dir = shorten_path (output_dir1);

    input_path1 = alloc_mem_for_string (input_path1, strlen (input_dir));
    input_path1 = str_append (input_path1, input_dir);
    input_path2 = concat_paths (cur_work_dir, input_path1);
    input_path = shorten_path (input_path2);

    if (input_path[strlen (input_path) - 1] != '/')
    {
        input_path = str_append (input_path, "/");
    }

    gtkvts_root1 = concat_paths (cur_work_dir, (char*) gtkvts_root_in);
    gtkvts_root = shorten_path (gtkvts_root1);

    ftet_scen_path1 = alloc_mem_for_string (ftet_scen_path1, 
                                           strlen (gtkvts_root));
    ftet_scen_path1 = str_append (ftet_scen_path1, gtkvts_root);
    ftet_scen_path = concat_paths (ftet_scen_path1, "/tet_scen");

    ftet_scen_exists = is_file_exists (ftet_scen_path);
    ftet_scen = open_file (ftet_scen_path, "a+", NULL);
    if (!ftet_scen_exists)
    {
        fprintf (ftet_scen, "all\n");
    }
    fprintf (ftet_scen, "\t:include:/scenarios/func_scen\n");
    fclose (ftet_scen);

    /* read directories names from input_dir directory */
    pDir = opendir (input_path);
    if (pDir == NULL)
    {
        error_with_exit_code (EXIT_CODE_CANT_OPEN_DIR,
                              "%s: can't open directory %s", "test generator", 
                              input_path);
    } 
    
    ep = readdir (pDir);
    while (ep != NULL)
    {
        char* group_path = NULL;
        group_path = str_sum (input_path, ep->d_name);

        if (is_directory_exists (group_path) && strcmp (ep->d_name, ".") 
            && strcmp (ep->d_name, "..") && strcmp (ep->d_name, "CVS"))
        {
            pSave = group_path;
            if (group_path[strlen (group_path) - 1] != '/')  
            {
                group_path = str_append (group_path, "/");
            }
            pSave = NULL;
            gen_tests_group (gtkvts_root, output_dir, 
                             group_path, ep->d_name, 
                             scenario_file);
        }
        ep = readdir (pDir);
        free (group_path);
    }

    free (cur_work_dir);
    free (scenario_file);
    free (output_dir);
    free (output_dir1);
    free (input_path);
    free (input_path1);
    free (input_path2);
    free (gtkvts_root);
    free (gtkvts_root1);
    free (ftet_scen_path);
    free (ftet_scen_path1);
    free (pDir);
    free (path1);
    free (scen_dir);
}

/*
 * Read input files from input_dir, generates tests and makefiles, 
 * create directory where stores its (test and makefile), 
 * add corresponded record in scen_file.
 *
 */
static char* 
gen_tests_group (const char* gtkvts_root, const char* output_dir, 
                 const char* group_path, const char* group_nme, 
                 const char* scen_file)
{
    DIR*  pDir;
    struct dirent* ep;
    FILE* pFile;

    char* pSave = NULL;
    char* tpl_path = NULL;
    char* output_dir_path = NULL;

    char* test = NULL;
    char* test_tpl = NULL;
    char* purpose_tpl = NULL;

    /* common test template obtains */
    tpl_path = str_sum (group_path, COMMON_TEST_TPL);

    if (is_file_exists (tpl_path))
    {
        pFile = open_file (tpl_path, "r", NULL);
    } else {
        char* tpl_path1 = NULL;
        char* tpl_path2 = NULL;

        free (tpl_path);
        tpl_path1 = alloc_mem_for_string (tpl_path1, strlen (gtkvts_root)); 
        tpl_path1 = str_append (tpl_path1, gtkvts_root);
        tpl_path2 = concat_paths (tpl_path1, 
                                  "src/tests/functions/_Templates/");
        tpl_path = concat_paths (tpl_path2, COMMON_TEST_TPL);
        pFile = open_file (tpl_path, "r", NULL);

        free (tpl_path1);
        free (tpl_path2);
    }
    free (tpl_path);
    test_tpl = read_file_to_string (pFile);
    fclose (pFile);
    
    /* common purpose template obtains */
    tpl_path = concat_paths ((char*) group_path, COMMON_PURPOSE_TPL);
    if (is_file_exists (tpl_path))
    {
        pFile = open_file (tpl_path, "r", NULL);
    } else {
        free (tpl_path);
        tpl_path = str_sum (gtkvts_root, NULL);
        tpl_path = str_append (tpl_path,"/src/tests/functions/_Templates/");
        tpl_path = str_append (tpl_path,COMMON_PURPOSE_TPL);
        pFile = open_file (tpl_path, "r", NULL);
    }
    purpose_tpl = read_file_to_string (pFile);
    free (tpl_path);
    fclose (pFile);

    /* form output dir path */
    output_dir_path = alloc_mem_for_string (output_dir_path, 
                                            strlen (output_dir));
    output_dir_path = str_append (output_dir_path, output_dir);
    if (output_dir_path[strlen (output_dir_path) - 1] != '/')
    {
        output_dir_path = str_append (output_dir_path, "/");
    }

    if (!is_directory_exists (output_dir_path))
    {
        create_dir (output_dir_path);        
    }

    /* read file names from group_path directory */
    pDir = opendir (group_path);
    if (pDir == NULL)
    {
        error_with_exit_code(EXIT_CODE_CANT_OPEN_DIR,
                             "%s: can't open directory %s", "test generator", 
                             group_path);
    } 

    ep = readdir (pDir);
    while (ep != NULL)
    {
        char* ftest_src = NULL;
        char* ftest_nme = NULL;
        char* input_path = NULL;
        char* output_path = NULL;

        ftest_nme = get_substr (ep->d_name, 0, 
                                strstr (ep->d_name, INPUT_EXT) - (ep->d_name + 1));
        if ((strcmp (ep->d_name + strlen (ep->d_name) - strlen (INPUT_EXT), 
                     INPUT_EXT)) 
            || (ftest_nme == NULL)) 
        {
            ep = readdir (pDir);
            if (ftest_nme)
            {
                free (ftest_nme);
            }
            continue;
        }
        /* generate test string */
        input_path = str_sum (group_path, ep->d_name);
        test = gen_test (test_tpl, purpose_tpl, input_path, group_nme, 
                         ftest_nme);
        if (test == NULL)
        {
            ep = readdir (pDir);
            print_warning ("warning: test generator can't generate test for %s\n", input_path);
            free (input_path);
            free (ftest_nme);
            continue;
        }

        /* create in output directory subdirectory for results residing */
        output_path = str_sum (output_dir_path, ftest_nme);
        pSave = output_path;
        if (output_path[strlen (output_path) - 1] != '/')
        {
            output_path = str_sum (output_path, "/");
            free (pSave);
        }
        pSave = NULL;
        if (!is_directory_exists (output_path))
        {
            create_dir (output_path);        
        }

        /* store test in corresponded file */
        gen_makefile (gtkvts_root, output_path, ftest_nme);
        ftest_src = str_sum (ftest_nme, ".c");
        output_path = str_append (output_path, ftest_src);
        pFile = open_file (output_path, "w", NULL);
        fputs (test, pFile);
        add_test_in_scen (gtkvts_root, scen_file, output_dir, ftest_nme);
        
        free (test);
        free (ftest_src);
        free (ftest_nme);
        free (input_path);
        free (output_path);
        fclose (pFile);

        ep = readdir (pDir);
    }

    (void) closedir (pDir);

    free (test_tpl);
    free (output_dir_path);
    free (purpose_tpl);

    return (NULL);
}

/*
 *  Generates string that contains test for finput_tpl_path file
 *
 */
static char* 
gen_test (const char* test_tpl, const char* purpose_tpl, 
          const char* input_path, const char* group_nme, const char* test_nme)
{
    int i;
    int purposes_num = 0;

    char* test = NULL;
    char* test_purposes = NULL;
    char* common_tag_values[COMMON_TAGS_NUM];

    for (i = 0; i < COMMON_TAGS_NUM; i++)
    {
        common_tag_values[i] = NULL;
    }

    test_purposes = gen_all_test_purposes (input_path, purpose_tpl, 
                                           &purposes_num, test_nme); 
    if ((test_purposes == NULL) || (test_nme == NULL)) { return (NULL);};
    test = strdup (test_tpl);

    common_tag_values [GROUP_NAME_POS] = strdup (group_nme);
    common_tag_values [OBJECT_NAME_POS] = strdup (test_nme);
    common_tag_values [TEST_PURPOSES_POS] = test_purposes;
    common_tag_values [TET_HOOKS_POS] = gen_tet_hooks (purposes_num);
    /* do all sustitutions and deallocate memory allocated for tag_values [] */
    for (i = 0; i < COMMON_TAGS_NUM; i++)
    {
        test = replace_all_substr_in_string (test, common_tags[i], 
                                             common_tag_values[i]);
        free (common_tag_values [i]);
    }

    return test;
}

/*
 * Generates string that contains test purposes and calculate the number 
 * of purposes. In gen_test() this string uses as substitution 
 * for <%test_purposes%>.
 *
 */
char* 
gen_all_test_purposes (const char* fobject_tpl, const char* purpose_tpl,
                       int* purposes_number, const char* test_nme)
{
    char* global_section = NULL;
    char* tag_values[TAGS_NUM];
    char* currStr = NULL;
    char* tmp_tag = NULL;
    char* tmp_tag_inf = NULL;

    char* params[MAX_PARAMS_NUM];
    char* test_purposes = NULL;
    char* purpose_tpl_copy = NULL;
    char* pSave = NULL;

    int switch_case = 0,i;
    int size = 0;
    int paramsCount = 0;
    int str_count = 0;
    int purpNumSave = 0;
    int blocksNum = 0;
    int error = 0;
    int ignore = 0;

    FILE* wfile = NULL;
    *purposes_number = 0;

    wfile = open_file (fobject_tpl, "r", NULL);
    size = fsize(wfile);
    if (size < 2)
    {
        return (NULL);
    }
    for (i = 0; i < TAGS_NUM; i++) 
    { 
        tag_values[i] = NULL;
    }
    for (i = 0; i < MAX_PARAMS_NUM; i++) 
    { 
        params[i] = NULL;
    }

    currStr = (char*) calloc (size, sizeof(char));
    fgets_and_skip_comments (currStr, size, wfile, &str_count);
    while(!feof(wfile))
    {
        char** tag_values_copy = NULL;
        if (strstr(currStr, "<FUNCTION>")) 
        {
            blocksNum++;
            if ((purpNumSave == *purposes_number) && (blocksNum != 1))
            {
                print_warning ("%s.inp:before %d: warning: ", test_nme, str_count);
                print_warning ("no &gen_purpose() for the test case above\n %s", "");
            }
            purpNumSave = *purposes_number;
            /*<FUNCTION> is begining for other function test. 
              So we should clear tag_values[]*/ 
            for (i = 0; i < TAGS_NUM; i++) 
            { 
                if ((i != PURPNUM_TAG_POS) && (tag_values[i] != NULL)) 
                { 
                    free (tag_values[i]); 
                    tag_values[i] = NULL;
                }
            }
            switch_case = 0;
        } else 
        if (strstr(currStr, "<COMMENT>")) 
        {
            switch_case = 1;
        } else 
        if (strstr(currStr, "<DEFINE>")) 
        {
            if (tag_values[CODE_TAG_POS] != NULL)
            {
                free (test_purposes);
                test_purposes = NULL;
                print_warning ("%s.inp:near %d: error: ", test_nme, str_count);
                print_warning ("<DEFINE> tag after <CODE> found \n %s", "");
                error = 1;
                break;
            }
            switch_case = 2;
        } else
        if (strstr(currStr, "<CODE>")) 
        {
            if (tag_values[DEFINE_TAG_POS] == NULL) 
            {
                free (test_purposes);
                test_purposes = NULL;
                print_warning ("\n%s.inp:near %d: warning: ", test_nme, str_count);
                print_warning ("can't find <DEFINE> tag before <CODE> \n %s", "");
                break;
            }
            switch_case = 3;
        } else
        if (strstr(currStr, "<DISABLED>")) 
        {
            switch_case = 4;
        } else
        if (strstr (currStr, "&gen_purpose") == currStr) 
        {
            switch_case = 5;
        } else 
        if (strstr (currStr, "<GLOBAL>"))
        {
            for (i = 0; i < TAGS_NUM; i++)
            {
                if (tag_values[i] != NULL)
                {
                    free (test_purposes);
                    test_purposes = NULL;
                    print_warning ("%s.inp:near %d: error: ", test_nme, str_count);
                    print_warning ("<GLOBAL> tag should be replaced to the begining of file\n %s", "");
                    error = 1;
                    break;
                }
            }
            if (error) break;
            switch_case = 6;
        } else {
            switch_case = 7;
        }

        switch (switch_case)
        {
          case 0:
              fgets_and_skip_comments (currStr, size, wfile, &str_count);

              while(!strstr(currStr,"</FUNCTION>") && !feof(wfile))
              {
                  pSave = tag_values[FUNCTION_TAG_POS];
                  tag_values[FUNCTION_TAG_POS] 
                      = str_sum (tag_values[FUNCTION_TAG_POS], 
                                 currStr);
                  if (pSave != NULL) free (pSave);
                  fgets_and_skip_comments (currStr, size, wfile, &str_count);
              }

              check_function_tag (test_nme, str_count, tag_values[FUNCTION_TAG_POS]);
              tmp_tag = tag_values[FUNCTION_TAG_POS];
              if (tag_values[COMMENT_TAG_POS])
              {
                  free (tag_values[COMMENT_TAG_POS]);
                  tag_values[COMMENT_TAG_POS] = NULL;
              }
              tag_values[COMMENT_TAG_POS] = convert_to_comment (
                  tag_values[FUNCTION_TAG_POS]);

              break;
          case 1:
              fgets_and_skip_comments (currStr, size, wfile, &str_count);

              /* insert \n befor comment if it's need */
              if (currStr[0] != '\n')
              {
                  pSave = tag_values[COMMENT_TAG_POS];
                  tag_values[COMMENT_TAG_POS] = str_sum (tag_values[COMMENT_TAG_POS], 
                                                         "\n");
              }
              while(!strstr(currStr,"</COMMENT>") && !feof(wfile))
              {
                  pSave = tag_values[COMMENT_TAG_POS];
                  tag_values[COMMENT_TAG_POS] = str_sum (tag_values[COMMENT_TAG_POS], 
                                                         currStr);
                  fgets_and_skip_comments (currStr, size, wfile, &str_count);
              }
              break;
          case 2:
              fgets_and_skip_comments (currStr, size, wfile, &str_count);
              tag_values[DEFINE_TAG_POS] = strdup ("");
              while(!strstr(currStr,"</DEFINE>") && !feof(wfile))
              {
                  tag_values[DEFINE_TAG_POS] 
                      = str_append (tag_values[DEFINE_TAG_POS], currStr);
                  fgets_and_skip_comments (currStr, size, wfile, &str_count);
              }
              break;
          case 3:
              tag_values[CODE_TAG_POS] = strdup ("");
              fgets_and_skip_comments (currStr, size, wfile, &str_count);
              while(!strstr ( currStr, "</CODE>") && !feof(wfile))
              {
                  tag_values[CODE_TAG_POS] 
                      = str_append (tag_values[CODE_TAG_POS], currStr);
                  fgets_and_skip_comments (currStr, size, wfile, &str_count);
              }
              if (!tmp_tag_inf)
                  tmp_tag_inf = strdup (tag_values[CODE_TAG_POS]);
              else 
              {
                  if (strcmp (tmp_tag_inf, tag_values[CODE_TAG_POS]))
                  {
                      free (tmp_tag_inf);
                      tmp_tag_inf = strdup (tag_values[CODE_TAG_POS]);
                  }
              }
              break;
          case 4:
              {
              int bracket_num = 1;
              int open_position;
              open_position = str_count;
              fgets_and_skip_comments (currStr, size, wfile, &str_count);
              while(!feof(wfile) && !ferror(wfile))
              {
                  if (strstr(currStr,"</DISABLED>"))
                  {
                      bracket_num--;
                      if (bracket_num == 0) break;
                  } else 
                  if (strstr(currStr,"<DISABLED>"))
                  {
                      bracket_num++;                      
                  };
                  fgets_and_skip_comments (currStr, size, wfile, &str_count);
              };
              if (bracket_num != 0)
              {
                  free (test_purposes);
                  test_purposes = NULL;
                  print_warning ("%s.inp:near %d: error: ", test_nme, open_position);
                  print_warning ("[<DISABLED> number] - [</DISABLED> number] = %i\n ", bracket_num);
                  error = 1;
              };
              }
              break;
          case 5:
          {
              char* temp = NULL;
              char* p1 = NULL;
              char* pch = NULL;

              paramsCount = 0;
              if (ignore_list)
              {
                  temp = (char*) calloc (strlen(ignore_list), sizeof(char));
                  strcpy (temp,ignore_list);
              }
              fgets_and_skip_comments (currStr, size, wfile, &str_count);
              while(!strstr(currStr," );") && !feof(wfile))
              {
                  /* ignore tests*/
                  if (ignore_list && strstr (currStr,"&Ignore_List:"))
                  {
                      strcpy (temp,ignore_list);
                      if (!strstr (temp,"#"))
                      {
                          if (strstr(currStr,temp))
                          {
                              while(!strstr(currStr," );") && !feof(wfile))
                              {
                                  fgets_and_skip_comments (currStr, size, wfile, &str_count);
                              }
                              ignore = 1;
                              break;
                          }
                      } else {
                          pch = strtok (temp," ");
                                 
                          while (pch != NULL)
                          {
                              p1 = "&Ignore_List: ";
                              pch = strtok (NULL,"#;");
                              if (!pch) break;
                              p1 = str_sum (p1,pch);
                              p1 = str_sum (p1,";");
                              if (strstr(currStr,p1))
                              {
                                  while(!strstr(currStr," );") && !feof(wfile))
                                  {
                                      fgets_and_skip_comments (currStr, size, wfile, &str_count);
                                  }
                                  ignore = 1;
                                  break;
                              }
                          }
                      }
                      free(temp);
                      free(p1);
                      free(pch);
                      if (ignore)
                          break;
                  }
                  /* read bug id*/
                  if (strstr(currStr,"&Bug_id:"))
                  {
                      char* tmp = NULL;
                      char* buff = NULL;

                      bug_id = strdup (currStr);
                      bug_id = trim (bug_id);

                      bug_id = replace_all_substr_in_string (bug_id,"\t","");
                      bug_id = replace_all_substr_in_string (bug_id,"\n","");
                      bug_id = replace_all_substr_in_string (bug_id," ","");
                      bug_id = replace_all_substr_in_string (bug_id,"&Bug_id:","");
                      bug_id = replace_all_substr_in_string (bug_id,"|"," | ");
                      bug_id = replace_all_substr_in_string (bug_id,"("," (");
                      bug_id = replace_all_substr_in_string (bug_id,"{"," {");
                      bug_id = replace_all_substr_in_string (bug_id,",",", ");

                      buff = str_sum ("BUG ID: ",bug_id);
                      buff = str_append (buff,"\n");
                      tmp = str_sum (buff, tmp_tag);
                      buff = str_append (buff, tmp_tag);
                      if (tag_values[COMMENT_TAG_POS])
                      {
                          free (tag_values[COMMENT_TAG_POS]);
                          tag_values[COMMENT_TAG_POS] = NULL;
                      }
                      tag_values[COMMENT_TAG_POS] = convert_to_comment (buff);
                      free (buff);
                      
                      buff = str_sum ("bug_inf =", " \"");
                      buff = str_append (buff, tmp);
                      buff = str_append (buff, "\";\n");
                      buff = str_append (buff,"if(bug_inf) tet_printf(\"    %s\",bug_inf);\n");

                      if (tag_values[CODE_TAG_POS])
                      {
                          buff = str_append (buff, tag_values[CODE_TAG_POS]);
                          free (tag_values[CODE_TAG_POS]);
                          tag_values[CODE_TAG_POS] = NULL;
                      }

                      tag_values[CODE_TAG_POS] = strdup (buff);

                      free (bug_id);
                      free (buff);
                      free (tmp);
                  } else
                  {
                      if (tag_values[COMMENT_TAG_POS] 
                          && strcmp (tag_values[COMMENT_TAG_POS],convert_to_comment (tmp_tag)))
                      {
                          free (tag_values[COMMENT_TAG_POS]);
                      }
                      tag_values[COMMENT_TAG_POS] 
                          = convert_to_comment (tmp_tag);
                      if (tmp_tag_inf && strcmp (tmp_tag_inf, tag_values[CODE_TAG_POS]))
                      {
                          if (tag_values[CODE_TAG_POS])
                          {
                              free (tag_values[CODE_TAG_POS]);
                          tag_values[CODE_TAG_POS] = NULL;
                          }
                          tag_values[CODE_TAG_POS] = strdup (tmp_tag_inf);
                      }
                      
                  }
                  
                  /* read params  */
                  params[paramsCount] = strdup (currStr);
                  params[paramsCount] = trim (params[paramsCount]);

                  /* remove '\n' from the end of the string */
                  params[paramsCount][strlen (params[paramsCount]) - 1]='\0';
                  paramsCount++;    
                  if (paramsCount >= MAX_PARAMS_NUM)
                  {
                      break;
                  }
                  fgets_and_skip_comments (currStr, size, wfile, &str_count);
              }
              if (ignore)
              {
                  ignore = 0;
                  break;
              }

              if (paramsCount >= MAX_PARAMS_NUM)
              {
                  free (test_purposes);
                  test_purposes = NULL;
                  break;
              }
              
              /* do parameters substitutions */

              tag_values_copy = do_substitutions (TAGS_NUM, tag_values, 
                                                  paramsCount, params);

              if (strstr (tag_values_copy[CODE_TAG_POS],"bug_inf"))
              {
                  char* p;
                  char* pch1;
                  char* pch2;
                  char* tmp_buff;
                  char* tmp2;

                  pch1 = strstr (tag_values_copy[CODE_TAG_POS],"bug_inf = \"");
                  pch2 = strstr (tag_values_copy[CODE_TAG_POS],"\";\n");

                  tmp_buff = get_substr 
                      (tag_values_copy[CODE_TAG_POS], 
                       pch1 - tag_values_copy[CODE_TAG_POS] + 11,
                       pch2 - tag_values_copy[CODE_TAG_POS] - 3);
                  tmp2 = get_substr 
                      (tag_values_copy[CODE_TAG_POS], 
                       pch2 - tag_values_copy[CODE_TAG_POS] - 1,
                       strlen (tag_values_copy[CODE_TAG_POS]) - 2);
                  tmp_buff = replace_all_substr_in_string (tmp_buff,"\\","\\\\");
                  tmp_buff = replace_all_substr_in_string (tmp_buff,"\"","\\\"");
                  tmp_buff = replace_all_substr_in_string (tmp_buff,"\n","\\n\\\n");
                  p = tmp_buff;
                  tmp_buff = str_sum ("bug_inf = \"", tmp_buff);
                  if (p) free (p);

                  tmp_buff = str_append (tmp_buff, tmp2);
                  
                  if (tag_values_copy[CODE_TAG_POS])
                  {
                      free (tag_values_copy[CODE_TAG_POS]);
                  }
                  tag_values_copy[CODE_TAG_POS] = tmp_buff;
                  
                  free (tmp2);

              }

              for (i = 0; i < paramsCount; i++) 
              { 
                  free (params[i]);
              }
              if (tag_values_copy == NULL)
              {
                  free (test_purposes);
                  test_purposes = NULL;
                  break;
              }
              /* generate new purpose string and add it to test_purposes */
              (*purposes_number)++;
              if (tag_values_copy[PURPNUM_TAG_POS])
              {
                  free (tag_values_copy[PURPNUM_TAG_POS]);
              }
              tag_values_copy[PURPNUM_TAG_POS] = (char*) calloc (8, sizeof (char));
              sprintf (tag_values_copy[PURPNUM_TAG_POS], "%d", *purposes_number);
              
              purpose_tpl_copy = strdup (purpose_tpl);
              for (i = 0; i < TAGS_NUM; i++)
              {
                  if (tag_values_copy[i] == NULL)
                  {
                      tag_values_copy[i] = alloc_mem_for_string (tag_values_copy[i], 1);
                      tag_values_copy[0] = '\0';
                  }
                  purpose_tpl_copy = replace_all_substr_in_string (purpose_tpl_copy, 
                                                                   tags[i], 
                                                                   tag_values_copy[i]);
                  free (tag_values_copy[i]);
                  tag_values_copy[i] = NULL;
              }
              
              if (!test_purposes)
              {
                  test_purposes = alloc_mem_for_string 
                      (test_purposes, strlen (purpose_tpl_copy));
              }
              test_purposes = str_append (test_purposes, purpose_tpl_copy);
              free (tag_values_copy);
              tag_values_copy = NULL;
              free (purpose_tpl_copy);
              break;
          }
          case 6:
              fgets (currStr, size, wfile);
              str_count++;
              while(!strstr(currStr,"</GLOBAL>") && !feof(wfile))
              {
                  pSave = global_section;
                  global_section = str_sum (global_section, currStr);
                  if (pSave != NULL) free (pSave);
                  fgets (currStr, size, wfile);
                  str_count++;
              };
              pSave = test_purposes;
              test_purposes = str_sum (test_purposes, global_section);
              if (pSave != NULL) free (pSave);
              if (global_section != NULL) free (global_section);
              break;
        };

        if (error) break;
        fgets_and_skip_comments (currStr, size, wfile, &str_count);
    }

    if (tmp_tag_inf)
    {
        free (tmp_tag_inf);
    }

    pSave = test_purposes;
    if (test_purposes)
    {
        test_purposes = str_sum ("gchar* bug_inf = NULL;\n",test_purposes);
        free (pSave);
    }
    
    if ((purpNumSave == *purposes_number))
    {
        print_warning ("\n%s.inp:before %d: warning: ", test_nme, str_count);
        print_warning ("no &gen_purpose() for the test case above\n  %s", "");
    }

    for (i = 0; i < TAGS_NUM; i++)
    {
        if (tag_values[i] != NULL) 
        {
            free (tag_values[i]);
            tag_values[i] = NULL;
        }
    }

    free (currStr);
    fclose (wfile);

    return (test_purposes);
} 

/*
 * Do parameters substitutions
 *
 */
static char**  
do_substitutions (int tags_num, char* tags[], int params_num, char* params[]) 
{
    int i, j = 0;
    char** res_tags = NULL;
    char* pch = NULL;
    char buff[100];

    res_tags = (char**) calloc (tags_num, sizeof(char*));
    for (i = 0; i < tags_num; i++) 
    {
        if (tags[i] == NULL)
        {
            res_tags[i] = NULL;
            continue;        
        }
        pch = alloc_mem_for_string (pch, strlen (tags[i]));
        strcpy (pch, tags[i]);

        for (j = 0; j < params_num; j++) 
        {
            sprintf(buff, "<%%%i%%>", j);
            pch = replace_all_substr_in_string(pch, buff, params[j]);
        }
        switch (i)
        {
          case DEFINE_TAG_POS:
          case CODE_TAG_POS:
          {
              char* p = pch;
              pch = do_indent (pch, INDENT);
              if (p) free (p);
              break;
          }
        }

        res_tags[i] = calloc (sizeof (char), strlen (pch) + 1);
        strcpy (res_tags[i], pch);
        free (pch);
        pch = NULL;
    }    

    return res_tags;
}


/*
 *  Generates string that contains tet_hooks
 */
static char* 
gen_tet_hooks (int number)
{
    int i;
    char iStr[8]; 
    char* tet_hook_tpl = "{test_purpose_<%purpose_number%>, 1},\n"; 
    char* cur_tet_hook = NULL;
    char* all_tet_hooks = NULL;
    char* pSave = NULL;    

    for(i=1; i<=number; i++)
    {
        sprintf (iStr, "%d", i);
        cur_tet_hook = strdup (tet_hook_tpl);
        cur_tet_hook = replace_all_substr_in_string (cur_tet_hook, 
                                                     "<%purpose_number%>", iStr);
        pSave = all_tet_hooks;
        all_tet_hooks = str_sum (all_tet_hooks, cur_tet_hook);
        if (pSave != NULL) free (pSave);
        free (cur_tet_hook);
    }
    if (i > 0)
    { 
        char* p;
        all_tet_hooks[strlen (all_tet_hooks) - 1] = '\0';

        p = all_tet_hooks;
        all_tet_hooks = do_indent (all_tet_hooks, INDENT);
        if (p) free (p);
    }
    return all_tet_hooks;
}


/*
 * Generate a makefile 
 */
static void 
gen_makefile (const char* gtkvts_root, const char* dir_path, const char* ftest_nme)
{
    FILE* mf;
    char* makefile_path = NULL;

    makefile_path = str_sum(dir_path, "/makefile");
    mf = open_file (makefile_path, "w+", NULL);
    
    fprintf (mf, "TET_INC_DIR    = $(TET_ROOT)/inc/tet3\n");
    fprintf (mf, "TET_LIB_DIR    = $(TET_ROOT)/lib/tet3\n");
    fprintf (mf, "GTKVTS_ROOT    = %s\n", gtkvts_root);
    fprintf (mf, "GTKVTS_LIB_DIR = $(GTKVTS_ROOT)/lib\n");
    fprintf (mf, "\n");    
    fprintf (mf, "OS = $(shell uname -s)\n");
    fprintf (mf, "LIBS_SunOS = -lX11\n");
    fprintf (mf, "LIBS_Linux = \n");
    fprintf (mf, "ADD_LIBS = $(LIBS_$(OS))\n");
    fprintf (mf, "\n");    
    fprintf (mf, "LIBS = $(TET_LIB_DIR)/tcm.o $(TET_LIB_DIR)/libapi.a $(GTKVTS_LIB_DIR)/libthr.a $(GTKVTS_LIB_DIR)/librobot.a $(GTKVTS_LIB_DIR)/libtestgen.a $(GTKVTS_LIB_DIR)/libfile.a $(GTKVTS_LIB_DIR)/libstr.a $(GTKVTS_LIB_DIR)/libmem.a $(ADD_LIBS)\n");
    fprintf (mf, "\n");    
    fprintf (mf, "%s: %s.c $(TET_INC_DIR)/tet_api.h\n", 
             ftest_nme, ftest_nme);    
    fprintf (mf, "\t$(CC) ");
    {
        char* cflags = getenv ("CFLAGS");
        if (cflags)
        {
            fprintf (mf, "%s", cflags);
        }
    }
    fprintf (mf, " `pkg-config --cflags gtk+-2.0` -I$(TET_ROOT)/gtkvts/src/tools/testgen -I$(TET_ROOT)/gtkvts/src/libs -I$(TET_INC_DIR) -o $@ $< `pkg-config --libs gtk+-2.0` $(LIBS)\n");    
    fprintf (mf, "\tchmod a+x %s\n", ftest_nme);    
    fprintf (mf, "\n");    
    fprintf (mf, "clean:\n");    
    fprintf (mf, "\trm -rf %s.c \n", ftest_nme);    
    fprintf (mf, "\n");    

    fclose (mf);
    free (makefile_path);
}

/*
 *   Generates TET scen file
 */
static void 
add_test_in_scen (const char* gtkvts_root, const char* scen_file, 
                  const char* output_dir, const char* ftest_nme)
{
    FILE* sf = NULL;
    char* scen_item = NULL;
    char* short_output_dir = NULL;

    if ( !scen_file || !output_dir || !ftest_nme)
    {
        return;
    }
    sf = open_file (scen_file, "a+", NULL);

    short_output_dir = replace_substr_in_string ((char*) output_dir, 
                                                 gtkvts_root, "");
    scen_item = str_sum (scen_item, short_output_dir);
    if (short_output_dir[0] != '/')
    {
        char* p = scen_item;
        scen_item = str_sum ("/", scen_item);
        free (p);
    }
    if (short_output_dir[strlen (short_output_dir) - 1] != '/')
    {
        scen_item = str_append (scen_item, "/");
    }
    scen_item = str_append (scen_item, ftest_nme);
    scen_item = str_append (scen_item, "/");
    scen_item = str_append (scen_item, ftest_nme);
    fprintf (sf, "\t%s\n", scen_item);
    
    free (scen_item);
    free (short_output_dir);
    fclose (sf);
}

/*
 * Check <FUNCTION> tag structure
 */
static char*  
check_function_tag (const char* test_nme, int str_number, char* fTag)
{
    #define ITEMS_NUM 6
    char* items[ITEMS_NUM] = { "Name:",
                                      "Params:",
                                      "Returns:",
                                      "Assertion:",
                                      "Params Values:",
                                      "Expected:" };
    int i;
    char* new_pos = NULL;
    char* old_pos = NULL;
    char* start_block = NULL;


    if (fTag == NULL)
    {
        print_warning ("%s.inp:before %d: warning: ", test_nme, str_number);
        print_warning (" empty <FUNCTION> tag contents \n %s", "");
        return (NULL);
    }


    start_block = fTag;
    new_pos = start_block;

    for (i = 0; i < ITEMS_NUM; i++)
    {
        old_pos = new_pos;
        new_pos = strstr (start_block, items[i]);
        if ((new_pos != NULL) && (old_pos > new_pos))
        {
            print_warning ("%s.inp:before %d: warning: ", test_nme, str_number);
            print_warning (" <FUNCTION> tag items order is wrong \n %s", "");
            print_warning (" '%s' before '%s'\n", items[i], items[i-1]);
            return (NULL);
        } else if (new_pos == NULL) {
            print_warning ("%s.inp:before %d: warning: ", test_nme, str_number);
            print_warning (" Can't find '%s' item\n", items[i]);
            return (NULL);
        }
        start_block = new_pos + 1;
    }
    #undef ITEMS_NUM
    return (NULL);

}

/*
 * Reads strings from stream and stores them in str,
 * while not comment string is reached, and increaces str_counter.
 */
static char*
fgets_and_skip_comments (char* str, int num, FILE* stream, int* str_counter)
{
    char* macros_str[] = { "#define ", "#undef ", "#include ", "#line ", NULL};
    int i;
    int macro = 0;  

    do
    {
        fgets (str, num, stream);
        (*str_counter)++;
        i = 0;
        while (macros_str[i] != NULL)
        {
            if (strstr ( str, macros_str[i]) )
            {
                macro = 1;
                break;
            }
            i++;
        }
    }
    while ((str[0] == '#') && !macro && (!feof(stream)) && (!ferror(stream)));

    if ((feof(stream)) || (ferror(stream)))
    {
        return NULL;
    } else {
        return (str);
    }
}




