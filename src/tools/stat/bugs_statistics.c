#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <common.h>
#include "libfile.h"
#include "libstr.h"

#define TEST_NUM 15000
#define BUG_WORD "BUG ID: "
typedef struct 
{
    char* name;
    char* result;
    char* bug;
    char* func;
} Table_result;

Table_result table[TEST_NUM];
int bg = 0;

void free_table (int count)
{
    int i = 0;
    for (i = 0;i<=count;i++)
    {
        if(table[i].name) free (table[i].name);
        if(table[i].result) free (table[i].result);
        if(table[i].bug) free (table[i].bug);
        if(table[i].func) free (table[i].func);
    }
}

void print_results (int params,int count)
{
    Table_result tmp;
    int i;
    char* buff = NULL;
 
    switch (params)
    {
      case 1:
          printf ("Tests fail without bugs:\n");
          for (i = 0; i < count; i++)
          {
              tmp = table[i];
              if (!strcmp (tmp.result,"FAIL") && !strcmp (tmp.bug,"NONE"))
              {
                  printf ("\nBug: %s, Number of test: '%i', Result: %s\nTest Case: %s\n--Title-- %s\n",
                          tmp.bug,i+1,tmp.result,tmp.name,tmp.func);
                  printf ("\n----------------------------------------------------------------------------\n");
                  bg = 1;
              }
          };
          break;
      case 2:
          printf ("Failure due to bugs:\n");
          for (i = 0; i < count; i++)
          {
              tmp = table[i];
              if (!strcmp (tmp.result,"FAIL")  && strcmp (tmp.bug,"NONE") )
              {
                  printf ("\nBug: %s, Number of test: '%i', Result: %s\nTest Case: %s\n--Title-- %s\n",
                          tmp.bug,i+1,tmp.result,tmp.name,tmp.func);
                  printf ("\n----------------------------------------------------------------------------\n");
                  bg = 1;
              }
          };
          break;
      case 3:
          printf ("Tests passed with bugs:\n");
          for (i = 0; i < count; i++)
          {
              tmp = table[i];
              if (!strcmp (tmp.result,"PASS") && strcmp (tmp.bug,"NONE"))
              {
                  printf ("\nBug: %s, Number of test: '%i', Result: %s\nTest Case: %s\n--Title-- %s\n",
                          tmp.bug,i+1,tmp.result,tmp.name,tmp.func);
                  printf ("\n----------------------------------------------------------------------------\n");
                  bg = 1;
              }
          };
          break;
      case 4:
          printf ("Tests unresolved:\n");
          for (i = 0; i < count; i++)
          {
              tmp = table[i];
              if (!strcmp (tmp.result,"UNRESOLVED"))
              {
                  printf ("\nBug: %s, Number of test: '%i', Result: %s\nTest Case: %s\n--Title-- %s\n",
                          tmp.bug,i+1,tmp.result,tmp.name,tmp.func);
                  printf ("\n----------------------------------------------------------------------------\n");
                  bg = 1;
              }
          };
          break;
      case 5:
          printf ("Bugs mentioned in func tests:\n");

          buff = (char*) calloc (6,sizeof(char));
          for (i = 0; i < count; i++)
          {
              tmp = table[i];
              if (strcmp (tmp.bug,"NONE") && !strstr(buff,tmp.bug))
              {
                  buff = str_append (buff,tmp.bug);
                  buff = str_append (buff,"\n");
                  bg = 1;
              }
          }
          if(bg) 
          {
              printf ("%s",buff);
              printf ("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n");
          } else {
              printf ("- None -\n\n");
          }
          if (buff) free (buff);
          break;
    }
}

void bug_statistics (const char* file,int var)
{
    Table_result journal;
    FILE* pFile = NULL;
    int size = 0;
    char* currStr;
    char* pch;
    char* p1;
    char* test_case = NULL;
    char* test_result = NULL;
    char* bug = NULL;
    char* func = NULL;

    int count = 0;

// Creating table for file
    pFile = open_file (file,"r",NULL);
    size = fsize(pFile);

    currStr = (char*) calloc (size, sizeof(char));
    while(!feof(pFile))
    {
        fgets (currStr, size, pFile);   
        if (strstr(currStr, "test case:")) 
        {
            pch = strstr(currStr, "test case:");
            test_case = get_substr (currStr,pch-currStr+10,strlen(currStr)-3);
            fgets (currStr, size, pFile);   
            while (!strstr(currStr,"TP Start") && !strstr(currStr,"IC End") && !feof(pFile))
            {
                if (strstr(currStr, "PASS"))
                {
                    test_result = strdup ("PASS");
                }else
                if (strstr(currStr, "FAIL"))
                {
                    test_result =  strdup ("FAIL");
                }else
                if (strstr(currStr, "UNRESOLVED"))
                {
                    test_result = strdup ("UNRESOLVED");
                }else
                    test_result = NULL;
                if (strstr(currStr, BUG_WORD))
                {
                    pch = strstr(currStr, BUG_WORD);
                    bug = get_substr (currStr,pch-currStr+8,strlen (currStr)-2);

                    fgets (currStr, size, pFile);
                
                    if (strstr(currStr, "Name:"))
                    {
                        pch = strstr(currStr, "Name:");
                        p1 = get_substr (currStr,pch-currStr,strlen (currStr)-1);
                        func = strdup ("\n");
                        func = str_append (func,p1);
                        free (p1);

                        while (!strstr (currStr,"Expected:"))
                        {
                            fgets (currStr, size, pFile);
                            if (strstr(currStr, "Params:"))
                            {
                                pch = strstr(currStr, "Params:");
                                p1 = get_substr (currStr,pch-currStr,strlen (currStr)-1);
                                func = str_append (func,p1);
                                free(p1);
                            }
                            if (strstr(currStr, "Returns:"))
                            {
                                pch = strstr(currStr, "Returns:");
                                p1 = get_substr (currStr,pch-currStr,strlen (currStr)-1);
                                func = str_append (func,p1);
                                free(p1);
                            }
                            if (strstr(currStr, "Assertion:"))
                            {
                                pch = strstr(currStr, "Assertion:");
                                p1 = get_substr (currStr,pch-currStr,strlen (currStr)-1);
                                func = str_append (func,p1);
                                free(p1);
                            }
                            if (strstr(currStr, "Params Values:"))
                            {
                                pch = strstr(currStr, "Params Values:");
                                p1 = get_substr (currStr,pch-currStr,strlen (currStr)-1);
                                func = str_append (func,p1);
                                free(p1);
                            }
                            if (strstr(currStr, "Expected:"))
                            {
                                pch = strstr(currStr, "Expected:");
                                p1 = get_substr (currStr,pch-currStr,strlen (currStr)-1);
                                func = str_append (func,p1);
                                free(p1);
                            }
                        }
                    }
                }
                fgets (currStr, size, pFile);
            }
        } else {
            test_case = strdup ("None");
            test_result = NULL;
        }
        
        if (test_result)
        {

            journal.name = strdup (test_case);
            journal.result = strdup (test_result);
            if (bug)
            {
                journal.bug  = strdup (bug);
                free (bug);
                bug = NULL;
            }
            else
                journal.bug = strdup ( "NONE");

            if (func)
            {
                journal.func = strdup (func);
                free (func);
                func = NULL;
            }
            else
                journal.func = strdup ("\n  NONE");
        
            table[count].result = strdup (journal.result);
            table[count].bug = strdup (journal.bug);
            table[count].name = strdup (journal.name);
            table[count].func = strdup (journal.func);

            count++;


            if (journal.name) free (journal.name);
            if (journal.result) free (journal.result);
            if (journal.func) free (journal.func);
            if (journal.bug) free (journal.bug);

        } else {
            if (bug)
            {
                free (bug);
                bug = NULL;
            }
            if (func)
            {
                free (func);
                func = NULL;
            }
        }
        free (test_case);
        if (test_result) free (test_result);
    }
    if (test_result) free (test_result);
    free (currStr);
    fclose (pFile);

    printf ("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n");
    if (var == 1)
        print_results (1,count);
    if (var == 2)
        print_results (2,count);
    if (var == 3) 
        print_results (3,count);
    if (var == 4)
        print_results (4,count);
    if (var == 5) 
    {
        printf ("======= All statistics. =======\n");
        print_results (1,count);
        printf ("\n============================================================================\n");
        printf ("============================================================================\n");
        print_results (2,count);
        printf ("\n============================================================================\n");
        printf ("============================================================================\n");
        print_results (3,count);
        printf ("\n============================================================================\n");
        printf ("============================================================================\n");
        print_results (4,count);
    }
    if (var == 6)
    {
        print_results (5,count);
    }
    if (!bg)
    {
        printf ("Journal file hasn't bug information.");
        printf ("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n");
    }
    free_table (count);
}

int 
main (int argc, char* argv[]) 
{
    if (argc == 1)
    {
        printf ("\nusage: bug_statistics [params] <path_to_journal_file>\n\n");
        printf ("\t<path_to_journal_file> \tPath to journal file;\n");
        printf ("\t[params]: \n\t\t{-fail_without_bug (-fwb),\n\t\t -fail_with_bug (-fb),\n\t\t -pass_with_bug (-pb),\n\t\t -unresolved (-u),\n\t\t -list (list of bugs),\n\t\t -all (default option).} \n");
        printf ("\n\n");
        return 0;
    }
    if (strstr (argv[1], "-f") 
        || strstr (argv[1], "-p") 
        || strstr (argv[1], "-u")
        || !strcmp (argv[1], "-all")
        || !strcmp (argv[1], "-list"))
    {       
        if (argc != 3)
        {
            printf ("\nusage: bug_statistics [params] <path_to_journal_file>\n\n");
            printf ("\t<path_to_journal_file> \tPath to journal file;\n");
            printf ("\t[params]: \n\t\t{-fail_without_bug (-fwb),\n\t\t -fail_with_bug (-fb),\n\t\t -pass_with_bug (-pb),\n\t\t -unresolved (-u),\n\t\t -list (list of bugs),\n\t\t -all (default option).} \n");
            printf ("\n\n");
            return 0;
        } else {
            if (!strcmp(argv[1],"-fail_without_bug") || !strcmp(argv[1],"-fwb"))
                bug_statistics (argv[2],1);
            else  if (!strcmp(argv[1],"-fail_with_bug") || !strcmp(argv[1],"-fb"))
                bug_statistics (argv[2],2);
            else  if (!strcmp(argv[1],"-pass_with_bug") || !strcmp(argv[1],"-pb"))
                bug_statistics (argv[2],3);
            else  if (!strcmp(argv[1],"-unresolved") || !strcmp(argv[1],"-u"))
                bug_statistics (argv[2],4);
            else  if (!strcmp(argv[1],"-all"))
                bug_statistics (argv[2],5);
            else  if (!strcmp(argv[1],"-list"))
                bug_statistics (argv[2],6);
            else printf ("\nWrong parameters\n\n");
            return 0;
        }
    } else {
        if (argc != 2)
        {
            printf ("\nusage: bug_statistics [params] <path_to_journal_file>\n\n");
            printf ("\t<path_to_journal_file> \tPath to journal file;\n");
            printf ("\tparams: \n\t\t{-fail_without_bug (-fwb),\n\t\t -fail_with_bug (-fb),\n\t\t -pass_with_bug (-pb),\n\t\t -unresolved (-u),\n\t\t -list (list of bugs),\n\t\t -all (default option).} \n");
            printf ("\n\n");
            return 0;
        } else {
                bug_statistics (argv[1],5);
                return 0;
        }
    }
    return 0;
}
