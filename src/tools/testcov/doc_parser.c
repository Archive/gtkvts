#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <common.h>
#include "libthr.h"
#include "libstr.h"
#include "libfile.h"
#include "libmem.h"

#define HTML_DOC_DIR_PATH_POS 1
#define EXC_LIST_FILE_NAME_POS 2
#define HDR_LIST_FILE_NAME_POS 3
#define ASR_LIST_FILE_NAME_POS 4
#define IGNORE_LIST_POS 5
#define S_DOC_PARSER_PATH_POS 6
#define X_DOC_PARSER_PATH_POS 7

int 
main (int argc, char** argv) 
{
    char* input_path = NULL;
    FILE* index_html = NULL;
    char* line = NULL;

    input_path = concat_paths (argv[HTML_DOC_DIR_PATH_POS], "index.html");
    index_html = open_file (input_path, "r", NULL);
    line = alloc_mem_for_string (line, fsize (index_html));
    fgets (line, fsize (index_html), index_html);

    if (strstr (line, "DTD HTML"))
    {
        char* exec_data [] = 
            {argv[X_DOC_PARSER_PATH_POS],
             argv[HTML_DOC_DIR_PATH_POS],
             argv[EXC_LIST_FILE_NAME_POS],
             argv[HDR_LIST_FILE_NAME_POS],
             argv[ASR_LIST_FILE_NAME_POS],
             argv[IGNORE_LIST_POS],
             NULL};

        printf ("+++++Found XML DocBook format docs+++++\n");
        printf ("-----Running %s %s %s %s %s %s...\n", argv[X_DOC_PARSER_PATH_POS], 
                argv[HTML_DOC_DIR_PATH_POS], argv[EXC_LIST_FILE_NAME_POS],
                argv[HDR_LIST_FILE_NAME_POS], argv[ASR_LIST_FILE_NAME_POS],
                argv[IGNORE_LIST_POS]);

        exec_program (argv[X_DOC_PARSER_PATH_POS], exec_data);
    } else {
        char* exec_data [] = 
            {argv[S_DOC_PARSER_PATH_POS],
             argv[HTML_DOC_DIR_PATH_POS],
             argv[EXC_LIST_FILE_NAME_POS],
             argv[HDR_LIST_FILE_NAME_POS],
             argv[ASR_LIST_FILE_NAME_POS],
             argv[IGNORE_LIST_POS],
             NULL};

        printf ("+++++Found SGML DocBook format docs+++++\n");
        printf ("------Running %s %s %s %s %s %s...\n", argv[S_DOC_PARSER_PATH_POS], 
                argv[HTML_DOC_DIR_PATH_POS], argv[EXC_LIST_FILE_NAME_POS],
                argv[HDR_LIST_FILE_NAME_POS], argv[ASR_LIST_FILE_NAME_POS],
                argv[IGNORE_LIST_POS]);

        exec_program (argv[S_DOC_PARSER_PATH_POS], exec_data);
    }

    free (input_path);
    free (line);
    fclose (index_html);

    return 0;
}
