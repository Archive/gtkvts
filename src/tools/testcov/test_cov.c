#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <common.h>
#include "libtestcov.h"
#include "libstr.h"
#include "libfile.h"



int
main (int argc, char** argv)
{
    DIR* dir;
    struct dirent* ep;
    TestListItem* tree = NULL;
    gchar* cname;
    gchar buff[200];
    gchar*  dname = argv[1];
    gchar*  aname = argv[2];

    if (argc < 3)
    {
        printf ("\nusage: test_cov  <source dir> <assertion file> [type] [state]\n\n");
        printf ("\t<source dir> \t\tsource directory\n");
        printf ("\t<assertion file> \tpath to assertion list\n");
        printf ("\t[type] \t\t\t{-class (-c), -function (-f), -assertion (-a)}\n");
        printf ("\t[state]\t\t\t{-tested_documented (-td), -not_tested_documented (-ntd),\n\t\t\t\t -tested_not_documented (-tnd), -not_tested_not_documented (-ntnd)}\n");
        printf ("\n\n");
        return 0;
    }

    tree = test_list_new ();
    if (!tree)
    {
        return 0;
    }

    test_list_add_assertion_file (tree, aname);

    dir = opendir (dname);
    ep = readdir (dir);
    while (ep)
    {
        cname = (gchar*)str_sum (dname, ep->d_name);
        sprintf (buff, "%s/%s/%s.c", dname, ep->d_name, ep->d_name);
        fprintf (stderr, ".");
        test_list_add_source_file (tree, buff);
        ep = readdir (dir);
    }
    
    if (argc > 3)
        test_list_set_params (argv[3]);
    if (argc > 4)
        test_list_set_params (argv[4]);

    test_list_print (tree, "result");
    test_list_free (tree);

    return 0;
}


