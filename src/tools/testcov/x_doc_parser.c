#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <gtk/gtk.h>
#include <common.h>
#include "hdr_tests_generator.h"
#include "libfile.h"
#include "libmem.h"
#include "libstr.h"
#include "libtestgen.h"
#include "libtestcov.h"

#define HTML_DOC_DIR_PATH_POS 1
#define EXC_LIST_FILE_NAME_POS 2
#define HDR_LIST_FILE_NAME_POS 3
#define ASR_LIST_FILE_NAME_POS 4
#define IGNORE_LIST_POS 5

static char* gen_title (const char* input_file_name);
static void add_func_nodes_to_class_node (TestListItem* class_node, 
                                                   const char* input_file_name);
static TestListItem* read_doc_to_tree (const char* input_path);

const char *H2_MARK = "<h2";
const char *EXAMPLE_SUB_TAG = "class=\"informalexample\"";
const char *FUNC_DECL_SUB_TAG = "class=\"refsect2\"";
const char *PROGRAMLISTING_SUB_TAG = "class=\"programlisting\"";
const char *WARNING_SUB_TAG = "class=\"warning\"";
const char *inp_ext = ".html";
const char *DETAILS_MARK = "<h2>Details</h2>";
const char *PROPERTIES_MARK = "<h2>Properties</h2><div class=\"variablelist\"";
const char *STYLE_PROPERTIES_MARK = "<h2>Style Properties</h2><div class=\"variablelist\"";
const char *SIGNALS_MARK = "<h2>Signals</h2>";
const char *DEPRECATED_TEXT = "is deprecated and should not be used in newly-written code";

int 
main (int argc, char** argv) 
{
    char* input_path = concat_paths (argv[HTML_DOC_DIR_PATH_POS], "/");
    char* asr_exclude_list_file_name = argv[EXC_LIST_FILE_NAME_POS];
    char* hdr_list_file_name = argv[HDR_LIST_FILE_NAME_POS];
    char* asr_list_file_name = argv[ASR_LIST_FILE_NAME_POS];
    char* ignore_list = argv[IGNORE_LIST_POS];

    TestListItem *tree;

    if (!is_directory_exists (argv[HTML_DOC_DIR_PATH_POS]))
    {
        printf ("\ndoc_parser warning: can't find GTK+ docs in '%s';\n\
doc_parser_warning: coverage/asr_list.txt and header tests will not be generated\n\n", argv[HTML_DOC_DIR_PATH_POS]);
    } else {
        tree = read_doc_to_tree (input_path);
        gen_hdr_list_from_tree (tree, hdr_list_file_name);
        test_list_append_exclude_list_file
            (tree, asr_exclude_list_file_name, ignore_list);
        gen_asr_list_from_tree (tree, asr_list_file_name);
        test_list_free (tree);
    }
    
    free (input_path);
    exit (0);
}


static TestListItem* read_doc_to_tree (const char* input_path)
{
    TestListItem *tree = test_list_new();
    TestListItem *class_node;

    char* title = NULL;

    DIR* pdir;
    struct dirent* dp;

    pdir = opendir (input_path);
    while (pdir)
    {
        if ((dp = readdir(pdir)) != NULL) {
            if (strstr (dp->d_name, inp_ext))
            {                
                char* input_path_str = concat_paths ((char*) input_path, dp->d_name);

                printf ("Scanning '%s'...\n", input_path_str);
                title = gen_title (input_path_str);
                class_node = test_list_add (tree, title, CLASS,
                                            NOT_TESTED_NOT_DOCUMENTED, FALSE);
                add_func_nodes_to_class_node (class_node, input_path_str);

                free (input_path_str);
                free (title);
            }
        } else {
            break;
        }
    }

    free (pdir);

    return tree;
}

static void
add_func_nodes_to_class_node (TestListItem* class_node,
                              const char* input_file_name)
{
    FILE* p_input_file = open_file (input_file_name, "rt", NULL);
    char* p_input_file_contents = read_file_to_string (p_input_file);
    char* pc = NULL;
    char* pc1 = NULL;
    char* pre_func_tag_contents = NULL;
    char* func_tag_contents = NULL;
    int class_is_depr = TRUE;
    TestListItem* func_node = NULL;

    pc = strstr (p_input_file_contents, DETAILS_MARK);
    if (!pc)
    {
        free (p_input_file_contents);
        fclose (p_input_file);
        return;
    }

    pc1 = strstr (p_input_file_contents, PROPERTIES_MARK);
    if (!pc1)
    {
        pc1 = strstr (p_input_file_contents, STYLE_PROPERTIES_MARK);
    }
    if (!pc1)
    {
        pc1 = strstr (p_input_file_contents, SIGNALS_MARK);
    }
    if (pc1)
    {
        *pc1 = '\0';
    }
    
    pre_func_tag_contents = get_tag_contents (pc, "div", FUNC_DECL_SUB_TAG, NULL, NULL);

    while (pre_func_tag_contents)
    {

        /* +3 to include "</" and ">" */
        pc = strstr (pc, pre_func_tag_contents) + strlen (pre_func_tag_contents) + strlen ("div") + 3;

        func_tag_contents = get_tag_contents (pre_func_tag_contents, "pre", PROGRAMLISTING_SUB_TAG, NULL, NULL);
        if (func_tag_contents)
        {
            int is_depr = FALSE;
            char* tc = NULL;
            char* pre_div_tag_contents = NULL;
            char* div_tag_contents = NULL;
            char* div_tag_contents2 = NULL;
            
            pre_div_tag_contents = get_tag_contents 
                (pre_func_tag_contents, "div", WARNING_SUB_TAG, NULL, 
                 "class = \"informaltable\"");

            if (pre_div_tag_contents)
            {
                div_tag_contents = strdup (pre_div_tag_contents);
                div_tag_contents = cut_tags (div_tag_contents);
                if (strstr (div_tag_contents, DEPRECATED_TEXT))
                {
                    is_depr = TRUE;
                } else {
                    class_is_depr = FALSE;
                }
                free (div_tag_contents);
            } else {
                class_is_depr = FALSE;
            }            
            
            div_tag_contents2 = get_tag_contents 
                (pre_func_tag_contents, "div", EXAMPLE_SUB_TAG, NULL, 
                 "class = \"informaltable\"");
            if (div_tag_contents2)
            {
                char* p = strstr (pre_func_tag_contents, div_tag_contents2);
                *p = '\0';
            }
            
            tc = strdup (func_tag_contents);
            tc = cut_tags (tc);
            tc = trim (tc);
            
            if (!(strstr (tc, "struct") == tc)
                && !(strstr (tc, "enum") == tc)
                && !(strstr (tc, "#define") == tc)
                && !(strstr (tc, " = "))
                && strstr (tc, ");") == tc + strlen (tc) - 2)
            {
                char* asr_set = NULL;
                char* asr_set_add = NULL;
                char* asr_set_s = NULL;
                char* pch = NULL;
            
                func_node = test_list_add (class_node,
                                           tc,
                                           FUNCTION,
                                           NOT_TESTED_NOT_DOCUMENTED,
                                           is_depr);
                if (!is_depr)
                {
                    char* p_asr_set = NULL;
                    if (pre_div_tag_contents)
                    {
                        /* +3 to include "</" and ">" */
                        p_asr_set = 
                            strstr (pre_func_tag_contents, pre_div_tag_contents) 
                            + strlen (pre_div_tag_contents) + strlen ("div") + 3;
                    } else {
                        p_asr_set = 
                            strstr (pre_func_tag_contents, func_tag_contents)
                            + strlen (func_tag_contents) + strlen ("pre") + 3;
                    }
                    
                    asr_set_add = get_tag_contents (p_asr_set, "p", NULL, NULL, "class = \"informaltable\"");

                    while (asr_set_add)
                    {
                        char* pSave;
                        char* asr_set_add2;
                        

                        asr_set_add2 = strdup (asr_set_add);
                        
                        asr_set_add2 = trim_with_nl (asr_set_add2);
                        pSave = asr_set_add2;
                        asr_set_add2 = str_sum ("$", asr_set_add2);
                        free (pSave);
                        
                        if (strlen (asr_set_add2))
                        {
                            if (!asr_set)
                            {
                                asr_set = alloc_mem_for_string (asr_set, 1);
                            }
                            asr_set = str_append (asr_set, asr_set_add2);
                            /* we should add as dot after closing bracket to
                               split assertions like these: "(asr1.)asr2." */
                            if (*(asr_set + strlen (asr_set) - 1) == ')')
                            {
                                asr_set = str_append (asr_set, ".");
                            }
                        }
                        /* +3 to include "</" and ">" */
                        p_asr_set = strstr (p_asr_set, asr_set_add) + strlen (asr_set_add) 
                            + strlen ("p") + 3;
                        if (!(strstr (p_asr_set, "<p>") == p_asr_set))
                        {
                            free (asr_set_add);
                            break;
                        }
                        free (asr_set_add);
                        free (asr_set_add2);
                        asr_set_add = get_tag_contents (p_asr_set, "p", NULL, NULL, 
                                                        "class = \"informaltable\"");
                    }               
                    
                    if (asr_set)
                    {
                        asr_set = cut_tags (asr_set);
                        asr_set = trim (asr_set);
                        asr_set = replace_all_substr_in_string
                            (asr_set, "&lt;", "<");
                        asr_set = replace_all_substr_in_string
                            (asr_set, "&#60;", "<");
                        asr_set = replace_all_substr_in_string
                            (asr_set, "&gt;", ">");
                        asr_set = replace_all_substr_in_string
                            (asr_set, "&#62;", ">");
                        asr_set = replace_all_substr_in_string
                            (asr_set, "&quot;", "\"");
                        asr_set = replace_all_substr_in_string
                            (asr_set, "&amp;amp;", "&amp;");
                        asr_set = replace_all_substr_in_string
                            (asr_set, "&amp;", "&");
                        
                        asr_set_s = split_asr_set (asr_set);
                        
                        if (asr_set_s)
                        {
                            pch = strtok (asr_set_s, "$");
                            while (pch)
                            {
                                if (pch)
                                {
                                    test_list_add (func_node, pch, ASSERTION,
                                                   NOT_TESTED_NOT_DOCUMENTED,
                                                   is_depr);
                                }
                                pch = strtok (NULL, "$");
                            }
                            free (asr_set_s);
                        }
                    }
                }
            }

            free (tc);
            if (pre_div_tag_contents)
            {
                free (pre_div_tag_contents);
            }
        }
        pre_func_tag_contents = get_tag_contents (pc, "div", FUNC_DECL_SUB_TAG,
                                                  NULL, NULL);
    }

    class_node->is_deprecated = class_is_depr;
    return;

}


static char* 
gen_title (const char* input_file_name)
{
    FILE* p_input_file = open_file (input_file_name, "r", NULL);
    char* title = NULL;
    char* title_secondary = NULL;
    char* title_primary = NULL;
    char* p_input_file_contents = NULL;

    p_input_file_contents  = read_file_to_string (p_input_file);

    title_primary = get_tag_contents 
        (p_input_file_contents, "title", NULL, NULL, NULL);

    if (title_primary)
    {
        title_secondary = cut_tags (title_primary);
        title = alloc_mem_for_string 
            (title, strlen (title_secondary));
        strcat (title, title_secondary);
        free (title_secondary);
    }

    free (title_primary);
    free (p_input_file_contents);
    fclose (p_input_file);

    return title;
}
